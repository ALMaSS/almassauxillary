import os, re, platform, subprocess, shutil
from datetime import date

species_name = 'ApisRAM Honey Bee'
#species_name = 'Aphid'
author = 'Xiaodong Duan'

#a new if block needs to be added when add a new species
if species_name == 'ApisRAM Honey Bee':
	author += ' &Chris J. Topping' 
	if platform.system()=='Linux':
		#docx file path
		docx_file = '/home/uni.au.dk/au627002/OneDrive/Work/Projects/ApisRAM/SC4/SC4-3-FormalModel.docx'
		#html file path
		html_file = '/home/uni.au.dk/au627002/OneDrive/Code/ALMaSS_Doxygen/ApisRAM/SC4-3-FormalModel.html'
		#odd file path
		md_file = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_all/HoneyBee/HoneyBee_ODDox.txt'
		#late file path
		tex_file = '/home/uni.au.dk/au627002/OneDrive/Code/ALMaSS_Doxygen/ApisRAM/SC4-3-FormalModel.tex'
		#doxy file path for running doxygen
		doxy_file = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_ODdox/doxy_files/HoneyBeeODDox.dox'
		#images folder destination path
		img_dest_folder = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_ODdox/HoneyBee_ODdox/'

	else:
		docx_file = '/Users/xd/au/Work/Projects/ApisRAM/SC4/SC4-3-FormalModel.docx'
		html_file = '/Users/xd/au/Code/ALMaSS_Doxygen/ApisRAM/SC4-3-FormalModel.html'
		md_file = '/Users/xd/Code/ALMaSS/ALMaSS_all/HoneyBee/HoneyBee_ODDox.txt'
		tex_file = '/Users/xd/au/Code/ALMaSS_Doxygen/ApisRAM/SC4-3-FormalModel.tex'
		doxy_file = '/Users/xd/Code/ALMaSS/ALMaSS_ODdox/doxy_files/HoneyBeeODDox.dox'
		img_dest_folder = '/Users/xd/Code/ALMaSS/ALMaSS_ODdox/HoneyBee_ODdox/'

if species_name == 'Aphid':
	if platform.system()=='Linux':
		docx_file = '/home/uni.au.dk/au627002/OneDrive/Work/Projects/EcoStack/Aphid/AphidModel.docx'
		html_file = '/home/uni.au.dk/au627002/OneDrive/Code/ALMaSS_Doxygen/Aphid/AphidModel.html'
		md_file = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_all/Aphid/Aphid_ODDox.txt'
		tex_file = '/home/uni.au.dk/au627002/OneDrive/Code/ALMaSS_Doxygen/Aphid/AphidModel.tex'
		doxy_file = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_ODdox/doxy_files/AphidODDox.dox'
		img_dest_folder = '/home/uni.au.dk/au627002/Code/ALMaSS/ALMaSS_ODdox/Aphid_ODdox/'
	else:
		docx_file = '/Users/xd/au//Work/Projects/EcoStack/Aphid/AphidModel.docx'
		html_file = '/Users/xd/au/Code/ALMaSS_Doxygen/Aphid/AphidModel.html'
		md_file = '/Users/xd/Code/ALMaSS/ALMaSS_all/Aphid/Aphid_ODDox.txt'
		tex_file = '/Users/xd/au/Code/ALMaSS_Doxygen/Aphid/AphidModel.tex'
		doxy_file = '/Users/xd/Code/ALMaSS/ALMaSS_ODdox/doxy_files/AphidODDox.dox'
		img_dest_folder = '/Users/xd/Code/ALMaSS/ALMaSS_ODdox/Aphid_ODdox/'


#tags for extracting equation strings in latex file
equation_tag_in_tex = [['\\\\\(', '\\\\\)']]
#reg used for search in string
reg_source = '(.*?)'
#tags for extracting image including strings in html file
img_including_source = [['<img','\">']]

#when add new species, we need a new if block here
if species_name == 'ApisRAM Honey Bee':
	#real images list that will be kept
	no_replacing_img = ['image015', 'image054', 'image072', 'image132']
	#image including to be deleted
	deleting_img = ['image017', 'image018', 'image019', 'image020', 'image021', 'image022', 'image023', 'image024']
	#strings needs to be replaced or deleted
	replace_final_text = [['<html>  <head> <meta http-equiv=Content-Type content="text/html; charset=utf-8"> <meta name=Generator content="Microsoft Word 15 (filtered)"> <style>', ''],[' </style>  </head>  <body lang=en-DK link=\"#0563C1\" vlink=\"#954F72\" style=\'word-wrap:break-word\'>', ''],['</body>  </html>', ''],['\gimel', '\lambda']]
if species_name == 'Aphid':
	#real images list that will be kept
	no_replacing_img = ['image007']
	#image including to be deleted
	deleting_img = []
	#strings needs to be replaced or deleted
	replace_final_text = [['<html>  <head> <meta http-equiv=Content-Type content="text/html; charset=utf-8"> <meta name=Generator content="Microsoft Word 15 (filtered)"> <style>', ''],[' </style>  </head>  <body lang=en-DK link=\"#0563C1\" vlink=\"#954F72\" style=\'word-wrap:break-word\'>', ''],['</body>  </html>', ''],['</style>  </head>  <body lang=en-DK style=\'word-wrap:break-word\'>  ','']]

#get the current time for updating
today = date.today()

#title for the ODD file, species, author, updated time
title_name = '\mainpage '+species_name+'\r Created by: <br> <small> '+author+' <br> Department of Bioscience, Grenaavej 14, DK-8410 Rønde, Denmark <br> Last updated '+ today.strftime("%B %d, %Y") +' </small> <HR>'

if __name__ == '__main__':
	#convert docx to tex using pandoc
	current_pro = subprocess.Popen('pandoc '+docx_file+' -f docx -t latex -o '+tex_file, shell=True)
	current_pro.wait()
	print('Converting docx to tex is finished!')
	#load the tex file and extract all the equation strings
	with open(tex_file) as f:
		tex_content = f.read()
		#delete all the newlines
		tex_content=tex_content.replace('\r', ' ').replace('\n', ' ')

		#find all the strings for equation in the latex file
		for tag_item in equation_tag_in_tex:			
			equations_tex_list = re.findall(tag_item[0]+reg_source+tag_item[1], tex_content)
			#print(equations_tex_list)
			#print(len(equations_tex_list))
			#print(len(set(equations_tex_list)))

			#get all the unique math latex without replication, this not used
			unique_tex_list = []
			for ele in equations_tex_list:
				if ele not in unique_tex_list:
					unique_tex_list.append(ele)
			count = 0
			for ele in unique_tex_list:
				count += 1
				#print(count, ele)
			#print(len(unique_tex_list))
		f.close()
	
	#read html file 
	with open(html_file) as f:
		source = f.read()
		#delete all the newlines in html file
		source=source.replace('\r', ' ').replace('\n', ' ')
		for img_including_item in img_including_source:
			#find all the image including string in the html file
			img_including_list = re.findall(img_including_item[0]+reg_source+img_including_item[1], source)
			#print(len(img_including_list))

			#replace every low quality equation image with latex
			replace_counting=-1
			for img_including_finding in img_including_list:
				replace_flag = True
				#we need to keep the real image including
				for img_keeping_item in no_replacing_img:
					if img_keeping_item in img_including_finding:
						replace_flag = False
						break
				#delete items, since there are multiple images for one equation while there is only one line in math latex
				for img_deleting_item in deleting_img:
					if img_deleting_item in img_including_finding:		
						source = re.sub(img_including_item[0]+img_including_finding+img_including_item[1], '', source)
						replace_flag = False
						break
				#replace the image including with the math in latex
				if replace_flag:
					replace_counting+=1
					source = source.replace(img_including_item[0]+img_including_finding+img_including_item[1], '\\f$'+equations_tex_list[replace_counting]+'\\f$')
		f.close()

		#delete unnecessary parts and/or replace some texts for specific species  
		if len(replace_final_text):
			for replace_item in replace_final_text:
				source = source.replace(replace_item[0], replace_item[1])

		#write the odd file for the given species
		with open(md_file, 'w') as file_writer:
			file_writer.write(title_name+source)
			file_writer.close()

		#run doxygen
		current_pro = subprocess.Popen('doxygen '+doxy_file, cwd=os.path.dirname(doxy_file), shell=True)
		current_pro.wait()
		print('Doxygen running is finished!')

		
		#get the folder name for the image folder
		img_folder_name = os.path.basename(html_file)
		img_folder_name = img_folder_name.replace('.html', '.fld/')
		#copy including images folder
		type_pool = ['html', 'latex']
		for doxy_type in type_pool:
			html_img_dest_folder = os.path.join(img_dest_folder, doxy_type, img_folder_name)
			if os.path.exists(html_img_dest_folder):
				shutil.rmtree(html_img_dest_folder)
			shutil.copytree(html_file.replace('.html', '.fld/'), os.path.join(img_dest_folder, doxy_type, html_img_dest_folder))
		print('Image folder is copied')