# setting layers hierarchy and stacking it all up for a raw top
# this chunk also adds a basic layer of sliver 'value1' where there are gaps in data
import arcpy

arcpy.CheckOutExtension("Spatial")


class ArcpyEnv:
    extension = "Spatial"
    spatial_reference = 3067
    parallel_processing_factor = "100%"
    overwrite_output = True


def add_basic_sliver_layer(
        arcpy,
        landscape_name
):
    from arcpy.sa import *
    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)
    arcpy.env.workspace = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)
    outIsNull = IsNull("finland_topo")
    outCon = Con(outIsNull, 1, "finland_topo", "VALUE = 1")
    outCon.save(LCR + "finland_topo_silver")
    arcpy.RasterToPolygon_conversion(
        "finland_topo_silver",
        LCR + "TopoRaw_vector_silver",
        "NO_SIMPLIFY",
        "Value"
    )


def concat(
        arcpy,
        landscape_name
):
    print "concatination started"

    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)
    arcpy.env.workspace = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)

    ordered_layers = [
        "i222",
        "sn407",
        "i221",
        "sn402",
        "f301",
        "f302s",
        "f303",
        "f305s",
        "f307s",
        "f306s",
        "f308s",
        "n502",
        "n503",
        "n504",
        "n507",
        "i216",
        "n500",
        "n501",
        "sn401",
        "sn404",
        "sn405",
        "sn406",
        "w100",
        "w101",
        "w102",
        "w103",
        "w104",
        "w105",
        "i213",
        "w106",
        "sn403",
        "i217",
        "i219",
        "w107",
        "i215",
        "i214",
        "sn400",
        "i212",
        "i200",
        "i201",
        "i202",
        "i203",
        "i204",
        "i205",
        "i206",
        "i207",
        "i208",
        "i209",
        "i210",
        "i211",
        "i220",
        "f300"
    ]
    existing_layers = []
    for layer in ordered_layers:
        if arcpy.Exists(layer):
            existing_layers.append(layer)
    print existing_layers
    invert = (existing_layers[::-1])
    print invert

    arcpy.MosaicToNewRaster_management(
        invert,
        LCR,
        "finland_topo",
        "", "16_BIT_UNSIGNED",
        "1",
        "1",
        "FIRST",
        "FIRST"
    )
    arcpy.RasterToPolygon_conversion(
        "finland_topo",
        LCR + "TopoRaw_vector",
        "NO_SIMPLIFY",
        "Value"
    )
    print"raw topography ready"
