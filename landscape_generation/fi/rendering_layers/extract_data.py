from configuration.build_where_clause import build_where_clause
from arcpy.sa import *
from arcpy import env


def make_topography(arcpy, sr, newtab, outgdb, LC, LCR):
    print "rendering layers"

    arcpy.TableToTable_conversion(newtab, outgdb, 'landscapeclass')

    table = outgdb + 'landscapeclass'

    files = [
        'v',
        'p',
        's',
        't'
    ]
    # wybiera z pliku tekstowego wszystkie nazwy terenow
    field = 'name'
    with arcpy.da.SearchCursor(table, [field]) as cursor:
        landscape_classes = ({row[0] for row in cursor})
    print landscape_classes
    print "copying features from national database"
    for file in files:
        object = outgdb + file
        print "working on mahalayer - ", object
        arcpy.JoinField_management(object, 'LUOKKA', table, 'number')
        print file, "table updated"
        for clas in landscape_classes:
            print "searching for... ", clas
            in_features = object
            out_feature = LC + clas + file
            fieldname = "name"
            fieldvalue = clas
            where_clause = build_where_clause(arcpy, in_features, fieldname, fieldvalue)
            arcpy.Select_analysis(in_features, out_feature, where_clause)
            arcpy.DefineProjection_management(out_feature, sr)

            print clas, "exported"


    env.workspace = LC
    shapefiles = arcpy.ListFeatureClasses()
    print ""
    print ">>>identifying empty objects..."
    print""
    for shapefile in shapefiles:
        if arcpy.management.GetCount(shapefile)[0] == "0":
            arcpy.Delete_management(shapefile)

    print">>>empty objects deleted"
    print""
    print"procesing objects..."
    print"processing linear objects..."
    print "processing streams under 2m..."

    if arcpy.Exists(LC + "stream_under_2_mv"):
        eucDistTemp = EucDistance(LC + "stream_under_2_mv", "", "1", "")
        rasTemp = Con(eucDistTemp < 0.6, 100)
        rasTemp.save(LCR + "w100")
        print "done"
    else:
        print "No such features in the area"

    print "processing streams 2 - 5m..."

    if arcpy.Exists(LC + "stream_2_5_mv"):
        eucDistTemp = EucDistance(LC + "stream_2_5_mv", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.6, 101)
        rasTemp.save(LCR + "w101")
        print "done"
    else:
        print "No such features in the area"

    print "processing road Ia..."

    if arcpy.Exists(LC + "road_Iav"):
        eucDistTemp = EucDistance(LC + "road_Iav", "", "1", "")
        rasTemp = Con(eucDistTemp < 6, 200)
        rasTemp.save(LCR + "i200")
        print "done"
    else:
        print "No such features in the area"

    print "processing road Ib..."

    if arcpy.Exists(LC + "road_Ibv"):
        eucDistTemp = EucDistance(LC + "road_Ibv", "", "1", "")
        rasTemp = Con(eucDistTemp < 4, 201)
        rasTemp.save(LCR + "i201")
        print "done"
    else:
        print "No such features in the area"

    print "processing road IIa..."

    if arcpy.Exists(LC + "road_IIav"):
        eucDistTemp = EucDistance(LC + "road_IIav", "", "1", "")
        rasTemp = Con(eucDistTemp < 3.5, 202)
        rasTemp.save(LCR + "i202")
        print "done"
    else:
        print "No such features in the area"

    print "processing road IIb..."

    if arcpy.Exists(LC + "road_IIbv"):
        eucDistTemp = EucDistance(LC + "road_IIbv", "", "1", "")
        rasTemp = Con(eucDistTemp < 2.5, 203)
        rasTemp.save(LCR + "i203")
        print "done"
    else:
        print "No such features in the area"

    print "processing road IIIa..."

    if arcpy.Exists(LC + "road_IIIav"):
        eucDistTemp = EucDistance(LC + "road_IIIav", "", "1", "")
        rasTemp = Con(eucDistTemp < 2, 204)
        rasTemp.save(LCR + "i204")
        print "done"
    else:
        print "No such features in the area"

    print "processing roads IIIb..."

    if arcpy.Exists(LC + "road_IIIbv"):
        eucDistTemp = EucDistance(LC + "road_IIIbv", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.5, 205)
        rasTemp.save(LCR + "i205")
        print "done"
    else:
        print "No such features in the area"

    print "processing smaller roads..."

    if arcpy.Exists(LC + "smaller_roadv"):
        eucDistTemp = EucDistance(LC + "smaller_roadv", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.1, 206)
        rasTemp.save(LCR + "i206")
        print "done"
    else:
        print "No such features in the area"

    print "processing walking paths..."

    if arcpy.Exists(LC + "walking_pathv"):
        eucDistTemp = EucDistance(LC + "walking_pathv", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.1, 207)
        rasTemp.save(LCR + "i207")
        print "done"
    else:
        print "No such features in the area"

    print "processing railways..."

    if arcpy.Exists(LC + "railwayv"):
        eucDistTemp = EucDistance(LC + "railwayv", "", "1", "")
        rasTemp = Con(eucDistTemp < 10, 208)
        rasTemp.save(LCR + "i208")
        print "done"
    else:
        print "No such features in the area"

    print "processing narrow railways..."

    if arcpy.Exists(LC + "railway_narrowv"):
        eucDistTemp = EucDistance(LC + "railway_narrowv", "", "1", "")
        rasTemp = Con(eucDistTemp < 8, 209)
        rasTemp.save(LCR + "i209")
        print "done"
    else:
        print "No such features in the area"

    print "processing metro railways..."

    if arcpy.Exists(LC + "metro_railwayv"):
        eucDistTemp = EucDistance(LC + "metro_railwayv", "", "1", "")
        rasTemp = Con(eucDistTemp < 8, 210)
        rasTemp.save(LCR + "i210")
        print "done"
    else:
        print "No such features in the area"

    print "processing pipelines..."

    if arcpy.Exists(LC + "pipelinev"):
        eucDistTemp = EucDistance(LC + "pipelinev", "", "1", "")
        rasTemp = Con(eucDistTemp < 5, 211)
        rasTemp.save(LCR + "i211")
        print "done"
    else:
        print "No such features in the area"

    print "processing tree lines..."

    if arcpy.Exists(LC + "tree_linev"):
        eucDistTemp = EucDistance(LC + "tree_linev", "", "1", "")
        rasTemp = Con(eucDistTemp < 5, 300)
        rasTemp.save(LCR + "f300")
        print "done"
    else:
        print "No such features in the area"

    print "processing rivers..."

    if arcpy.Exists(LC + "riversp"):
        arcpy.PolygonToRaster_conversion(LC + "riversp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 102)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w102")
        arcpy.Delete_management(LC + "tmpRaster")

        print "done"
    else:
        print "No such features in the area"

    print "planar objects processing"

    print "processing parking lots..."

    if arcpy.Exists(LC + "parking_lotsp"):
        arcpy.PolygonToRaster_conversion(LC + "parking_lotsp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 212)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i212")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing cemeteries..."

    if arcpy.Exists(LC + "cemeteryp"):
        arcpy.PolygonToRaster_conversion(LC + "cemeteryp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 400)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "sn400")
        arcpy.Delete_management(LC + "tmpRaster")
        print"done"
    else:
        print "No such features in the area"

    print "processing sand areas..."

    if arcpy.Exists(LC + "sand_area"):
        arcpy.PolygonToRaster_conversion(LC + "sand_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 500)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n500")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing landfills & trashyards..."

    if arcpy.Exists(LC + "landfill_trashyardp"):
        arcpy.PolygonToRaster_conversion(LC + "landfill_trashyardp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER",
                                         "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 213)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i213")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing rock areas..."

    if arcpy.Exists(LC + "rock_areap"):
        arcpy.PolygonToRaster_conversion(LC + "rock_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 501)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n501")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing airports..."

    if arcpy.Exists(LC + "airportp"):
        arcpy.PolygonToRaster_conversion(LC + "airportp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 214)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n214")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing quarries..."

    if arcpy.Exists(LC + "quarryp"):
        arcpy.PolygonToRaster_conversion(LC + "quarryp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 215)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i215")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing agricultural fields..."

    if arcpy.Exists(LC + "agricultural_fieldp"):
        arcpy.PolygonToRaster_conversion(LC + "agricultural_fieldp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER",
                                         "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 401)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "sn401")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing vegetable gardens..."

    if arcpy.Exists(LC + "vegetable_gardenp"):
        arcpy.PolygonToRaster_conversion(LC + "vegetable_gardenp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 402)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "sn402")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing meadows..."

    if arcpy.Exists(LC + "meadowp"):
        arcpy.PolygonToRaster_conversion(LC + "meadowp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 502)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n502")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing parks..."

    if arcpy.Exists(LC + "parkp"):
        arcpy.PolygonToRaster_conversion(LC + "parkp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 403)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "sn403")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing peat areas..."

    if arcpy.Exists(LC + "peat_areap"):
        arcpy.PolygonToRaster_conversion(LC + "peat_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 503)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n503")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing swamps"

    if arcpy.Exists(LC + "swampp"):
        arcpy.PolygonToRaster_conversion(LC + "swampp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 504)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n504")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing soil dumps..."

    if arcpy.Exists(LC + "soil_dumpp"):
        arcpy.PolygonToRaster_conversion(LC + "soil_dumpp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 216)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i216")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing sport_areas..."

    if arcpy.Exists(LC + "sport_areap"):
        arcpy.PolygonToRaster_conversion(LC + "sport_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 217)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i217")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing lakes..."

    if arcpy.Exists(LC + "lakep"):
        arcpy.PolygonToRaster_conversion(LC + "lakep", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 103)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w103")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing seas..."

    if arcpy.Exists(LC + "seap"):
        arcpy.PolygonToRaster_conversion(LC + "seap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 104)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w104")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing forest meadows & bushes..."
    print" I am tired, give me a break"
    if arcpy.Exists(LC + "forest_meadows_bushesp"):
        arcpy.PolygonToRaster_conversion(LC + "forest_meadows_bushesp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER",
                                         "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 301)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "f301")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing moss areas..."

    if arcpy.Exists(LC + "moss_areap"):
        arcpy.PolygonToRaster_conversion(LC + "moss_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 507)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "n507")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing water_drainage_areas..."

    if arcpy.Exists(LC + "water_drainage_areap"):
        arcpy.PolygonToRaster_conversion(LC + "water_drainage_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER",
                                         "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 105)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w105")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing floodplain_wetlands..."

    if arcpy.Exists(LC + "floodplain_wetlandsp"):
        arcpy.PolygonToRaster_conversion(LC + "floodplain_wetlandsp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER",
                                         "NONE",
                                         "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 106)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w106")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing storage_areas..."

    if arcpy.Exists(LC + "storage_areap"):
        arcpy.PolygonToRaster_conversion(LC + "storage_areap", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 219)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i219")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing buildings..."

    if arcpy.Exists(LC + "buildingp"):
        arcpy.PolygonToRaster_conversion(LC + "buildingp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 220)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i220")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing pool_waters..."

    if arcpy.Exists(LC + "pool_waterp"):
        arcpy.PolygonToRaster_conversion(LC + "pool_waterp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 106)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "w106")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"

    print "processing built_up..."

    if arcpy.Exists(LC + "built_upp"):
        arcpy.PolygonToRaster_conversion(LC + "built_upp", "AINLAHDE", LC + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(LC + "tmpRaster"), 1, 221)
        rasTempN = SetNull(rasTemp < 2, rasTemp)
        rasTempN.save(LCR + "i221")
        arcpy.Delete_management(LC + "tmpRaster")
        print "done"
    else:
        print "No such features in the area"
