import fnmatch
import os
from zipfile import ZipFile

from configuration.data_paths_and_spatial_reference import directory_paths
from configuration.init_data_merge import init_data_merge
from configuration.remove_folder import remove_folder


def clip(
        arcpy,
        scratchDB,
        nationalDB,
        gisDB,
        out_feature,
        spatial_reference

):
    print "Copying files to process to gisDB ..."
    print "Processing Maastotietokanta data ..."

    # Select the proper 'poviats' to copy to gisDB
    print "Selecting poviats intersecting the study area ..."
    arcpy.SelectLayerByLocation_management('poviats_lyr', 'intersect', out_feature)
    arcpy.CopyFeatures_management('poviats_lyr', scratchDB + 'poviats_selected')
    poviatIDList = []
    cursor_pov = arcpy.SearchCursor(scratchDB + 'poviats_selected')
    region_code_column = 'LEHTITUNNU'
    for row_pov in cursor_pov:
        poviatID = row_pov.getValue(region_code_column)
        poviatIDList.append(poviatID)

    print 'poviats: ', poviatIDList

    print "removing: ", "C:\Ecostack\Finish_landscapes\\zippers"
    remove_folder("C:\Ecostack\Finish_landscapes\\zippers")
    print "removed: ", "C:\Ecostack\Finish_landscapes\\zippers"
    # Copying files to gisDB
    paths_to_zippers = []
    for poviatID in poviatIDList:
        poviat_id = poviatID.encode('utf-8')
        print "Processing poviat " + poviat_id
        for root, dirnames, filenames in os.walk(nationalDB):
            for filename in filenames:
                if poviat_id in filename:
                    paths_to_zippers.append((
                        poviat_id,
                        os.path.join(root, filename)
                    ))
    # print paths_to_zippers
    target_folders = extract_zippers(paths_to_zippers)
    # print 'target_folders: ', target_folders
    for target_folder in target_folders:
        for filename in os.listdir(target_folder):
            if filename.split('.')[1] == 'shp':
                arcpy.FeatureClassToGeodatabase_conversion(os.path.join(target_folder, filename), gisDB)

    # Listing all layers to process
    print "Clipping layers to the landscape extent ..."
    featureclasses = arcpy.ListFeatureClasses()
    print 'feature classes: ', featureclasses


    layerList = init_data_merge(featureclasses)

    for layer in layerList:
        # For each layer creating a list of files to merge
        fileList = []
        for item in featureclasses:
            if '_' + layer in item:
                fileList.append(item)
        print "layers to merge into mahalayers: ", fileList
        arcpy.Merge_management(fileList, scratchDB + layer)
        arcpy.RepairGeometry_management(scratchDB + layer)

        # Clipping to test area
        arcpy.Clip_analysis(scratchDB + layer, out_feature, gisDB + layer)

        # Defining projection
        arcpy.DefineProjection_management(gisDB + layer, spatial_reference)
    # print "Layer " + layer + " processed ..."


def process_forests(
        arcpy,
        scratchDB,
        forestDB,
        forestRastDB,
        out_feature,
        landscape_name
):
    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)

    arcpy.CreateFileGDB_management(forestRastDB, "ForestRasterFiles.gdb")
    forest_rast_db_path = os.path.join(forestRastDB, "ForestRasterFiles.gdb")
    print "Processing Forest data ..."

    # Select the proper 'forest poviats' to copy to gisDB
    print "Selecting forest poviats intersecting the study area ..."
    arcpy.SelectLayerByLocation_management('forpoviats_lyr', 'intersect', out_feature)
    arcpy.CopyFeatures_management('forpoviats_lyr', scratchDB + 'forpoviats_selected')
    forpoviatIDList = []
    cursor_pov = arcpy.SearchCursor(scratchDB + 'forpoviats_selected')
    region_code_column = 'LEHTITUNNU'
    for row_pov in cursor_pov:
        print 'region thingy: ', row_pov
        forpoviatID = row_pov.getValue(region_code_column)
        forpoviatIDList.append(forpoviatID)

    print 'forest poviats: ', forpoviatIDList

    # Copying files to gisDB

    for forpoviatID in forpoviatIDList:
        forpoviat_id = forpoviatID.encode('utf-8')
        print "Processing forest poviat all " + forpoviat_id

        for root, dirnames, filenames in os.walk(forestDB + "all"):
            for filename in fnmatch.filter(filenames, '*' + forpoviat_id[-2:] + '.tif'):
                print filename
                arcpy.RasterToGeodatabase_conversion(os.path.join(root, filename), forest_rast_db_path)
                print "all converted"
                filenameNew = filename[:-4]
                arcpy.Rename_management(os.path.join(forest_rast_db_path, filenameNew), filenameNew + "_all")
                print "all renamed"

    for forpoviatID in forpoviatIDList:
        forpoviat_id = forpoviatID.encode('utf-8')
        print "Processing forest poviat dis " + forpoviat_id

        for root, dirnames, filenames in os.walk(forestDB + "dis"):
            for filename in fnmatch.filter(filenames, '*' + forpoviat_id[-2:] + '.tif'):
                print filename
                arcpy.RasterToGeodatabase_conversion(os.path.join(root, filename), forest_rast_db_path)
                print "dis converted"
                filenameNew = filename[:-4]
                arcpy.Rename_management(os.path.join(forest_rast_db_path, filenameNew), filenameNew + "_dis")
                print "dis renamed"

        arcpy.env.workspace = forest_rast_db_path
        rastlistall = []
        forrasters = arcpy.ListRasters("*", "All")
        for raster in forrasters:
            if raster.endswith('_all'):
                rastlistall.append(raster)

        print rastlistall, "all"
        arcpy.MosaicToNewRaster_management(
            input_rasters=rastlistall,
            output_location=LCR,
            raster_dataset_name_with_extension="allforest",
            pixel_type="8_BIT_UNSIGNED",
            cellsize="1",
            number_of_bands="1",
            mosaic_method="LAST",
            mosaic_colormap_mode="FIRST"
        )
        print rastlistall, "processed"
        arcpy.env.workspace = forest_rast_db_path
        rastlistdis = []
        forrasters = arcpy.ListRasters("*", "All")
        for raster in forrasters:
            if raster.endswith('_dis'):
                rastlistdis.append(raster)
        print rastlistdis, "dis"
        arcpy.MosaicToNewRaster_management(
            input_rasters=rastlistdis,
            output_location=LCR,
            raster_dataset_name_with_extension="disforest",
            pixel_type="8_BIT_UNSIGNED",
            cellsize="1",
            number_of_bands="1",
            mosaic_method="LAST",
            mosaic_colormap_mode="FIRST"
        )
        print rastlistdis, 'processed'


def extract_zippers(
        zipper_paths
):
    os.mkdir(
        directory_paths.zip_output
    )

    target_folders = []
    for zipper_name, zipper_path in zipper_paths:
        target_folder = os.path.join(directory_paths.zip_output, zipper_name)

        with ZipFile(zipper_path, 'r') as opened_zip_file:
            opened_zip_file.extractall(target_folder)

        target_folders.append(
            target_folder
        )

    return target_folders
