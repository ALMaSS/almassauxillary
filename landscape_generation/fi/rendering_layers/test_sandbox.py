#This is just a playground for testing code chunks, disregard.

import arcpy
landscape_name = "oslo"

from arcpy.sa import *
class ArcpyEnv:
    extension = "Spatial"
    spatial_reference = 3067
    parallel_processing_factor = "100%"
    overwrite_output = True



arcpy.CheckOutExtension("Spatial")
LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
    landscape_directory=landscape_name)
arcpy.env.workspace = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
    landscape_directory=landscape_name)

arcpy.MosaicToNewRaster_management(
    "allroads ;sn404 ; allwater ; sn406 ; map506",
    LCR,
    "new_order",
    "",
    "16_BIT_UNSIGNED",
    "1",
    "1",
    "FIRST",
    "LAST"
)
