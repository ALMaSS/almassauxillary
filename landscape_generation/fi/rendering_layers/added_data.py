# creating new data based on the given topography 'eg. roadverges', adding missing data 'forests','soildumps' etc.
import arcpy

arcpy.CheckOutExtension("Spatial")
from arcpy.sa import *
from arcpy.sa import Lookup


class ArcpyEnv:
    extension = "Spatial"
    spatial_reference = 3067
    parallel_processing_factor = "100%"
    overwrite_output = True


def add_data(
        arcpy,
        landscape_name
):
    print "additional data"
    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)
    arcpy.env.workspace = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)

    list = []
    if arcpy.Exists("i200"):
        list.append("i200")
    if arcpy.Exists("i201"):
        list.append("i201")
    if arcpy.Exists("i202"):
        list.append("i202")
    if arcpy.Exists("i203"):
        list.append("i203")
    if arcpy.Exists("i204"):
        list.append("i204")
    if arcpy.Exists("i205"):
        list.append("i205")
    if arcpy.Exists("i206"):
        list.append("i206")

    print list

    def make_road_verges():
        arcpy.MosaicToNewRaster_management(
            input_rasters=list,
            output_location=LCR,
            raster_dataset_name_with_extension="allroads",
            pixel_type="16_BIT_UNSIGNED",
            cellsize="1",
            number_of_bands="1",
            mosaic_method="FIRST",
            mosaic_colormap_mode="LAST"
        )

        if arcpy.Exists("allroads"):
            eucDistTemp = EucDistance("allroads", "", "1", "")
            rasTemp = Con(eucDistTemp < 2, 404)
            rasTemp.save(LCR + "sn404")
            print "road verges done"
        else:
            print "No roads in the area"

    if list:
        make_road_verges()

    list2 = []
    if arcpy.Exists("i208"):
        list2.append("i208")
    if arcpy.Exists("i209"):
        list2.append("i209")
    if arcpy.Exists("i210"):
        list2.append("i210")
    print list2

    def make_rail_verges():
        arcpy.MosaicToNewRaster_management(
            input_rasters=list2,
            output_location=LCR,
            raster_dataset_name_with_extension="allrails",
            pixel_type="16_BIT_UNSIGNED",
            cellsize="1",
            number_of_bands="1",
            mosaic_method="FIRST",
            mosaic_colormap_mode="LAST"
        )
        if arcpy.Exists("allrails"):
            eucDistTemp = EucDistance("allrails", "", "1", "")
            rasTemp = Con(eucDistTemp < 2, 405)
            rasTemp.save(LCR + "sn405")
            print "rail verges done"
        else:
            print "No railways the area"

    if list2:
        make_rail_verges()

    list3 = []
    if arcpy.Exists("w100"):
        list3.append("w100")
    if arcpy.Exists("w101"):
        list3.append("w101")
    if arcpy.Exists("w102"):
        list3.append("w102")
    if arcpy.Exists("w103"):
        list3.append("w103")
    if arcpy.Exists("w104"):
        list3.append("w104")
    if arcpy.Exists("w105"):
        list3.append("w105")
    if arcpy.Exists("w106"):
        list3.append("w106")
    if arcpy.Exists("w107"):
        list3.append("w107")
    if arcpy.Exists("w108"):
        list3.append("w108")

    print list3

    def make_water_verges():
        arcpy.MosaicToNewRaster_management(
            input_rasters=list3,
            output_location=LCR,
            raster_dataset_name_with_extension="allwater",
            pixel_type="16_BIT_UNSIGNED",
            cellsize="1",
            number_of_bands="1",
            mosaic_method="FIRST",
            mosaic_colormap_mode="LAST"
        )
        if arcpy.Exists("allwater"):
            eucDistTemp = EucDistance("allwater", "", "1", "")
            rasTemp = Con(eucDistTemp < 2, 406)
            rasTemp.save(LCR + "sn406")
            print "waterbody verges done"
        else:
            print "No waterbodies in the area"

    if list3:
        make_water_verges()

    print "forest rasters processing"

    print "selecting bushes"
    arcpy.BuildRasterAttributeTable_management("allforest")
    bushes = ExtractByAttributes("allforest", "Value <= 30")
    bushesclass = Con(bushes < 30, 302)
    bushesclass.save(LCR + "f302")
    # below will smooth the outline of forest files
    arcpy.RasterToPolygon_conversion(LCR + "f302", LCR + "f302sv", "SIMPLIFY", "Value")
    arcpy.PolygonToRaster_conversion(LCR + "f302sv", "gridcode", LCR + "f302s", "", "", 1)

    print "selecting deciduous dominant forest"
    arcpy.BuildRasterAttributeTable_management("disforest")
    decdom = ExtractByAttributes("disforest", "Value > 70")
    decdomclass = Con(decdom > 70, 305)
    decdomclass.save(LCR + "f305")
    # below will smooth the outline of forest files
    arcpy.RasterToPolygon_conversion(LCR + "f305", LCR + "f305sv", "SIMPLIFY", "Value")
    arcpy.PolygonToRaster_conversion(LCR + "f305sv", "gridcode", LCR + "f305s", "", "", 1)

    print "selecting mixed forests"
    mixforest = ExtractByAttributes("disforest", "Value > 30 AND Value < 70")
    mixforestclass = Con(mixforest > 30, 306)
    mixforestclass.save(LCR + "f306")
    # below will smooth the outline of forest files
    arcpy.RasterToPolygon_conversion(LCR + "f306", LCR + "f306sv", "SIMPLIFY", "Value")
    arcpy.PolygonToRaster_conversion(LCR + "f306sv", "gridcode", LCR + "f306s", "", "", 1)

    print "selecting coniferous dominant"
    dis = ExtractByAttributes("disforest", "Value > 30")
    disclass = Con(dis > 30, 99)
    disclass.save(LCR + "dec")
    arcpy.RasterToPolygon_conversion(LCR + "dec", LCR + "decs", "NO_SIMPLIFY", "Value")

    forest = ExtractByAttributes("allforest", "Value > 30")
    forestclass = Con(forest > 30, 98)
    forestclass.save(LCR + "all")
    arcpy.RasterToPolygon_conversion(LCR + "all", LCR + "alls", "NO_SIMPLIFY", "Value")

    arcpy.Erase_analysis("alls", "decs", "condom")
    arcpy.PolygonToRaster_conversion("condom", "gridcode", LCR + "tmpRaster", "CELL_CENTER", "NONE", "1")
    rasTemp = Con(IsNull(LCR + "tmpRaster"), 1, 307)
    rasTempN = SetNull(rasTemp < 2, rasTemp)
    rasTempN.save(LCR + "f307")
    arcpy.Delete_management(LCR + "tmpRaster")
    # below will smooth the outline of forest files
    arcpy.RasterToPolygon_conversion(LCR + "f307", LCR + "f307sv", "SIMPLIFY", "Value")
    arcpy.PolygonToRaster_conversion(LCR + "f307sv", "gridcode", LCR + "f307s", "", "", 1)

    print "selecting swamp forests"
    arcpy.RasterToPolygon_conversion(LCR + "n504", LCR + "swamps", "NO_SIMPLIFY", "Value")
    arcpy.Intersect_analysis(["swamps", "alls"], LCR + "sf")
    arcpy.PolygonToRaster_conversion("sf", "gridcode", LCR + "tmpRaster", "CELL_CENTER", "NONE", "1")
    rasTemp = Con(IsNull(LCR + "tmpRaster"), 1, 308)
    rasTempN = SetNull(rasTemp < 2, rasTemp)
    rasTempN.save(LCR + "f308")
    arcpy.Delete_management(LCR + "tmpRaster")
    # below will smooth the outline of forest files
    arcpy.RasterToPolygon_conversion(LCR + "f308", LCR + "f308sv", "SIMPLIFY", "Value")
    arcpy.PolygonToRaster_conversion(LCR + "f308sv", "gridcode", LCR + "f308s", "", "", 1)

    print "adding house backyards"

    if arcpy.Exists(LCR + "i220"):
        eucDistTemp = EucDistance(LCR + "i220", "", "1", "")
        rasTemp = Con(eucDistTemp < 45, 407)
        rasTemp.save(LCR + "sn407")
        print "done"
    else:
        print "No houses in the area"

    print "adding dumps form CORINE"
    mask = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\outputs\\project.gdb\\mask".format(
        landscape_directory=landscape_name)
    CORINE = "C:\\Ecostack\\Finish_landscapes\\data\\GIS_data\\CORINE.gdb\\U2018_CLC2018_V2020_20u1"
    arcpy.RasterToPolygon_conversion(mask, LCR + "maskshp")
    arcpy.Clip_analysis(CORINE, LCR + "maskshp", LCR + "corine")
    target_raster_value = 132
    temporary_raster_name = LCR + "tmpRaster"
    arcpy.PolygonToRaster_conversion("corine", "Code_18", temporary_raster_name, "CELL_CENTER", "NONE", "1")
    arcpy.AddField_management(temporary_raster_name, "c18", "DOUBLE")
    arcpy.CalculateField_management(temporary_raster_name, "c18", "[Code_18]", "VB")
    outRaster = Lookup(temporary_raster_name, "c18")
    outRaster.save(LCR + "code18")

    rasTemp = Con(Raster(LCR + "code18") == target_raster_value, 222, 1)
    rasTempN = SetNull(rasTemp == 1, rasTemp)
    rasTempN.save(LCR + "i222")
    arcpy.Delete_management(temporary_raster_name)
