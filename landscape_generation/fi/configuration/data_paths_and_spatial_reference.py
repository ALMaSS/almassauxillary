# Data paths  and spatial reference

class ArcpyEnv:
    extension = "Spatial"
    spatial_reference = 3067
    parallel_processing_factor = "100%"
    overwrite_output = True

BASE_DIR = "C:\\Ecostack\\Finish_landscapes"
class Paths:
    dst = BASE_DIR + "\\finland_map_python"  # Output destination
    nationalDB = BASE_DIR + "\\data\\GIS_data\\Maastotietokanta"  # Folder with folders with shapefiles for 'poviats'
    forestDB = BASE_DIR + "\\data\\GIS_data\\Forest\\"
    agriDB = BASE_DIR + "\\data\\agricultural_data\\agricultural_register\\Kasvulohkot_2018_kaikki_2-siivottu.shp"
    regions = BASE_DIR + "\\data\\GIS_data\\Maastotietokanta\\UTM_EUREF_SHP\\utm25LR.shp"
    forregions = BASE_DIR + "\\data\\GIS_data\\Maastotietokanta\\UTM_EUREF_SHP\\utm200.shp"
    template = BASE_DIR + "\\template\\"
    in_features = BASE_DIR + "\\target\\target.gdb\\target"
    zip_output = BASE_DIR + "\\zippers\\"
    reclasstable = BASE_DIR + "\\finland_map_python\\test_areas\\reclasstable_new.txt"  # Reclass ascii table
directory_paths = Paths()
arcpy_env = ArcpyEnv()
