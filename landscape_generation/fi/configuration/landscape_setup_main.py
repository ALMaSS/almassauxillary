#Setting up some basic parameters
import os
from configuration.build_where_clause import build_where_clause
from configuration.database_paths_creation import (
    configure_db_paths,
    necessary_paths_creation,
)
from configuration.data_paths_and_spatial_reference import directory_paths


def landscape_setup(
        arcpy,
        defaultextent,
        defaultmask,
        dst,
        template,
        spatial_reference,
        landscape_name,
):
    arcpy.env.extent = defaultextent
    arcpy.env.mask = defaultmask

    necessary_paths_creation(
        dst,
        template,
        landscape_name
    )

    dbpath = configure_db_paths(
        dst,
        landscape_name

    )
    outPath = dbpath[0]
    gisDB = dbpath[1]
    scratchDB = dbpath[2]
    scratch = dbpath[3]
    forestRastDB = dbpath[4]

    in_features = directory_paths.in_features
    out_feature = os.path.join(dst, landscape_name, "outputs\\project.gdb",
                               landscape_name + "_mask")
    fieldname = "nazwa"
    fieldvalue = landscape_name
    where_clause = build_where_clause(arcpy, in_features, fieldname, fieldvalue)

    arcpy.Select_analysis(in_features, out_feature, where_clause)
    arcpy.DefineProjection_management(out_feature, spatial_reference)
    localSettings = out_feature

    mask = os.path.join(dst, landscape_name, "outputs\\project.gdb\\mask")
    arcpy.PolygonToRaster_conversion(out_feature, "OBJECTID", mask, "CELL_CENTER", "NONE", "1")

    # Model settings
    arcpy.env.overwriteOutput = True
    print gisDB
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratch
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"

    return (
        out_feature,
        mask,
        outPath,
        gisDB,
        scratchDB,
        scratch,
        forestRastDB
    )
