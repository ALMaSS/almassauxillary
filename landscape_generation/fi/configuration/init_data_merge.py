#tool for merging layers

def init_data_merge(
        featureclasses
):
    layer_list = []
    for feature in featureclasses:
        layer = feature[9]

        if layer not in layer_list:
            layer_list.append(layer)
    print "mahalayers to merge to: ", layer_list
    return layer_list
