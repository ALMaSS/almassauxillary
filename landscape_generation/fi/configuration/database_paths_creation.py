import os
import shutil

from arcpy import arcpy

from configuration.data_paths_and_spatial_reference import (
    directory_paths,
)


def configure_db_paths(
        dst,
        landscape,
):
    outPath = os.path.join(
        dst, landscape, "outputs", landscape + ".gdb\\"
    )
    gisDB = os.path.join(
        dst, landscape, landscape + ".gdb\\"
    )

    scratchDB = os.path.join(
        dst, landscape, "outputs\\scratch.gdb\\"
    )  # scratch folder for shp tempfiles
    scratch = os.path.join(
        dst, landscape, "outputs\\scratch\\"
    )  # scratch folder for raster tempfiles

    forestRastDB = os.path.join(
        dst, landscape, 'outputs'

    )
    localSettings = os.path.join(dst, landscape, "outputs\\project.gdb",
                                 landscape + "_mask")  # project folder with mask
    return (
        outPath,
        gisDB,
        scratchDB,
        scratch,
        forestRastDB,
        localSettings
    )


def necessary_paths_creation(
        dst,
        template,
        landscape
):
    dstpath = os.path.join(dst, landscape)
    shutil.copytree(template, dstpath)
    gdbpath = os.path.join(dstpath, "test_area.gdb")
    newgdbpath = os.path.join(dstpath, landscape)
    os.rename(gdbpath, newgdbpath + ".gdb")
    gdbpath2 = os.path.join(dstpath, "outputs", "test_area.gdb")
    newgdbpath2 = os.path.join(dstpath, "outputs", landscape)
    os.rename(gdbpath2, newgdbpath2 + ".gdb")


def make_topography_directories(
        arcpy,
        landscape_name
):
    table_with_classes = "C:\\Ecostack\\Finish_landscapes\\data\\GIS_data\\landscape_classes.csv"
    outgdb = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\{gdb_name}.gdb\\".format(
        landscape_directory=landscape_name, gdb_name=landscape_name)
    arcpy.CreateFileGDB_management(
        "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\".format(
            landscape_directory=landscape_name), "landscapeclasses.gdb")
    LC = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclasses.gdb\\".format(
        landscape_directory=landscape_name)
    arcpy.CreateFileGDB_management(
        "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\".format(
            landscape_directory=landscape_name), "landscapeclassesRaster.gdb")
    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name)
    return (
        table_with_classes,
        outgdb,
        LC,
        LCR
    )


def configure_db_paths2():
    # Path to the template directory
    template = directory_paths.template

    # Set fixed paths
    dst = directory_paths.dst  # Output destination

    nationalDB = directory_paths.nationalDB  # Folder with folders with shapefiles for 'poviats'
    forestDB = directory_paths.forestDB

    regions = directory_paths.regions
    forregions = directory_paths.forregions

    arcpy.MakeFeatureLayer_management(regions, "poviats_lyr")
    arcpy.MakeFeatureLayer_management(forregions, "forpoviats_lyr")

    # We need to set these each time we loop through otherwise we recycle the ones fromt the previous
    # run
    defaultextent = arcpy.env.extent
    defaultmask = arcpy.env.mask

    return (
        template,
        dst,
        nationalDB,
        forestDB,
        defaultextent,
        defaultmask
    )
