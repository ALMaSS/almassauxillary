import arcpy

landscape_name = "kielce"

from arcpy.sa import *
import arcpy
from arcpy.sa import *
from arcpy import env


class ArcpyEnv:
    extension = "Spatial"
    spatial_reference = 3067
    parallel_processing_factor = "100%"
    overwrite_output = True


arcpy.CheckOutExtension("Spatial")
LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
    landscape_directory=landscape_name)
arcpy.env.workspace = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
    landscape_directory=landscape_name)
scratchDB = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\outputs\\scratch.gdb\\".format(
    landscape_directory=landscape_name)

Reclass2 = Con(
        IsNull(
            scratchDB + "map1_NW_selection_lyr_raster"
        ),
        2,
        1
    )
toNibble2 = SetNull(
    Reclass2,
    2,
    "Value = 1"
)


map2_NW = Nibble(
    LCR + "finland_topo_silver",
    toNibble2,
    "DATA_ONLY"
)
map2_NW.save(
    LCR + "map2_N"
)
inrats = Raster(LCR + "map2_N")

map_2NWO = Con(
    (inrats == 1), 505, inrats)

map_2NWO.save(
    LCR + "map2XD")