#This part is a main sliver removal tool
import arcpy
from arcpy.sa import *
from arcpy import env

def remove_sliver(landscape_name, scratchDB, LCR):
    print "Processing silver for landscape " + landscape_name + " ..."



    arcpy.env.workspace = LCR
    print "Eliminating sliver polygons ..."

    print "Step 2: Removing elongated background polygons ..."
    arcpy.MakeFeatureLayer_management(
        LCR + "TopoRaw_vector_silver",
        "map1_NW_lyr"
    )
    arcpy.SelectLayerByAttribute_management(
        "map1_NW_lyr",
        "NEW_SELECTION",
        "gridcode = 1"
    )
    arcpy.CopyFeatures_management(
        "map1_NW_lyr",
        scratchDB + "map1_NW_selection"
    )
    arcpy.MinimumBoundingGeometry_management(
        scratchDB + "map1_NW_selection",
        scratchDB + "map1_NW_selection_bounding",
        "CIRCLE",
        "NONE",
        "",
        "MBG_FIELDS"
    )
    arcpy.JoinField_management(
        scratchDB + "map1_NW_selection",
        "Id",
        scratchDB + "map1_NW_selection_bounding",
        "Id",
        ""
    )
    arcpy.MakeFeatureLayer_management(
        scratchDB + "map1_NW_selection",
        "map1_NW_selection_lyr"
    )
    arcpy.SelectLayerByAttribute_management(
        "map1_NW_selection_lyr",
        "NEW_SELECTION",
        "Shape_Area/ Shape_Length<12"
    )  # "Shape_Area/Shape_Area_1 < 0.05 AND
    arcpy.SelectLayerByAttribute_management(
        "map1_NW_selection_lyr",
        "NEW_SELECTION",
        "Shape_Area/Shape_Length<12"
    )  # AND Shape_Area < 1500 Shape_Area/Shape_Area_1 < 0.05 AND
    arcpy.PolygonToRaster_conversion(
        "map1_NW_selection_lyr",
        "gridcode",
        scratchDB + "map1_NW_selection_lyr_raster",
        "",
        "",
        1
    )
    Reclass2 = Con(
        IsNull(
            scratchDB + "map1_NW_selection_lyr_raster"
        ),
        2,
        1
    )
    toNibble2 = SetNull(
        Reclass2,
        2,
        "Value = 1"
    )
    map2_NW = Nibble(
        LCR + "finland_topo_silver",
        toNibble2,
        "DATA_ONLY"
    )
    map2_NW.save(
        LCR + "map2_N"
    )
    inrats = Raster(LCR + "map2_N")

    map_2NWO = Con(
        (inrats == 1), 505, inrats)

    map_2NWO.save(
        LCR + "map2_NW"
    )

    arcpy.RasterToPolygon_conversion(
        LCR + "map2_NW",
        scratchDB + "map2_NW_vector",
        "NO_SIMPLIFY",
        "VALUE"
    )




