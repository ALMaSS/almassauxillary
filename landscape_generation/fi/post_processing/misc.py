#toolbox
import csv

import arcpy
import pandas as pd
from dbfpy import dbf


def dbf_to_csv(out_table):  # Input a dbf, output a csv
    csv_fn = out_table[:-4] + ".csv"  # Set the table as .csv format
    with open(csv_fn, 'wb') as csvfile:  # Create a csv file and write contents from dbf
        in_db = dbf.Dbf(out_table)
        out_csv = csv.writer(csvfile)
        names = []
        for field in in_db.header.fields:  # Write headers
            names.append(field.name)
        out_csv.writerow(names)
        for rec in in_db:  # Write records
            out_csv.writerow(rec.fieldData)
        in_db.close()
    return csv_fn


# Defining table to pandas data frame conversion
def table_to_data_frame(in_table, input_fields=None, where_clause=None):
    """
    Function will convert an arcgis table into a pandas dataframe with an object ID index, and the selected
    input fields using an arcpy.da.SearchCursor.
    """
    OIDFieldName = arcpy.Describe(in_table).OIDFieldName
    if input_fields:
        final_fields = [OIDFieldName] + input_fields
    else:
        final_fields = [field.name for field in arcpy.ListFields(in_table)]
    data = [row for row in arcpy.da.SearchCursor(in_table, final_fields, where_clause=where_clause)]
    fc_dataframe = pd.DataFrame(data, columns=final_fields)
    fc_dataframe = fc_dataframe.set_index(OIDFieldName, drop=True)
    return fc_dataframe
