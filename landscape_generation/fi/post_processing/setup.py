#This part creates directores for script part2, and sets some parameters. Some parts are not necessary (#) but might come handy in the future
import os
from collections import namedtuple
import arcpy
def create_silver_directories_and_paths(
        landscape_name,
        dst
):
    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    LCR = "C:\\Ecostack\\Finish_landscapes\\finland_map_python\\{landscape_directory}\\landscapeclassesRaster.gdb\\".format(
        landscape_directory=landscape_name
    )

    outPath = os.path.join(
        dst,
        landscape_name,
        "outputs",
        landscape_name + ".gdb/"
    )
    gisDB = os.path.join(
        dst,
        landscape_name,
        landscape_name + ".gdb/"
    )
    scratchDB = os.path.join(
        dst,
        landscape_name,
        "outputs/scratch.gdb/"
    )  # scratch folder for tempfiles
    scratch = os.path.join(
        dst,
        landscape_name,
        "outputs/scratch/"
    )  # scratch folder for tempfiles
    localSettings = os.path.join(
        dst,
        landscape_name,
        "outputs\\project.gdb",
        landscape_name + "_mask"
    )  # project folder with _mask
    #
    # asciifile = "".join(["ASCII_", landscape_name, ".txt"])
    # asciiexp = os.path.join(
    #     dst,
    #     landscape_name,
    #     "outputs/ALMaSS_files",
    #     asciifile
    # )  # export in ascii (for ALMaSS)
    # attrtable = "".join(["ATTR_", landscape_name, ".csv"])  # Name of attribute table
    # attrexp = os.path.join(
    #     dst,
    #     landscape_name,
    #     "outputs/ALMaSS_files",
    #     attrtable
    # )  # full path
    # soiltable = "".join(["SOIL_", landscape_name, ".csv"])  # Name of soil type table
    # soilexp = os.path.join(
    #     dst,
    #     landscape_name,
    #     "outputs/ALMaSS_files",
    #     soiltable
    # )  # full path
    # fieldstable = "".join(["FIELDS_", landscape_name, ".dbf"])  # Name of fields table
    #
    # fieldsexp = os.path.join(
    #     dst,
    #     landscape_name,
    #     "outputs/ALMaSS_files",
    #     fieldstable
    # )  # full path
    # permtable = "".join(["PERM_CROPS_", landscape_name, ".csv"])  # Name of permanent crops table
    # permexp = os.path.join(
    #     dst,
    #     landscape_name,
    #     "outputs/ALMaSS_files"
    # )  # folder
    #
    # # Deleting folder with ALMaSS outputs if exists
    # if os.path.exists(os.path.join(dst, landscape_name, "outputs/ALMaSS_files")):
    #     shutil.rmtree(os.path.join(dst, landscape_name, "outputs/ALMaSS_files"))
    #     print "... deleting existing folder"
    # os.mkdir(os.path.join(dst, landscape_name, "outputs/ALMaSS_files"))
    # print localSettings

    PathStore = namedtuple(
        'Paths', ['gisDB', 'scratchDB', 'local_settings', 'LCR']
    )
    paths = PathStore(
        gisDB=gisDB,
        scratchDB=scratchDB,
        local_settings=localSettings,
        LCR=LCR
    )

    return paths


def arcpy_env_settings(gisDB, scratchDB, local_settings):
    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = local_settings
    arcpy.env.mask = local_settings
    arcpy.env.cellSize = local_settings
    print "... model settings read"
    print " "

#
