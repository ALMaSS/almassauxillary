#This is the main pathway to remove all gimmicks from the main.py landscape generation. Also known as script part2.
import gc
import os
import time

import arcpy

from post_processing.remove_sliver import remove_sliver
from post_processing.repair_agriculture import repair_agriculture
from post_processing.repair_roads_and_rivers import repair_roads_and_rivers
from post_processing.setup import create_silver_directories_and_paths, arcpy_env_settings

arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"


def process_sliver(landscape_name, dst):
    nowTime = time.strftime('%X %x')
    gc.enable
    print "Sliver removal: " + nowTime

    paths = create_silver_directories_and_paths(
        landscape_name,
        dst
    )

    arcpy_env_settings(
        paths.gisDB,
        paths.scratchDB,
        paths.local_settings
    )
    remove_sliver(
        landscape_name,
        paths.scratchDB,
        paths.LCR
    )
    print "working on agriculture field gaps...."
    out_feature = os.path.join(
        dst,
        landscape_name,
        "outputs\\project.gdb",
        landscape_name + "_mask"
    )
    repair_agriculture(paths.LCR, out_feature)
    print "No sliver left"
    repair_roads_and_rivers(paths.LCR)

