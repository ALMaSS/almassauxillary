#This part rapairs issues with roads and rivers, sets them in hierarhy with other layers and produces the final output of topo landscape generation
import arcpy
from arcpy.sa import *


def repair_roads_and_rivers(
        LCR
):
    print "repairing roads and rivers..."
    # eliminate nibble mistakes by the roads
    or3 = Reclassify(
        LCR + "new_all_fields_map",
        "Value",
        RemapValue([[404, 506]])
    )
    or3.save(
        LCR + "map506"
    )

    arcpy.MosaicToNewRaster_management(
        "allroads ;sn404 ; allwater ; sn406 ; map506",
        LCR,
        "new_order",
        "",
        "16_BIT_UNSIGNED",
        "1",
        "1",
        "FIRST",
        "LAST"
    )