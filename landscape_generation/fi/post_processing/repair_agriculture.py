#This part removes sliver related to agriculture, adds non clasified fields to the list, and gived them numbers +1000
import arcpy
from arcpy.sa import *

from configuration.data_paths_and_spatial_reference import directory_paths, arcpy_env


def repair_agriculture(
        LCR,
        out_feature
):
    AGR = directory_paths.agriDB

    arcpy.env.workspace = LCR

    arcpy.Clip_analysis(
        AGR,
        out_feature,
        LCR + "agrifields"
    )
    arcpy.DefineProjection_management(
        LCR + "agrifields",
        arcpy_env.spatial_reference
    )

    arcpy.RasterToPolygon_conversion(
        LCR + "sn401",
        LCR + "falseagri",
        "SIMPLIFY",
        "Value"
    )
    arcpy.Erase_analysis(
        LCR + "falseagri",
        LCR + "agrifields",
        LCR + "agrileft"
    )
    arcpy.PolygonToRaster_conversion(
        LCR + "agrileft",
        "gridcode",
        LCR + "agriseparast",
        "",
        "",
        1
    )
    arcpy.RasterToPolygon_conversion(
        LCR + "agriseparast",
        LCR + "agrileft2",
        "NO_SIMPLIFY",
        "Value"
    )
    arcpy.MakeFeatureLayer_management(
        LCR + "agrileft2",
        LCR + "agl_lyr"
    )
    arcpy.SelectLayerByAttribute_management(
        LCR + "agl_lyr",
        "NEW_SELECTION",
        "(Shape_Area >= 1000) AND (Shape_Area/Shape_Length>5)"
    )
    arcpy.CopyFeatures_management(
        LCR + "agl_lyr",
        LCR + "fieldsunknown"
    )

    arcpy.Merge_management(
        "fieldsunknown ; agrifields",
        LCR + "allfields"
    )

    arcpy.AddField_management(
        LCR + "allfields",
        "nmbr",
        "DOUBLE"
    )
    cursor = arcpy.UpdateCursor(
        LCR + "allfields"
    )  # allfields is the ultimate field register
    const = 1001
    for j, row in enumerate(cursor):
        row.setValue(
            'nmbr',
            j + const
        )
        cursor.updateRow(
            row
        )

    or2 = Reclassify(
        LCR + "map2_NW",
        "Value",
        RemapValue([
            [401, 408]
        ])
    )
    or2.save(
        LCR + "map408"
    )
    arcpy.PolygonToRaster_conversion(
        LCR + "allfields",
        "nmbr",
        LCR + "allfields_rast",
        "",
        "",
        1
    )
    arcpy.MosaicToNewRaster_management(
        "allfields_rast; map408",
        LCR,
        "new_all_fields_map",
        "",
        "16_BIT_UNSIGNED",
        "1",
        "1",
        "FIRST",
        "FIRST"
    )
