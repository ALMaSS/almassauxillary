
#Landscape generator for Finland
#This script convert feature layers to rasters and assemble a raw surface covering land-use map. Further analysis, including cleaning of sliver polygons
#original script for Denmark by Flemming Skov & Lars Dalby - Oct-Dec 2014; modified by Elzbieta Ziolkowska;
# modified for Finland by Lukasz Mikolajczyk - Sun in Scorpio 2020
#Pathway for raw landscape generation
import gc
import time

from arcpy import arcpy

from configuration.database_paths_creation import make_topography_directories, configure_db_paths2
from configuration.data_paths_and_spatial_reference import (
    arcpy_env,
)
from configuration.landscape_setup_main import landscape_setup
from configuration.remove_folder import remove_folder
from rendering_layers.Raw_data_clipping import clip, process_forests
from rendering_layers.added_data import add_data
from rendering_layers.concatination_and_sliver_value1 import add_basic_sliver_layer
from rendering_layers.concatination_and_sliver_value1 import concat
from rendering_layers.extract_data import make_topography
from post_processing.sliver import process_sliver


def main():
    arcpy.CheckOutExtension(arcpy_env.extension)
    arcpy.env.parallelProcessingFactor = arcpy_env.parallel_processing_factor
    arcpy.env.overwriteOutput = arcpy_env.overwrite_output
    spatial_reference = arcpy.SpatialReference(arcpy_env.spatial_reference)

    nowTime = time.strftime('%X %x')
    gc.enable
    print "Model landscape generator part1 started: " + nowTime
    print "... system modules checked"



    ### Setting a list with all the landscapes to process
    landscape_names = ["kielce"]

    [
        template,
        dst,
        nationalDB,
        forestDB,
        defaultextent,
        defaultmask
    ] = configure_db_paths2()

    ### Processing each landscape

    #removing old, outdated files
    for landscape_name in landscape_names:
        print "Processing landscape ' " + landscape_name + " '..."
        print "removing: ", dst + "\\{directory_to_remove}".format(directory_to_remove=landscape_name)
        remove_folder(dst + "\\{directory_to_remove}".format(directory_to_remove=landscape_name))
        print "removed: ", dst + "\\{removed_direcotry}".format(removed_direcotry=landscape_name)

        [
            out_feature,
            mask,
            outPath,
            gisDB,
            scratchDB,
            scratch,
            forestRastDB
        ] = landscape_setup(
            arcpy,
            defaultextent,
            defaultmask,
            dst,
            template,
            spatial_reference,
            landscape_name
        )

        clip(
            arcpy,
            scratchDB,
            nationalDB,
            gisDB,
            out_feature,
            spatial_reference,
        )
        [
            newtab,
            outgdb,
            LC,
            LCR
        ] = make_topography_directories(arcpy, landscape_name)

        make_topography(
            arcpy,
            spatial_reference,
            newtab,
            outgdb,
            LC,
            LCR
        )
        process_forests(
            arcpy,
            scratchDB,
            forestDB,
            forestRastDB,
            out_feature,
            landscape_name
        )
        add_data(
            arcpy,
            landscape_name
        )
        concat(
            arcpy,
            landscape_name
        )
        add_basic_sliver_layer(arcpy,
                               landscape_name
                               )
        process_sliver(
            landscape_name,
            dst
        )

        print "raw topo done"
        nowTime = time.strftime('%X %x')
        print "it is: " + nowTime


if __name__ == '__main__':
    main()