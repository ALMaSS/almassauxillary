# -*- coding: cp1252 -*-
#=======================================================================================================================
### Name: lsgenDK_part1_21c04
### Parent : lsgenDK_part1_forLCT_part3Dev_21b22
### Purpose: The script convert feature layers to rasters and assemble a raw surface covering land-use map. Further analysis, including cleaning of sliver polygons and creating ALMaSS input landscape map are covered by separate script due to processing issues.
### Authors: original script for Denmark by Flemming Skov & Lars Dalby - Oct-Dec 2014; modified for Denmark by Elzbieta Ziolkowska - February 2017; further modified by Geoff Groom 2019 - 2021
### Passes for use with ArcGis 10.4, 10.6, 10.8
###
### script update specific notes:
### 21b11 : includes a workaround to resolve the issue of T1_Roads not correctly including cell values > 128 ...
###         ... in <Conversion> the CON TRUE and CON FALSE raster cell values for the raster inputs to T1_ROADS are inflated x10 from what it use to be, ...
###         e.g. rasTemp = Con(eucDistTemp < 1.75, 1100, 10)
###         ... in <Themes> the key line is then rasTemp = (CellStatistics(rasterList_road, "MAXIMUM", "DATA") / 10)
###
#===== Chunk: General setup =====#

### Import system modules
import arcpy, traceback, sys, time, gc, os, shutil, csv
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
arcpy.env.overwriteOutput = True
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part1 started: " + nowTime
print "... system modules checked"

### Defining all neccesary functions

# Define function to construct WHERE clause:
def buildWhereClause(table, field, value):
  """Constructs a SQL WHERE clause to select rows having the specified value
  within a given field and table."""

  # Add DBMS-specific field delimiters
  fieldDelimited = arcpy.AddFieldDelimiters(table, field)
  # Determine field type
  fieldType = arcpy.ListFields(table, field)[0].type
  # Add single-quotes for string field values
  if str(fieldType) == 'String':
      value = "'%s'" % value
  # Format WHERE clause
  whereClause = "%s = %s" % (fieldDelimited, value)
  return whereClause

### Set drive
#drive = "O:\ST_ALMaSS-LS19\CORE\DK_30lsgen_19d25\DK_30lsgen_19d25_WorkingModel"
drive = "C:/ST_ALMaSS-LS19/TESTS/fullTests_20e20/_DK_/_DK_20i23_dkgis20/30lsgen/8_forLCT_part3Dev/8_forLCT_part3Dev_21b09"  ### EDIT THIS !

### Set paths

# Path to the template directory
template = (drive + "/test_areas/test_area/")

# Set fixed paths
dst = (drive + "/test_areas/")  # Output destination
nationalDB = (drive + "/data/dk_newgis_part1_8.gdb")  # Geodatabase with feature layers for DK

# We need to set these each time we loop through otherwise we recycle the ones fromt the previous run  
defaultextent = arcpy.env.extent
defaultmask = arcpy.env.mask

#in_features = (drive + "/kvadrater.gdb/kvadrater_dkgis_newlayers") # ... for various 5x5km and 2x2km test windows
in_features = (drive + "/kvadrater.gdb/kvadrater_apisram_2019")                                                             ### EDIT THIS !

### Setting a list with all the landscapes to process
#landscapes = ["WN", "WS", "EN", "ES"]
landscapes = ["WS"]                                                                                                         ### EDIT THIS !

#===== End chunk: General setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
  print "Processing landscape " + landscapes[index] + " ..."

  #===== Chunk: Landscape-specific setup =====#
  
  arcpy.env.extent = defaultextent
  arcpy.env.mask = defaultmask

  ##  THIS PART MAKES THE FOLDERS NECESSARY - REMEMBER TO UN-COMMENT FIRST TIME
  dstpath = os.path.join(dst, landscapes[index])
  print template
  print dstpath
  shutil.copytree(template, dstpath)
  gdbpath = os.path.join(dstpath, "test_area.gdb")
  newgdbpath = os.path.join(dstpath, landscapes[index])
  os.rename(gdbpath, newgdbpath + ".gdb")
  gdbpath2 = os.path.join(dstpath, "outputs", "test_area.gdb")
  newgdbpath2 = os.path.join(dstpath, "outputs", landscapes[index])
  os.rename(gdbpath2, newgdbpath2 + ".gdb")

  # Data - paths to data, output gdb, scratch folder and simulation landscape mask
  outPath = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
  gisDB = os.path.join(dst, landscapes[index], landscapes[index] + ".gdb/")
  scratchDB = os.path.join(dst, landscapes[index], "outputs/scratch/")  # scratch folder for tempfiles

  # Select the landscape and convert to raster
  # Set local variables
  #in_features = (drive + "/kvadrater.gdb/kvadrater_dkgis_newlayers") # ... for various 5x5km and 2x2km test windows
  #in_features = (drive + "/kvadrater.gdb/kvadrater_apisram_2019") # ... for the WS, WN, EN; ES 2019 position adjusted APISRAM windows
  out_feature = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask") # _mask is a vector feature class
  fieldname = "NAME"
  fieldvalue = landscapes[index]
  where_clause = buildWhereClause(in_features, fieldname, fieldvalue)

  # Execute Select
  arcpy.Select_analysis(in_features, out_feature, where_clause)

  # Make 200m buffered form, with squared corners, of out_feature ...
  # This is required in order to avoid "edge effects" arising from polygon-size controlled operations in part-2 ...
  out_feature_buf200m = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask_buf200m") # this is a polygon feature class
  arcpy.Buffer_analysis(out_feature, out_feature_buf200m, "200 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")
  out_feature_buf200m_envlp = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask_buf200m_envlp") # this is a polygon feature class
  arcpy.FeatureEnvelopeToPolygon_management(out_feature_buf200m, out_feature_buf200m_envlp, "SINGLEPART")

  # Set localSettings ...
  #localSettings = out_feature
  localSettings = out_feature_buf200m_envlp # is a vector polygon

  # and convert polygon of the mask to raster
  #arcpy.PolygonToRaster_conversion(out_feature, "OBJECTID", mask, "CELL_CENTER", "NONE", "1")
  mask = os.path.join(dst, landscapes[index], "outputs/project.gdb/mask")
  arcpy.PolygonToRaster_conversion(out_feature_buf200m_envlp, "OBJECTID", mask, "CELL_CENTER", "NONE", "1")

  # Model settings
  arcpy.env.overwriteOutput = True
  arcpy.env.workspace = gisDB
  arcpy.env.scratchWorkspace = scratchDB
  arcpy.env.extent = localSettings
  arcpy.env.mask = localSettings
  arcpy.env.cellSize = localSettings
  print "... model settings read"

  # Model execution - controls which processes to run:
  default = 1  # 1 -> run process; 0 -> do not run process
  
  # Mosaic
  road = default      #create road theme
  builtup = default #create built up theme
  nature = default       #create nature theme
  wetnature = default   #create wet nature theme
  water = default   #create fresh water theme
  cultural = default      #create cultural feature theme
  finalmap = default      #assemble final map

  rasterList_road = []
  rasterList_builtup = []
  rasterList_nature = []
  rasterList_wetnature = []
  rasterList_water = []
  rasterList_cultural = []

  # Conversion  - features to raster layers
  landsea = default   #land_sea
  slopes_105 = default   #slopes along roads
  roadsideverge_110 = default   #road verges
  paths_112 = default   #paths
  parks_114 = default   #parking areas
  dirtroads_115 = default   #unpaved roads and tracks
  railways_120 = default   #railways
  smallroads_122 = default   #small roads (< 3 meter)
  mediumroads_125 = default   #medium sized roads (3-6 meter)
  largeroads_130 = default   #large roads (> 6 meter)
  pylons_150 = default   #pylons
  windturbines_155 = default   #wind turbines
  builtuplow_205 = default   #built up areas low
  builtuphigh_210 = default   #built up areas high
  citycenter_215 = default   #city center
  industry_220 = default   #industrial areas
  churchyard_225 = default   #Churchyards
  buildings_250 = default   #buildings
  forests_310 = default   #top10dk forest
  shrubs_315 = default   #top10dk shrub
  sand_320 = default   #top10dk sand flats
  heathland_325 = default   #top10dk heath land
  wetland_330 = default   #top10dk wetland
  meadowprotected_355 = default   #protected meadows
  heathlandprotected_360 = default   #protected heath land
  bog_365 = default   #protected bog
  drygrassland_370 = default   #protected dry grassland
  marshprotected_375 = default   #protected salt marshes
  lakesprotected_380 = default   #protected lakes
  lakes_440 = default   #lakes
  smallstreams_435 = default   #small streams (< 2.5 meter)
  mediumstreams_436 = default   #medium streams (2.5 - 12 meter)
  largestreams_437 = default   #large streams (> 12 meter)
  lakebuffer_420 = default   #lake buffer
  fields_1000 = default  #fields
  dikes_620 = default   #dikes
  archeological_625 = default   #archeological sites
  recreational_630 = default   #recreational areas
  hedgerows_635 = default   #hedgerows
  coppice_640 = default   #tree groups
  individualtrees_641 = default   #individual trees
  gravelpits_650 = default   #gravel pits
  ais_1100 = default   #ais landcover map

  #NB: these buffers are calculated automatically - mentioned here to keep track on codes
  #aarn425    #buffers small streams
  #aarn426    #buffers medium streams
  #aarn427    #buffers large streams

  print " "
  #===== End chunk: Landscape-specific setup =====#

#####################################################################################################

  try:

  #===== Chunk: Clipping =====#
  # from national data to a given study area
    print "Clipping national data to the study area extent"
    layers_list = [] #list of layers in nationalDB that is used later to check if such layer exist in test area
    for root, dirs, datasets in arcpy.da.Walk(nationalDB):
      for ds in datasets:
        print "Clipping " + ds + " ..."
        arcpy.Clip_analysis(os.path.join(root, ds), localSettings, gisDB + ds, "")
        layers_list.append(ds)
  #===== End: Clipping =====#

  #===== Chunk: Conversion =====#
  # from feature layers to raster
    
  # 1 - land and sea (land_hav)
    if landsea == 1:
      print "Processing base map (land/sea) ..."
      if arcpy.Exists(outPath + "landhav"):
        arcpy.Delete_management(outPath + "landhav")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("dkland_20l13_mp2sp").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("dkland_20l13_mp2sp", "ID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1") # here, land goes to 1, sea to NoData
        rasIsNull = IsNull(outPath + "tmpRaster") # here, NoData goes to 1, rest to 0
        rasTemp = Con(rasIsNull == 1, 0, 1) # here, sea goes to 0, rest to 1... then the 0 part is reset to external TOLE 80 by the reclassTable, which becomes ALMaSS TOLE 29 (saltwater)
        rasTemp.save(outPath + "landhav")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(mask, outPath + "landhav", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")

  ## THEME ROAD
    print "Processing ROAD THEME..."

  # 105 - slopes along larger roads (skrt105)
    if slopes_105 == 1:
      print "Processing artificial slopes along larger roads ..."
      if arcpy.Exists(outPath + "skrt105"):
        arcpy.Delete_management(outPath + "skrt105")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_skraent_preprocd").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_skraent_preprocd", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 105)
        rasTemp.save(outPath + "skrt105")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "skrt105", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "skrt105")

  # 110 - road verges (vejk110)
    if roadsideverge_110 == 1:
      print "Processing road verges ..."
      if arcpy.Exists(outPath + "vejk110"):
        arcpy.Delete_management(outPath + "vejk110")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_vejkant").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_vejkant","","1","")
        rasTemp = Con(eucDistTemp < 1.75, 1100, 10)
        rasTemp.save(outPath + "vejk110")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vejk110", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "vejk110")

  # 112 - paths (stie112)
    if paths_112 == 1:
      print "Processing paths  ..."
      if arcpy.Exists(outPath + "stie112"):
        arcpy.Delete_management(outPath + "stie112")
        print "... deleting existing raster"
      if int(arcpy.Exists("select_lyr")) == 1:
        arcpy.Delete_management("select_lyr")
      arcpy.MakeFeatureLayer_management("geodk_20d27_vejmidte_brudt", "select_lyr", "", "", "")
      arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", "(GEOMSTATUS = 'Endelig' AND VEJKLASSE = 'Cykelbane langs vej')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig' AND VEJKLASSE = 'Cykelsti langs vej')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig' AND VEJKLASSE = 'Cykelsti langs vej')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig' AND VEJKLASSE = 'Sti, diverse')")      
      arcpy.CopyFeatures_management("select_lyr", "veje_stier", "", "0", "0", "0")
      if int(arcpy.GetCount_management("veje_stier").getOutput(0))>0:
        eucDistTemp = EucDistance("veje_stier", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.51, 1120, 10)
        rasTemp.save(outPath + "stie112")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "stie112", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "stie112")

  # 114 - parking areas (park114)
    if parks_114 == 1:
      print "Processing parking areas ..."
      if arcpy.Exists(outPath + "park114"):
        arcpy.Delete_management(outPath + "park114")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_parkering").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_parkering", "", "1", "")
        rasTemp = Con(eucDistTemp < 3.0, 1140, 10)
        rasTemp.save(outPath + "park114")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "park114", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "park114")

  # 115 - dirt roads (spor115)
    if dirtroads_115 == 1:
      print "Processing dirt roads  ..."
      if arcpy.Exists(outPath + "spor115"):
        arcpy.Delete_management(outPath + "spor115")
        print "... deleting existing raster"
      if int(arcpy.Exists("select_lyr")) == 1:
        arcpy.Delete_management("select_lyr")
      arcpy.MakeFeatureLayer_management("geodk_20d27_vejmidte_brudt", "select_lyr", "", "", "")
      arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Ubef�stet' AND VEJKLASSE = 'Lokalvej-Sekund�r')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Ubef�stet' AND VEJKLASSE = 'Lokalvej-Terti�r')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Ubef�stet' AND VEJKLASSE = 'Anden vej')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Ubef�stet' AND VEJKLASSE = 'Indk�rselsvej')")      
      arcpy.CopyFeatures_management("select_lyr", "veje_spor", "", "0", "0", "0")
      if int(arcpy.GetCount_management("veje_spor").getOutput(0))>0:
        eucDistTemp = EucDistance("veje_spor", "", "1", "")
        rasTemp = Con(eucDistTemp < 2.25, 1150, 10)
        rasTemp.save(outPath + "spor115")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "spor115", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "spor115")

  # 120 - railway tracks (jern120)
    if railways_120 == 1:
      print "Processing railway tracks ..."
      if arcpy.Exists(outPath + "jern120"):
        arcpy.Delete_management(outPath + "jern120")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_jernbane_brudt").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_jernbane_brudt", "", "1", "")
        rasTemp = Con(eucDistTemp < 3.01, 1200, 10)
        rasTemp.save(outPath + "jern120")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "jern120", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "jern120")

   # 122 - Small roads (vu30122)
    if smallroads_122 == 1:
      print "Processing small roads  ..."
      if arcpy.Exists(outPath + "vu30122"):
        arcpy.Delete_management(outPath + "vu30122")
        print "... deleting existing raster"
      if int(arcpy.Exists("select_lyr")) == 1:
        arcpy.Delete_management("select_lyr")
      arcpy.MakeFeatureLayer_management("geodk_20d27_vejmidte_brudt", "select_lyr", "", "", "")
      arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Bef�stet' AND VEJKLASSE = 'Lokalvej-Sekund�r')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Bef�stet' AND VEJKLASSE = 'Lokalvej-Terti�r')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Bef�stet' AND VEJKLASSE = 'Anden vej')")
      arcpy.SelectLayerByAttribute_management("select_lyr", "ADD_TO_SELECTION", "(GEOMSTATUS = 'Endelig') AND (OVERFLADE = 'Bef�stet' AND VEJKLASSE = 'Indk�rselsvej')")      
      arcpy.CopyFeatures_management("select_lyr", "veje_vu30", "", "0", "0", "0")
      if int(arcpy.GetCount_management("veje_vu30").getOutput(0))>0:
        eucDistTemp = EucDistance("veje_vu30", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.75, 1220, 10)
        rasTemp.save(outPath + "vu30122")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vu30122", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "vu30122")

   # 125 - Intermediate sized roads (vu60125)
    if mediumroads_125 == 1:
      print "Processing medium sized roads ..."
      if arcpy.Exists(outPath + "vu60125"):
        arcpy.Delete_management(outPath + "vu60125")
        print "... deleting existing raster"
      if int(arcpy.Exists("select_lyr")) == 1:
        arcpy.Delete_management("select_lyr")
      arcpy.MakeFeatureLayer_management("geodk_20d27_vmbALL_preprocd", "select_lyr", "", "", "")
      arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", "(roadSet = 'vu60')")
      arcpy.CopyFeatures_management("select_lyr", "veje_vu60", "", "0", "0", "0")
      if int(arcpy.GetCount_management("veje_vu60").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("veje_vu60", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 10, 1250)
        rasTemp.save(outPath + "vu60125")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vu60125", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "vu60125")

   # 130 - Large roads (vu90130)
    if largeroads_130 == 1:
      print "Processing large roads ..."
      if arcpy.Exists(outPath + "vu90130"):
        arcpy.Delete_management(outPath + "vu90130")
        print "... deleting existing raster"
      if int(arcpy.Exists("select_lyr")) == 1:
        arcpy.Delete_management("select_lyr")
      arcpy.MakeFeatureLayer_management("geodk_20d27_vmbALL_preprocd", "select_lyr", "", "", "")
      arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", "(roadSet = 'vu90')")
      arcpy.CopyFeatures_management("select_lyr", "veje_vu90", "", "0", "0", "0")
      if int(arcpy.GetCount_management("veje_vu90").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("veje_vu90", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 10, 1300)
        rasTemp.save(outPath + "vu90130")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vu90130", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "vu90130")

  # 150 - pylons (hjsp150)
    if pylons_150 == 1:
      print "Processing pylons ..."
      if arcpy.Exists(outPath + "hjsp150"):
        arcpy.Delete_management(outPath + "hjsp150")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("hjspmast").getOutput(0))>0:
        eucDistTemp = EucDistance("hjspmast", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.5, 1500, 10)
        rasTemp.save(outPath + "hjsp150")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "hjsp150", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "hjsp150")

  # 155 - wind turbines (vind155)
    if windturbines_155 == 1:
      print "Processing wind turbines ..."
      if arcpy.Exists(outPath + "vind155"):
        arcpy.Delete_management(outPath + "vind155")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_vindmoelle").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_vindmoelle", "", "1", "")
        rasTemp = Con(eucDistTemp < 4.01, 1550, 10)
        rasTemp.save(outPath + "vind155")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vind155", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_road.append(outPath + "vind155")

  ## THEME BUILT-UP
    print "Processing BUILT-UP THEME..."

  # 205 - built area - low (lavb205)
    if builtuplow_205 == 1:
      print "Processing built areas (low) ..."
      if arcpy.Exists(outPath + "lavb205"):
        arcpy.Delete_management(outPath + "lavb205")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_lavbebyg").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_lavbebyg", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 205)
        rasTemp.save(outPath + "lavb205")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "lavb205", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "lavb205")

  # 210 - built area - high (hojb210)
    if builtuphigh_210 == 1:
      print "Processing built areas (high) ..."
      if arcpy.Exists(outPath + "hojb210"):
        arcpy.Delete_management(outPath + "hojb210")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_hojbebyg").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_hojbebyg", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 210)
        rasTemp.save(outPath + "hojb210")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "hojb210", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "hojb210")

  # 215 - city center - (byke215)
    if citycenter_215 == 1:
      print "Processing city center ..."
      if arcpy.Exists(outPath + "byke215"):
        arcpy.Delete_management(outPath + "byke215")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_bykerne").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_bykerne", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 215)
        rasTemp.save(outPath + "byke215")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "byke215", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "byke215")

  # 220 - industry (indu220)
    if industry_220 == 1:
      print "Processing industrial areas ..."
      if arcpy.Exists(outPath + "indu220"):
        arcpy.Delete_management(outPath + "indu220")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_erhverv").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_erhverv", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 220)
        rasTemp.save(outPath + "indu220")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "indu220", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "indu220")

  # 225 - churchyards (kirk225)
    if churchyard_225 == 1:
      print "Processing churchyards ..."
      if arcpy.Exists(outPath + "kirk225"):
        arcpy.Delete_management(outPath + "kirk225")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_begrav").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_begrav", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 225)
        rasTemp.save(outPath + "kirk225")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "kirk225", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "kirk225")

  # 250 - buildings (bygn250)
    if buildings_250 == 1:
      print "Processing buildings ..."
      if arcpy.Exists(outPath + "bygn250"):
        arcpy.Delete_management(outPath + "bygn250")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_bygning").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_bygning", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 250)
        rasTemp.save(outPath + "bygn250")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "bygn250", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_builtup.append(outPath + "bygn250")

  ## THEME NATURE
    print "Processing NATURE THEME..."

  # 310 - forests (skov310)
    if forests_310 == 1:
      print "Processing forests ..."
      if arcpy.Exists(outPath + "skov310"):
        arcpy.Delete_management(outPath + "skov310")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_skov").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_skov", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 310)
        rasTemp.save(outPath + "skov310")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "skov310", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "skov310")
      
  # 315 - shrubs  (krat315)
    if shrubs_315 == 1:
      print "Processing shrubs ..."
      if arcpy.Exists(outPath + "krat315"):
        arcpy.Delete_management(outPath + "krat315")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_krat_bev").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_krat_bev", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 315)
        rasTemp.save(outPath + "krat315")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "krat315", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "krat315")
      
  # 320 - sand flat - mainly beaches (sand320)
    if sand_320 == 1:
      print "Processing sand flats ..."
      if arcpy.Exists(outPath + "sand320"):
        arcpy.Delete_management(outPath + "sand320")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_sand_beach").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_sand_beach", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 320)
        rasTemp.save(outPath + "sand320")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "sand320", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "sand320")
      
  # 325 - heath land (hede325)
    if heathland_325 == 1:
      print "Processing heath land ..."
      if arcpy.Exists(outPath + "hede325"):
        arcpy.Delete_management(outPath + "hede325")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_hede").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_hede", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 325)
        rasTemp.save(outPath + "hede325")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "hede325", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "hede325")
      
  # 330 - wetland (vaad330)
    if wetland_330 == 1:
      print "Processing wetland ..."
      if arcpy.Exists(outPath + "vaad330"):
        arcpy.Delete_management(outPath + "vaad330")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_vaad").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_vaad", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 330)
        rasTemp.save(outPath + "vaad330")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "vaad330", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_nature.append(outPath + "vaad330")

  # protected areas
    print "Processing protected areas ..."
    if int(arcpy.GetCount_management("BES_NATURTYPER").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("BES_NATURTYPER", "selectlyr")

  # 355 - protected meadows (eng355)
      if meadowprotected_355 == 1:
        print "Processing protected meadows ..."
        if arcpy.Exists(outPath + "eng355"):
          arcpy.Delete_management(outPath + "eng355")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "eng355", "(Natyp_kode = 1)")
        if int(arcpy.GetCount_management("eng355").getOutput(0))>0:
          eucDistTemp = EucDistance("eng355","","1","")
          rasTemp = Con(eucDistTemp < 3, 355, 1)
          rasTemp.save(outPath + "eng355")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "eng355", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_nature.append(outPath + "eng355")
      
  # 360 - protected heath land (hede360)
      if heathlandprotected_360 == 1:
        print "Processing protected heath land ..."
        if arcpy.Exists(outPath + "hede360"):
          arcpy.Delete_management(outPath + "hede360")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "hede360", "(Natyp_kode = 2)")
        if int(arcpy.GetCount_management("hede360").getOutput(0))>0:
          eucDistTemp = EucDistance("hede360","","1","")
          rasTemp = Con(eucDistTemp < 3, 360, 1)
          rasTemp.save(outPath + "hede360")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "hede360", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_nature.append(outPath + "hede360")

  # 370 - protected dry grassland 3070 (over370)
      if drygrassland_370 == 1:
        print "Processing protected dry grassland ..."
        if arcpy.Exists(outPath + "over370"):
          arcpy.Delete_management(outPath + "over370")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "over370", "(Natyp_kode = 4)")
        if int(arcpy.GetCount_management("over370").getOutput(0))>0:
          eucDistTemp = EucDistance("over370","","1","")
          rasTemp = Con(eucDistTemp < 3, 370, 1)
          rasTemp.save(outPath + "over370")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "over370", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_nature.append(outPath + "over370")
      
  # 375 - protected marsh 3075 (seng375)
      if marshprotected_375 == 1:
        print "Processing protected marsh ..."
        if arcpy.Exists(outPath + "seng375"):
          arcpy.Delete_management(outPath + "seng375")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "seng375", "(Natyp_kode = 5)")
        if int(arcpy.GetCount_management("seng375").getOutput(0))>0:
          eucDistTemp = EucDistance("seng375","","1","")
          rasTemp = Con(eucDistTemp < 3, 375, 1)
          rasTemp.save(outPath + "seng375")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "seng375", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_nature.append(outPath + "seng375")
    
  ## THEME WETNATURE
      print "Processing WETNATURE THEME..." # N.B. "selectlyr" has been set under Theme Nature above

  # 365 - protected swamp 3065 (mose365)
      if bog_365 == 1:
        print "Processing protected swamp ..."
        if arcpy.Exists(outPath + "mose365"):
          arcpy.Delete_management(outPath + "mose365")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "mose365", "(Natyp_kode = 3)")
        if int(arcpy.GetCount_management("mose365").getOutput(0))>0:
          eucDistTemp = EucDistance("mose365","","1","")
          rasTemp = Con(eucDistTemp < 3, 365, 1)
          rasTemp.save(outPath + "mose365")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "mose365", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_wetnature.append(outPath + "mose365")

  # 380 - protected lakes 3080 (soe380)
      if lakesprotected_380 == 1:
        print "Processing protected lakes ..."
        if arcpy.Exists(outPath + "soe380"):
          arcpy.Delete_management(outPath + "soe380")
          print "... deleting existing raster"
        arcpy.Select_analysis("selectlyr", "soe380", "(Natyp_kode = 6)")
        if int(arcpy.GetCount_management("soe380").getOutput(0))>0:
          eucDistTemp = EucDistance("soe380","","1","")
          rasTemp = Con(eucDistTemp < 1, 380, 1)
          rasTemp.save(outPath + "soe380")
        else:
          print "No such features in the area"
          arcpy.CopyRaster_management(outPath + "landhav", outPath + "soe380", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
        rasterList_wetnature.append(outPath + "soe380")

  ## THEME WATER
    print "Processing WATER THEME..."

  # 440 - lakes (soer440)
    if lakes_440 == 1:
      print "Processing lakes ..."
      if arcpy.Exists(outPath + "soer440"):
        arcpy.Delete_management(outPath + "soer440")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_soe").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_soe", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 440)
        rasTemp.save(outPath + "soer440")
        # arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "soer440", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")   
      rasterList_water.append(outPath + "soer440")

  # 425/435 - Small streams (0-2.5) (vandloeb_brudt)+ buffer  OBS:  remember to use 'ukendte'
    if smallstreams_435 == 1:
      print "Processing small streams (0 - 2.5 meter)"
      if arcpy.Exists(outPath + "aaer435"):
        arcpy.Delete_management(outPath + "aaer435")
        print "... deleting existing raster"
      if arcpy.Exists(outPath + "aaer425"):
        arcpy.Delete_management(outPath + "aaer425")
        print "... deleting existing raster"
      arcpy.Select_analysis("geodk_20d27_vlm_brudt", "aaer415", "(VANDL_TYPE = 'Almindelig') AND (MIDTBREDDE = '0-2.5' OR MIDTBREDDE = 'Ukendt')")
      if int(arcpy.GetCount_management("aaer415").getOutput(0))>0:
        eucDistTemp = EucDistance("aaer415","","1","")
        rasTemp = Con(eucDistTemp < 0.95, 435, 1)
        rasTemp.save(outPath + "aaer435")
        rasTemp = Con(eucDistTemp < 2.01, 425, 1)
        rasTemp.save(outPath + "aaer425")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer425", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer435", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "aaer425")
      rasterList_water.append(outPath + "aaer435")

  # 426/436 - medium streams (2.5-12) (vandloeb_brudt)+ buffer
    if mediumstreams_436 == 1:
      print "Processing medium streams (2.5 - 12 meter)"
      if arcpy.Exists(outPath + "aaer436"):
        arcpy.Delete_management(outPath + "aaer436")
        print "... deleting existing raster"
      if arcpy.Exists(outPath + "aaer426"):
        arcpy.Delete_management(outPath + "aaer426")
        print "... deleting existing raster"
      arcpy.Select_analysis("geodk_20d27_vlm_brudt", "aaer416", "(VANDL_TYPE = 'Almindelig') AND (MIDTBREDDE = '2.5-12')")
      if int(arcpy.GetCount_management("aaer416").getOutput(0))>0:
        eucDistTemp = EucDistance("aaer416","","1","")
        rasTemp = Con(eucDistTemp < 5, 436, 1)
        rasTemp.save(outPath + "aaer436")
        rasTemp = Con(eucDistTemp < 7, 426, 1)
        rasTemp.save(outPath + "aaer426")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer426", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer436", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "aaer426")
      rasterList_water.append(outPath + "aaer436")

  # 427/437 - large streams (> 12 meter) (vandloeb_brudt)+ buffer
    if largestreams_437 == 1:
      print "Processing large streams (> 12 meter)"
      if arcpy.Exists(outPath + "aaer437"):
        arcpy.Delete_management(outPath + "aaer437")
        print "... deleting existing raster"
      if arcpy.Exists(outPath + "aaer427"):
        arcpy.Delete_management(outPath + "aaer427")
        print "... deleting existing raster"
      arcpy.Select_analysis("geodk_20d27_vlm_brudt", "aaer417", "(VANDL_TYPE = 'Almindelig') AND (MIDTBREDDE = '12-')")
      if int(arcpy.GetCount_management("aaer417").getOutput(0))>0:
        eucDistTemp = EucDistance("aaer417","","1","")
        rasTemp = Con(eucDistTemp < 5, 437, 1)
        rasTemp.save(outPath + "aaer437")
        rasTemp = Con(eucDistTemp < 7, 427, 1)
        rasTemp.save(outPath + "aaer427")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer427", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "aaer437", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "aaer427")
      rasterList_water.append(outPath + "aaer437")

  # 420 - lake buffer zones (sorn420)
    if lakebuffer_420 == 1:
      print "Processing lake buffer zones  ..."
      if arcpy.Exists(outPath + "sorn420"):
        arcpy.Delete_management(outPath + "sorn420")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_soe").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_soe","","1","")
        rasTemp = Con(eucDistTemp < 2.05, 420, 1)
        rasTemp.save(outPath + "sorn420")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "sorn420", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "sorn420")

  ## THEME CULTURAL
    print "Processing CULTURAL THEME..."

  # 620 - dikes (dige620)
    if dikes_620 == 1:
      print "Processing dikes ..."
      if arcpy.Exists(outPath + "dige620"):
        arcpy.Delete_management(outPath + "dige620")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_dige").getOutput(0))>0:
        eucDistTemp = EucDistance("geodk_20d27_dige", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.2, 620, 1)
        rasTemp.save(outPath + "dige620")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "dige620", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "dige620")
      
  # 625 - archeological sites (fred625)
    if archeological_625 == 1:
      print "Processing archeological landscape features ..."
      if arcpy.Exists(outPath + "fred625"):
        arcpy.Delete_management(outPath + "fred625")
        print "... deleting existing raster"
      arcpy.Select_analysis("augisArcheo_anlaeg", "archeo_sel_625", "(frednr <>'' AND anlaegsbet = 'Rundh�j')")
      if int(arcpy.GetCount_management("archeo_sel_625").getOutput(0))>0:
        eucDistTemp = EucDistance("archeo_sel_625", "", "1", "")
        rasTemp = Con(eucDistTemp < 8.05, 625, 1)
        rasTemp.save(outPath + "fred625")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "fred625", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "fred625")
      
  # 630 - recreational areas (rekr630)
    if recreational_630 == 1:
      print "Processing recreational areas ..."
      if arcpy.Exists(outPath + "rekr630"):
        arcpy.Delete_management(outPath + "rekr630")
        print "... deleting existing raster"
      arcpy.Select_analysis("geodk_20d27_teknisk", "teknisk_sel_630", "(AREALTYPE = 'Sportsanlaeg')")
      if int(arcpy.GetCount_management("teknisk_sel_630").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("teknisk_sel_630", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 630)
        rasTemp.save(outPath + "rekr630")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "rekr630", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "rekr630")

  # 635 - hedgerows (hegn635)
    if hedgerows_635 == 1:
      print "Processing hedgerows..."
      if arcpy.Exists(outPath + "hegn635"):
        arcpy.Delete_management(outPath + "hegn635")
        print "... deleting existing raster"
      arcpy.Select_analysis("geodk_20d27_hegn", "hegn_sel_635", "(HEGNSTYPE = 'Levende')")
      if int(arcpy.GetCount_management("hegn_sel_635").getOutput(0))>0:
        eucDistTemp = EucDistance("hegn_sel_635", "", "1", "")
        rasTemp = Con(eucDistTemp < 2, 635, 1)
        rasTemp.save(outPath + "hegn635")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "hegn635", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "hegn635")

  # 640 - copse/tree groups (trae640)
    if coppice_640 == 1:
      print "Processing tree groups ..."
      if arcpy.Exists(outPath + "trae640"):
        arcpy.Delete_management(outPath + "trae640")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("traegruppe").getOutput(0))>0:
        eucDistTemp = EucDistance("traegruppe", "", "1", "")
        rasTemp = Con(eucDistTemp < 8, 640, 1)
        rasTemp.save(outPath + "trae640")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "trae640", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "trae640")
      
  # 641 - individual trees (trae641)
    if individualtrees_641 == 1:
      print "Processing individual trees ..."
      if arcpy.Exists(outPath + "trae641"):
        arcpy.Delete_management(outPath + "trae641")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("trae").getOutput(0))>0:
        eucDistTemp = EucDistance("trae", "", "1", "")
        rasTemp = Con(eucDistTemp < 4, 641, 1)
        rasTemp.save(outPath + "trae641")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "trae641", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "trae641")
      
  # 650- gravel pits (raas650)
    if gravelpits_650 == 1:
      print "Processing gravel pits ..."
      if arcpy.Exists(outPath + "raas650"):
        arcpy.Delete_management(outPath + "raas650")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("geodk_20d27_raastof").getOutput(0))>0:
        arcpy.PolygonToRaster_conversion("geodk_20d27_raastof", "FEAT_KODE", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        rasTemp = Con(rasIsNull == 1, 1, 650)
        rasTemp.save(outPath + "raas650")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "raas650", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "raas650")


  # mark1000 plus - converts field polygons and assign each polygon a unique id
    if fields_1000 == 1:
      print "Processing field polygons ..."
      if arcpy.Exists(outPath + "fields1000"):
        arcpy.Delete_management(outPath + "fields1000")
        print "... deleting existing raster"
      if int(arcpy.GetCount_management("Marker_2019_CVR_fAUdisk").getOutput(0))>0:
        arcpy.AddField_management("Marker_2019_CVR_fAUdisk", "gridcode", "SHORT")
        cursor = arcpy.UpdateCursor("Marker_2019_CVR_fAUdisk")
        i = 1000
        for row in cursor:
          row.setValue('gridcode', i)   # setting gridcode
          i = i+1
          cursor.updateRow(row)
            
        arcpy.PolygonToRaster_conversion("Marker_2019_CVR_fAUdisk", "gridcode", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasIsNull = IsNull(outPath + "tmpRaster")
        markNummerFirst = (Plus(outPath + "tmpRaster", 0))
        markNummer = Int(markNummerFirst)
        rasTemp = Con(rasIsNull == 1, 1, markNummer)
        rasTemp.save(outPath + "fields1000")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "landhav", outPath + "fields1000", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")

    rasTemp = ''
    print " "
  #===== End Chunk: Conversion =====#

  #===== Chunk: Themes =====#
  # Combine rasters to thematic maps 

    if road == 1:   #Assembles a transportation theme for roads and road verges
      print "Processing road theme ..."
      if arcpy.Exists(outPath + "T1_road"):
        arcpy.Delete_management(outPath + "T1_road")
        print "... deleting existing raster"
      print rasterList_road
      rasTemp = (CellStatistics(rasterList_road, "MAXIMUM", "DATA") / 10)
      rasTemp.save (outPath + "T1_road")

    if builtup == 1:   #Assembles a built up theme
      print "Processing built up theme..."
      if arcpy.Exists(outPath + "T2_building"):
        arcpy.Delete_management(outPath + "T2_building")
        print "... deleting existing raster"
      print rasterList_builtup
      rasTemp = CellStatistics(rasterList_builtup, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T2_building")

    if nature == 1:   #Assembles a natural areas theme
      print "Processing natural areas ..."
      if arcpy.Exists(outPath + "T3_nature"):
        arcpy.Delete_management(outPath + "T3_nature")
        print "... deleting existing raster"
      print rasterList_nature
      rasTemp = CellStatistics(rasterList_nature, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T3_nature")

    if wetnature == 1:   #Assembles a 'wet nature' theme
      print "Processing wet natural areas ..."
      if arcpy.Exists(outPath + "T3_wetnature"):
        arcpy.Delete_management(outPath + "T3_wetnature")
        print "... deleting existing raster"
      print rasterList_wetnature
      rasTemp = CellStatistics(rasterList_wetnature, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T3_wetnature")

    if water == 1:   #Assembles a fresh water theme
      print "Processing streams and lakes ..."
      if arcpy.Exists(outPath + "T4_water"):
        arcpy.Delete_management(outPath + "T4_water")
        print "... deleting existing raster"
      print rasterList_water
      rasTemp = CellStatistics(rasterList_water, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T4_water")

    if cultural == 1:   # Assembles a theme of cultural features
      print "Processing hedgerows, dikes, trees, etc ..."
      if arcpy.Exists(outPath + "T5_culture"):
        arcpy.Delete_management(outPath + "T5_culture")
        print "... deleting existing raster"
      print rasterList_cultural
      rasTemp = CellStatistics(rasterList_cultural, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T5_culture")
  #===== End Chunk: Themes =====#

  #===== Chunk: Stack =====#
    # Assemble the final map
    # First delete any existing layers
    if finalmap == 1:   
      print "Processing mosaic for all themes ..."
      if arcpy.Exists(outPath + "MapRaw"):
        arcpy.Delete_management(outPath + "MapRaw")
        print "... deleting existing raster"

        print " "

      # Stack the thematic maps 
      # Here the hierarchy of individual themes is determined
      field = Raster(outPath + "fields1000")   # fields
      T4va = Raster(outPath + "T4_water")
      T3na = Raster(outPath + "T3_nature")
      T2be = Raster(outPath + "T2_building")
      T3ana = Raster(outPath + "T3_wetnature")
      T5ku = Raster(outPath + "T5_culture")
      T1ve = Raster(outPath + "T1_road")
      buildings_250 = Raster(outPath + "bygn250")
      landsea = Raster(outPath + "landhav")

      step1 = Con(field > 999, field, 1)                    # fields first
      print "fields added to map ..."
      step2 = Con(T4va == 1, step1, T4va)                   # freshwater on top
      print "fresh water added to map ..."
      step3 = Con(step2 == 1, T3na, step2)                  # natural areas on NOT (fields, water)
      print "natural areas added to map ..."
      step4 = Con(step3 == 1, T2be, step3)                  # built up areas on NOT (fields, water, natural areas)
      print "built up areas added to map ..."
      step4a = Con(T3ana == 1, step4, T3ana)                # wet natural areas on top
      print "wet natural areas added to map  ..."
      step5 = Con(T5ku == 1, step4a, T5ku)                  # cultural features on top
      print "cultural landscape features added to map ..."
      step6 = Con(T1ve == 1, step5, T1ve)                   # roads on top
      print "roads added to map ..."
      step7 = Con(buildings_250 == 1, step6, buildings_250) # buildings on top
      print "buildings added to map ..."
      map01 = Con(landsea == 0, 0, step7)                   # sea added
      print "sea added to map ..."
      map02 = Int(map01)
      map02.save (outPath + "MapRaw")
      arcpy.BuildRasterAttributeTable_management(outPath + "MapRaw", "Overwrite")
      nowTime = time.strftime('%X %x')
      print "Raw landscape map created ..."
      print "Processing of landscape " + landscapes[index] + " done ..." + nowTime
      print "  "
        
  #===== End Chunk: Stack =====#

  except:
    if arcpy.Exists(outPath + "tmpRaster"):
      arcpy.Delete_management(outPath + "tmpRaster")
    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print msgs
    print pymsg

    arcpy.AddMessage(arcpy.GetMessages(1))
    print arcpy.GetMessages(1)
