#=======================================================================================================================
### Name: lsgenDK_part2_21c04
### Parent : lsgenDK_part2_forLCT_part3Dev_21b22
### Purpose: The script cleans up the raw landscape map from sliver polygons and creates ALMaSS inputs
### Authors: Elzbieta Ziolkowska - June 2017; modified by Geoff Groom 2019 - 2021
### Last large update: January 2019
### Passes for use with ArcGis 10.4, 10.6, 10.8
### 19b15 : Geoff : Drive set as a variable
### 19c02 : Geoff : Export of regionALM as tiff
### 19e02 : Geoff : Ln350 : Reset as <drive> of output folder for regionALM as tif
### 20i23 : edits made to test LSgen with the new BES Nature gis layers for Theme 5ku (culture) added
###
### script update specific notes :
### 21b18 : reclass table reset to be reclasstable_dk_21b18.txt
### 21b22 : setup to run with the latest version of the marker data
###         addition of outCon.save(scratchDB + "outCon") in order to get zonalStatistics to work
###
#===== Chunk: General setup =====#

### Import system modules
import arcpy, traceback, sys, time, gc, shutil, os, csv
from dbfpy import dbf
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part2 started: " + nowTime
print "... system modules checked"

### Defining all neccesary functions

# Defining dbf to csv conversion function
def dbf_to_csv(out_table):#Input a dbf, output a csv
    csv_fn = out_table[:-4]+ ".csv" #Set the table as .csv format
    with open(csv_fn,'wb') as csvfile: #Create a csv file and write contents from dbf
        in_db = dbf.Dbf(out_table)
        out_csv = csv.writer(csvfile)
        names = []
        for field in in_db.header.fields: #Write headers
            names.append(field.name)
        out_csv.writerow(names)
        for rec in in_db: #Write records
            out_csv.writerow(rec.fieldData)
        in_db.close()
    return csv_fn

### Set drive
#drive = "O:\ST_ALMaSS-LS19\CORE\DK_30lsgen_19d25\DK_30lsgen_19d25_WorkingModel"
drive = "C:/ST_ALMaSS-LS19/TESTS/fullTests_20e20/_DK_/_DK_20i23_dkgis20/30lsgen/8_forLCT_part3Dev/8_forLCT_part3Dev_21b09"  ### EDIT THIS !

### Set fixed paths
dst = (drive + "/test_areas/")  # Output destination
nationalDB = (drive + "/data/dk_newgis_part1_8.gdb")  # Geodatabase with feature layers for DK
reclasstable = (drive + "/test_areas/reclasstable_dk_21b18.txt")     # Reclass ascii table
soilraster = (drive + "/data/Jordarter.gdb/soilcode10")      # Soil data

# We need to set these each time we loop through otherwise we recycle the ones from the previous run  
defaultextent = arcpy.env.extent
defaultmask = arcpy.env.mask

### Setting a list with all the landscapes to process
#landscapes = ["WN", "WS", "EN", "ES"]
landscapes = ["WS"]                                                                                                         ### EDIT THIS !

#===== End chunk: General setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
    print "Processing landscape " + landscapes[index] + " ..."

    #===== Chunk: Landscape-specific setup =====#
    
    arcpy.env.extent = defaultextent
    arcpy.env.mask = defaultmask

    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    outPath = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
    gisDB = os.path.join(dst, landscapes[index], landscapes[index] + ".gdb/")
    scratchDB = os.path.join(dst, landscapes[index], "outputs/scratch.gdb/")  # scratch folder for tempfiles
    projectDB = os.path.join(dst, landscapes[index], "outputs/project.gdb/")  # scratch folder for tempfiles
    #localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")   # project folder with mask

    # SET A VARIABLE TO THE 200M BUFFERED EXTENT WITH SQUARE CORNERS AND USE THAT AS LOCAL SETTINGS ...
    out_feature_buf200m_envlp = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask_buf200m_envlp") # this is a polygon feature class
    localSettings = out_feature_buf200m_envlp # is a vector polygon

    # SET A VARIABLE FOR THE ACTUAL LS WINDOW EXTENT, TO USE TO CLIP MAP_FINAL_VECTOR IMMEDIATELY BEFORE THE POLY-TO-RAS IN STEP-6
    final_clip_extent = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask") # this is a polygon feature class
        
    asciifile = "ASCII_" + landscapes[index] + ".txt"
    asciiexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", asciifile)  # export in ascii (for ALMaSS)
    attrtable = "ATTR_" + landscapes[index] + ".csv"  # Name of attribute table
    attrexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", attrtable)  # full path 
    soiltable = "SOIL_" + landscapes[index] + ".csv"  # Name of soil type table
    soilexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", soiltable)  # full path
    fieldstable = "FIELDS_" + landscapes[index] + ".dbf"  # Name of fields table
    fieldsexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", fieldstable)  # full path 

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"
    print " "

    #===== End chunk: Landscape-specific setup =====#

    try:

    #===== Chunk: Elimination of sliver polygons =====#
        print "Eliminating sliver polygons ..."

    #step0
        print "Step 0: Removing water from the process ..." # NW means NoWater !
        #map0_NW = SetNull(outPath + "MapRaw", outPath + "MapRaw", "VALUE IN (0, 380, 440, 435, 436, 437, 620)") # 620 ("dige") are banks in DK, not water channels!
        map0_NW = SetNull(outPath + "MapRaw", outPath + "MapRaw", "VALUE IN (0, 380, 440, 435, 436, 437)")
        map0_NW.save(scratchDB + "map0_NW") # CHECK AND EDIT THIS WRT CONTENT OF map0_NW
        arcpy.RasterToPolygon_conversion(map0_NW, scratchDB + "map0_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 0 finished ..."

    #step1: checking if any big background polygons (> 1ha) exists and if so convert them into fields
        print "Step 1: Checking for big (>1ha) background polygons ..."
        arcpy.Select_analysis("ais100", scratchDB + "AIS_arable_land", """"LUATYPE" = 2112""")
        arcpy.PolygonToRaster_conversion(scratchDB + "AIS_arable_land", "LUATYPE", scratchDB + "AIS_arable_land_Raster", "CELL_CENTER", "NONE", "1") # AIS arable land cell get cell value 2112
        outCon = Con(IsNull(Raster(scratchDB + "AIS_arable_land_Raster")), 0, 1) # AIS arable land cells get cell value 1, rest value 0
        outCon.save(scratchDB + "outCon")

        map0_NW_MAXResult = arcpy.GetRasterProperties_management(scratchDB + "map0_NW", "MAXIMUM") # find max cell value of the "not-water" parts ... will be a farmfield ...
        map0_NW_MAX = map0_NW_MAXResult.getOutput(0)

        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map0_NW_lyr") # vector version was made in step0
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "NEW_SELECTION", "gridcode = 1") # select for polys without a proper gridcode, i.e. a proper TOLE ...
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection")
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "SUBSET_SELECTION", "(Shape_Area >= 10000) AND (Shape_Area/Shape_Length>5)") # and filter them by geometry ...
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection2")

        if int(arcpy.GetCount_management(scratchDB + "map0_NW_selection2").getOutput(0))>0:
            ZonalStatisticsAsTable(scratchDB + "map0_NW_selection2", "Id", scratchDB + "outCon", scratchDB + "map0_NW_table", "NODATA", "ALL") # Evalualte each field on the selected polygons wrt stats of the proportion of the polygon that is AIS arable land in them ...
            arcpy.JoinField_management (scratchDB + "map0_NW_vector", "Id", scratchDB + "map0_NW_table", "Id", ["MEAN"]) # focus on the MEAN value ...
            cursor = arcpy.UpdateCursor(scratchDB + "map0_NW_vector")
            i = 1
            for row in cursor:
                if row.getValue('MEAN')>=0.8: # i.e. use the MEAN from above as a filter ...
                    print int(map0_NW_MAX) + i
                    row.setValue('gridcode', int(map0_NW_MAX) + i)   # setting gridcode for a new field
                    i = i+1
                cursor.updateRow(row)

        # Updating field1000 layer and adding info on farm type (all new fields belongs to one big farm of most common type 1), etc.
        arcpy.Update_analysis("Marker_2019_CVR_fAUdisk", scratchDB + "map0_NW_selection2", scratchDB + "FIELDS_" + landscapes[index], "BORDERS") # scratchDB + FIELDS_ is the output !

        # Selecting and feeding to a separate feature class that same select set of features from (scratchDB + "FIELDS_" + landscapes[index])
        # and using negBuf, Buf, clip to remove the long thin tails that some of these features have have...
        arcpy.Select_analysis(scratchDB + "FIELDS_" + landscapes[index], scratchDB + "newfarmFields_before", "(FarmType = 0)" and "(AFGKODE = 0)")
        if int(arcpy.Exists("select_lyr")) == 0:
            arcpy.Delete_management("select_lyr")
        arcpy.MakeFeatureLayer_management(scratchDB + "FIELDS_" + landscapes[index], "select_lyr", "", "", "")
        arcpy.SelectLayerByAttribute_management("select_lyr", "NEW_SELECTION", ("FarmType = 0" and "AFGKODE = 0"))
        arcpy.SelectLayerByAttribute_management("select_lyr", "SWITCH_SELECTION")
        arcpy.CopyFeatures_management("select_lyr", scratchDB + "old_farmFields")
        arcpy.RepairGeometry_management(scratchDB + "newfarmFields_before", "DELETE_NULL")
        # the -10 m buf is done as two -5 m buffer operations as the single - 10 m buffer can be associated with a crash ...
        arcpy.Buffer_analysis(scratchDB + "newfarmFields_before", scratchDB + "newfarmFields_before_negBuf5m", "-5 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")
        arcpy.Buffer_analysis(scratchDB + "newfarmFields_before_negBuf5m", scratchDB + "newfarmFields_before_negBuf10m", "-5 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")
        arcpy.Buffer_analysis(scratchDB + "newfarmFields_before_negBuf10m", scratchDB + "newfarmFields_before_negBuf10m_buf12m", "12 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")
        arcpy.Clip_analysis(scratchDB + "newfarmFields_before_negBuf10m_buf12m", scratchDB + "newfarmFields_before", scratchDB + "newfarmfields_negBuf10m_buf12m_clip", "")
        arcpy.Delete_management(scratchDB + "FIELDS_" + landscapes[index])
        mergeFeatures = [scratchDB + "old_farmFields", scratchDB + "newfarmfields_negBuf10m_buf12m_clip"]
        arcpy.Merge_management(mergeFeatures, scratchDB + "FIELDS_" + landscapes[index])
        arcpy.Select_analysis(scratchDB + "FIELDS_" + landscapes[index], scratchDB + "newfarmFields_after", "(FarmType = 0)" and "(AFGKODE = 0)")

        cursor = arcpy.UpdateCursor(scratchDB + "FIELDS_" + landscapes[index])
        j=1
        for row in cursor:
            if ((row.getValue('FarmType')==0) and (row.getValue('AFGKODE')==0)): # New criteria rule, by Geoff, 21b09
                row.setValue('FarmType', 1)   # setting FarmType for a new field
                row.setValue('gridcode', int(map0_NW_MAX) + j)   # setting gridcode for a new field
                row.setValue('ALMaSSEle', 35)   # setting ALMaSScode for a new field ; modified from 20 by Geoff, 21b09
                row.setValue('FARM_ID', 10000) # Added by Geoff
                j=j+1
                cursor.updateRow(row)
        
        # Exporting field1000 i.e. scratchDB + "FIELDS_" + landscapes[index], to shp in final output location
        arcpy.FeatureClassToShapefile_conversion(scratchDB + "FIELDS_" + landscapes[index], os.path.join(dst, landscapes[index], "outputs/ALMaSS_files")) # this makes the FIELDS_xxx shape in the /ALMaSS_files folder !
        dbf_to_csv(fieldsexp) # this makes FIELDS_xxx.csv in the /ALMaSS_files folder !

        arcpy.PolygonToRaster_conversion(scratchDB + "map0_NW_vector", "gridcode", scratchDB + "map1_NW_Raster", "", "", 1)
        arcpy.Delete_management(outPath + "AIS_arable_land_Raster")
        print "Step 1 finished ..."

    #step2: removing elongated background polygons
        print "Step 2: Removing elongated background polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map1_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map1_NW_lyr", scratchDB + "map1_NW_selection")
        
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map1_NW_selection", scratchDB + "map1_NW_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map1_NW_selection", "Id", scratchDB + "map1_NW_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map1_NW_selection", "map1_NW_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4 AND Shape_Area < 1500")
        arcpy.PolygonToRaster_conversion("map1_NW_selection_lyr", "gridcode", scratchDB + "map1_NW_selection_lyr_raster", "", "", 1)
        Reclass2 = Con(IsNull(scratchDB + "map1_NW_selection_lyr_raster"), 2, 1)
        toNibble2 = SetNull(Reclass2, 2, "Value = 1")
        map2_NW = Nibble(scratchDB + "map1_NW_Raster", toNibble2, "DATA_ONLY")
        map2_NW.save(scratchDB + "map2_NW")
        arcpy.RasterToPolygon_conversion(map2_NW, scratchDB + "map2_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 2 finished ..."

    #step3: removing small parts of multi-divided fields
        print "Step 3: Checking for small parts of multi-divided fields ..."
        cursor=arcpy.SearchCursor(scratchDB + "map2_NW_vector")
        fields_list=[]
        duplicates=[]
        for row in cursor:
          a=row.getValue("gridcode")
          if a>=1000:
            if a in fields_list:
              duplicates.append(a)
            else:
              fields_list.append(a)

        if len(duplicates)>0:
            print "multi-divided fields found ..."
            whereClause = "VALUE IN (" + str(duplicates)[1:-1] + ")"
            toCheck3 = Con(scratchDB + "map2_NW", scratchDB + "map2_NW", 0, whereClause)
            regGr3 = RegionGroup(toCheck3, "FOUR", "WITHIN", "", 0)
            toNibble3 = SetNull(regGr3, 1, "COUNT < 1000")
            map3_NW = Nibble(scratchDB + "map2_NW", toNibble3, "DATA_ONLY")
            map3_NW.save(scratchDB + "map3_NW")
            arcpy.RasterToPolygon_conversion(map3_NW, scratchDB + "map3_NW_vector", "NO_SIMPLIFY", "VALUE")   
            print "small multi-divided fields removed ..."    
        else:
            print "no multi-divided fields found ..."
            arcpy.CopyRaster_management(scratchDB + "map2_NW", scratchDB + "map3_NW")
            arcpy.CopyFeatures_management(scratchDB + "map2_NW_vector", scratchDB + "map3_NW_vector")
        print "Step 3 finished ..."

    #step4: adding the inland water and wet parts back
        print "Step 4: Adding water ..."
        map4 = Con(IsNull(scratchDB + "map0_NW"), Raster(outPath + "MapRaw"), Raster(scratchDB + "map3_NW"))
        map4.save(scratchDB + "map4")
        # assigning riverside plants to all gaps, i.e., 'NODATA' values
        map4_fill = Con(IsNull(map4), 520, map4) # setting new category 'river side plants' TO CHECK!!!
        map4_fill.save(scratchDB + "map4_fill")
        arcpy.RasterToPolygon_conversion(map4_fill, scratchDB + "map4_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 4 finished ..."

    #step5: filling the background gaps
        print "Step 5: Filling the remaining gaps ..."
        gridcodeValues = []
        cursor = arcpy.SearchCursor(scratchDB + "map4_vector")
        for row in cursor:
            gridcodeValues.append(row.getValue('gridcode'))

        if 1 in gridcodeValues:
            print "Still some background pixels left ..."
            arcpy.PolygonNeighbors_analysis(scratchDB + "map4_vector", scratchDB + "map4_vector_neighbors")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors", "src_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors", scratchDB + "map4_vector_neighbors_selection", "gridcode = 1")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors_selection", "nbr_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors_selection", scratchDB + "map4_vector_neighbors_selection2", "gridcode_1 >= 205 AND gridcode_1 <= 215")
            arcpy.JoinField_management (scratchDB + "map4_vector", "OBJECTID", scratchDB + "map4_vector_neighbors_selection2", "src_OBJECTID")
            cursor = arcpy.UpdateCursor(scratchDB + "map4_vector")
            for row in cursor:
                if row.getValue('gridcode')==1:
                    if row.getValue('src_OBJECTID')>0:
                        row.setValue('gridcode', 200)   # setting category 'settlement area with vegetation' to selected background objects
                    else:
                        row.setValue('gridcode', 290)   # setting category 'wasteland' to the rest of 'background' objects
                cursor.updateRow(row)
        else:
            print "No background pixels found ..."
        arcpy.PolygonToRaster_conversion(scratchDB + "map4_vector", "gridcode", scratchDB + "map5", "", "", 1)
        map5 = Raster(scratchDB + "map5")
        print "Step 5 finished ..."

    #step6: Cleaning of elongated sliver field polygons, e.g. left overs located between line of trees/hedgerow and road verge
        print "Step 6: Cleaning of elongated sliver field polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_vector", "map4_lyr")
        arcpy.SelectLayerByAttribute_management("map4_lyr", "NEW_SELECTION", "gridcode >= 1000")
        arcpy.CopyFeatures_management("map4_lyr", scratchDB + "map4_selection")
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map4_selection", scratchDB + "map4_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map4_selection", "Id", scratchDB + "map4_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_selection", "map4_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map4_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.1 AND Shape_Area/Shape_Length<4")
        arcpy.CopyFeatures_management("map4_selection_lyr", scratchDB + "map4_selection2")
        if int(arcpy.GetCount_management(scratchDB + "map4_selection2").getOutput(0))>0:
            arcpy.PolygonToRaster_conversion(scratchDB + "map4_selection2", "gridcode", scratchDB + "map4_selection2_raster", "", "", 1)
            Reclass6 = Con(IsNull(scratchDB + "map4_selection2_raster"), 2, 1)
            toNibble6 = SetNull(Reclass6, 2, "Value = 1")
            map6 = Nibble(map5, toNibble6, "DATA_ONLY")
            map6.save(scratchDB + "map6")
            arcpy.RasterToPolygon_conversion(map6, scratchDB + "map6_vector", "NO_SIMPLIFY", "VALUE")
        else:
            print "no cleaning needed ..."
            arcpy.CopyRaster_management(scratchDB + "map5", scratchDB + "map6")
            arcpy.CopyFeatures_management(scratchDB + "map4_vector", scratchDB + "map6_vector")

        cursor=arcpy.SearchCursor(scratchDB + "map6_vector")
        fields_list=[]
        duplicates_left=[]
        for row in cursor:
          a=row.getValue("gridcode")
          if a>=1000:
            if a in fields_list:
              duplicates_left.append(a)
            else:
              fields_list.append(a)

        # Finalise this part of the processing ...
        arcpy.Dissolve_management(scratchDB + "map6_vector", scratchDB + "map6_vector_dissolve", ["gridcode"], "", "", "")
        arcpy.Update_analysis(scratchDB + "map6_vector", scratchDB + "map6_vector_dissolve", scratchDB + "map_final_update", "BORDERS")
        # Now clip back to the LSwin MASK extent ...
        arcpy.Clip_analysis(scratchDB + "map_final_update", final_clip_extent, scratchDB + "map_final_vector", "")
        # And make the Polygon To Raster conversion ...        
        arcpy.PolygonToRaster_conversion(scratchDB + "map_final_vector", "gridcode", outPath + "map_final_raster", "", "", 1)
        print "Step 6 finished ..."
        nowTime = time.strftime('%X %x')
        print "Sliver polygons elimination done ..." + nowTime
        
    #===== End chunk: Elimination of sliver polygons =====#

    #===== Chunk: Finalize =====#
    # For this Chunk, first reset the arcpy.env.extent ...
        localSettings = final_clip_extent
        arcpy.env.extent = localSettings
    
    # Reclassify to ALMaSS raster values
    # ALMaSS uses different values for the landcover types, so this step simply translates
    # the numeric values.
        reclass_land = ReclassByASCIIFile(outPath + "map_final_raster", reclasstable, "DATA")
        reclass_land.save(outPath + "map_final_raster_reclass")
        nowTime = time.strftime('%X %x')
        print "Reclassification done ..." + nowTime

    # regionalise map
        regionALM = RegionGroup(reclass_land,"EIGHT","WITHIN","ADD_LINK","")
        regionALM.save(outPath + "Map_ALMaSS")
        nowTime = time.strftime('%X %x')
        print "Regionalisation done ..." + nowTime

    # export attribute table 
        table = outPath + "Map_ALMaSS"
        # Write an attribute tabel - based on this answer:
        # https://geonet.esri.com/thread/83294
        # List the fields
        fields = arcpy.ListFields(table)  
        field_names = [field.name for field in fields]  
          
        with open(attrexp,'wb') as f:  
            w = csv.writer(f)  
            # Write the headers
            w.writerow(field_names)  
            # The search cursor iterates through the rows of the table
            for row in arcpy.SearchCursor(table):  
                field_vals = [row.getValue(field.name) for field in fields]  
                w.writerow(field_vals)  
                del row
        print "Attribute table exported..." + nowTime

    # Find soil types
     # Set local variables
        inZoneData = regionALM
        zoneField = "VALUE"
        inValueRaster = soilraster
        outTable = os.path.join(dst, landscapes[index], "soiltypes.dbf")
        outZSaT = ZonalStatisticsAsTable(inZoneData, zoneField, inValueRaster, outTable, "DATA", "MAJORITY")
        fields = arcpy.ListFields(outTable)  
        field_names = [field.name for field in fields]
        with open(soilexp,'wb') as s:  
            w = csv.writer(s)  
            # Write the headers
            w.writerow(field_names)  
            for row in arcpy.SearchCursor(outTable):  
                field_vals = [row.getValue(field.name) for field in fields]  
                w.writerow(field_vals)  
                del row
        nowTime = time.strftime('%X %x')
        print "Soiltype table exported..." + nowTime

    # convert regionalised map to ascii
        arcpy.RasterToASCII_conversion(regionALM, asciiexp)
        print "Conversion to ASCII done ..." + nowTime

    # convert regionalised map to GeoTiff (arcpy CopyRaster)
        indata = regionALM
        outdata = drive + "/test_areas/" + landscapes[index] + "/outputs/ALMaSS_files/" + landscapes[index] + ".tif"
        print outdata
        arcpy.CopyRaster_management(indata, outdata)
        
        print "Conversion to Tiff done ..." + nowTime

        endTime = time.strftime('%X %x')
        print ""
        print "Landscape generated: " + endTime
    #===== End Chunk: Finalize =====#

    except:
        if arcpy.Exists(outPath + "tmpRaster"):
          arcpy.Delete_management(outPath + "tmpRaster")
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

        arcpy.AddMessage(arcpy.GetMessages(1))
        print arcpy.GetMessages(1)
