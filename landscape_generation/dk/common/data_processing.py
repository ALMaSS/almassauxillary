""" This module contains functions related to the processing of the DCE besætning (animal flocks) data and LPIS marker (fields) data """

import pandas as pd
import numpy as np

def Trace( function_name,  *argv ):
    function_name = function_name + '():\t'
    print("Farm Classification Data Processing: ", function_name, *argv)

def besstr_processing(df, year):
    trace_function_name = 'besstr_processing'

    Trace(trace_function_name, 'Number of entries in BES_STR: ' , len(df.index) )
    
    # Transform column 'DATO_OPTAELLING' from string to dates
    df['DATO_OPTAELLING_D'] = pd.to_datetime(df.DATO_OPTAELLING, format="%Y-%m-%d %H:%M:%S", errors = 'coerce')
    errs = df[ df.DATO_OPTAELLING_D.isnull() ]
    Trace(trace_function_name, 'No of rows where date was not well transformed from string to date object:', len(errs) )
    if ( len(errs) > 0 ):
        Trace(trace_function_name, '\n', errs, '\n')

    # Select all BES_ID that were counted in the given year, 
    # and pick only the newest numbers from this subset, for all combination of (BES_ID & BSTRK_ID - subflock type)
    year_start = pd.to_datetime(str(year-1)+'-11-15 00:00:00', format="%Y-%m-%d %H:%M:%S")
    year_end = pd.to_datetime(str(year+1)+'-02-15 00:00:00', format="%Y-%m-%d %H:%M:%S")
    df = df[ (df['DATO_OPTAELLING_D'] >= year_start) & (df['DATO_OPTAELLING_D'] <= year_end) ]
    Trace(trace_function_name, 'Number of entries in BES_STR from dates (', year_start.strftime("%Y-%m-%d"), ',' , year_end.strftime("%Y-%m-%d"), '): ', len(df.index) )
    df = df.loc[df.groupby(['BES_ID','BSTRK_ID'])['DATO_OPTAELLING_D'].idxmax()]
    Trace(trace_function_name, 'Of these, selection of newest counts only: ', len(df.index) )
    
    # Select for relevant flocks and sub-flock count only 
    bstrk_list = [ 1202, 1208, 1302, 1402, 1501, 1502, 1504, 1601, 1602, 1604, 3002, 3102, 3202, 3302, 3402, 3502, 4002, 4102, 4202, 4302, 4402, 4502, 4602, 4702, 4802, 4902 ]
    df = df.loc[ df['BSTRK_ID'].isin(bstrk_list) ]
    Trace(trace_function_name, 'Manual selection of relevant flocks and subflock types [ToUpdate]: ', bstrk_list )
    Trace(trace_function_name, 'Count of final selection of BES_STR (date, newest only, selected flock and subflock types): ', len(df.index) )
    
    return df


def besbruger_processing(df, df_cvr):
    trace_function_name = 'besbruger_processing'

    # Take only the newest entry if the same BES_ID has multiple CVR_ID
    # Transform column 'DATO_FRA' from string to dates
    df['DATO_FRA'] = pd.to_datetime(df.DATO_FRA, format="%Y-%m-%d %H:%M:%S", errors = 'coerce')
    errs = df[ df.DATO_FRA.isnull() ]
    Trace(trace_function_name, 'No of rows where date was not well transformed:', len(errs))
    if ( len(errs) > 0 ):
        Trace(trace_function_name, '\n', errs)

    # Pick only the newest CVR data for all BES_ID
    df = df.loc[df.groupby(['BES_ID'])['DATO_FRA'].idxmax()]
    Trace(trace_function_name, 'Number of entries in BESBRUGER after taking the latest update values: ', len(df))

    # Adding the CVR_NR on the dataframe, no point chasing it later 
    df['CVR_NR'] = df['CVR_ID'].map(df_cvr.set_index('ID')['CVR_NR'])
    df['CVR_NR'] = df['CVR_NR'].astype('Int64')

    # And finally keep only the entries that have a matched CVR_NR
    df = df[ df.CVR_NR.notnull() == True ]

    Trace( trace_function_name, 'Number of entries with CVR_NR found: ', len(df) )

    return df

def do_cvr_report(df, str_cvr, param = 1):
    trace_function_name = 'do_cvr_report'

    if ( param == 0 ):
        missing_data = df [ df[str_cvr].astype(bool) == False ]  # empty list as missing data
    else: 
        missing_data = df[ df[str_cvr].isnull() ] # nan as missing data
    
    # to make sure missing data pivot contains all DyreartID and I can later divide with the full data 
    # add for each dyreartid one an entry with ANTAL = 0 and cvr column null 
    missing_data = missing_data.append( pd.DataFrame( df.DYREART_ID.unique(), columns=['DYREART_ID'] ) ).reset_index()
    Trace(trace_function_name , ' Percentages of animal data missing ', str_cvr, ' is ', 100*len(missing_data)/len(df),'%.' )

    # Report of the total number of animals of each species (type) missing cvr info 
    x = pd.pivot_table( missing_data , index=['DYREART_ID'], aggfunc={'ANTAL':np.sum } ).reset_index()
    y = pd.pivot_table( df , index=['DYREART_ID'], aggfunc={'ANTAL':np.sum } ).reset_index()
    if ( len(x) != len(y) ):
        Trace(trace_function_name, 'WARNING: animal summary offset ! ')  # should never happen, corrected now by adding all dyreart into the missing data frame 
    y['ANTAL'] = 100 * x['ANTAL']/y['ANTAL']
    Trace( trace_function_name , '\n ', y )


def relcvradrsted_processing(df, df_cvr):
    trace_function_name = 'relcvradrsted_processing'

    # add the CVR-NR on the dataframe
    df['CVR_NR'] = df['CVR_ID'].map(df_cvr.set_index('ID')['CVR_NR'])
   
    # same address can have two CVRs or more. group.
    df = df.groupby('ADRSTED_ID')['CVR_NR'].apply(list).reset_index()

    # remove duplicate CVR_NRs in the list (not sure if relevant actually) 
    df['CVR_NR'] = df['CVR_NR'].apply( lambda x: list(dict.fromkeys(x)) )

    return df


def relbesadrsted_processing( df ):
    trace_function_name = "relbesadrsted_processing"

    # Take only the newest entry (using DATO_OPDATERING) if the same BES_ID has multiple ADRSTED_ID
    # Transform column 'DATO_OPDATERING' from string to dates
    df['DATO_OPDATERING'] = pd.to_datetime(df.DATO_OPDATERING, format="%Y-%m-%d %H:%M:%S", errors = 'coerce')
    errs = df[ df.DATO_OPDATERING.isnull() ]
    Trace(trace_function_name, 'No of rows where date was not well transformed:', len(errs))
    if ( len(errs) > 0 ):
        Trace(errs)

    # Pick only the newest ADRSTED_ID data for all BES_ID
    df = df.loc[df.groupby(['BES_ID'])['DATO_OPDATERING'].idxmax()]
    Trace(trace_function_name, 'Number of entries in RELBESADRSTED after taking the newest update: ', len(df))
    
    return df 


def animaldata_processing(df_besstr, df_besstrkode, df_chrbesaetning, df_dyreart): 
    trace_function_name = "animaldata_processing"

    # (1) Selecting the relevant flocks (sub-flock selection) in the year of interest, from BES_STR.dat
    df_animaldata = besstr_processing( df_besstr.copy(), 2019 )

    # (2) Add additional columns for sub-flock description
    df_animaldata['BESSTRTEKST'] = df_animaldata['BSTRK_ID'].map(df_besstrkode.set_index('ID')['BESSTRTEKST'])
    missing_data = df_animaldata[df_animaldata.BESSTRTEKST.isnull()]
    Trace(trace_function_name, 'Number of sub-flocks (BES_ID , BSTRK_ID) missing BESSTRTEKST number (should be 0): ', len(missing_data))

    # (3) Add additional columns for flock description 
    df_animaldata['DYREART_ID_2'] = df_animaldata['BSTRK_ID'].apply(lambda x: x//100 )
    missing_data = df_animaldata[df_animaldata.DYREART_ID_2.isnull()]
    Trace(trace_function_name, 'Number of sub-flocks (BES_ID , BSTRK_ID) missing DYREART_ID_2 number (should be 0): ', len(missing_data))
    df_animaldata['DYREART_ID'] = df_animaldata['BES_ID'].map(df_chrbesaetning.set_index('ID')['DYREART_ID'])
    missing_data = df_animaldata[df_animaldata.DYREART_ID.isnull()]
    Trace(trace_function_name, 'Number of sub-flocks (BES_ID , BSTRK_ID) missing DYREART_ID number (should be 0): ', len(missing_data))
    mismatched_data = df_animaldata.loc[~(df_animaldata['DYREART_ID'] == df_animaldata['DYREART_ID_2'])]
    Trace(trace_function_name, 'Number of mismatched DYREART_ID_2 (should be 0): ', len(mismatched_data) )

    # (4) Colapse the sub-flocks to get just total number animals in flocks (sum over sub-flocks)
    # Relevant subflocks to sum over have been filtered on in besstr_processing(), so this is good  
    df_animaldata = pd.pivot_table( df_animaldata , index=['BES_ID'], aggfunc={'ANTAL':np.sum , 'DYREART_ID': 'first', 'BSTRK_ID':list, 
           'DATO_OPTAELLING_D': list, 'BESSTRTEKST': list } )
    df_animaldata.reset_index(inplace = True) # to make BES_ID a normal column back from index
    df_animaldata = df_animaldata[ df_animaldata.ANTAL != 0 ] # Remove rows with antal dyr = 0 after the pivot
    
    # (5) More additional columns for flock description 
    df_animaldata['DYREARTTEKST'] = df_animaldata['DYREART_ID'].map(df_dyreart.set_index('DYREARTKODE')['DYREARTTEKST'])
    missing_data = df_animaldata[df_animaldata.DYREARTTEKST.isnull()]
    Trace(trace_function_name, 'Number of flocks (BES_ID) missing DYREARTTEKST number (should be 0): ', len(missing_data))

    Trace(trace_function_name, 'Number of rows in sub-flock collapsed animal data: ', len(df_animaldata.index) )

    return df_animaldata


def animaldata_cvr_matching(df_animaldata, df_besbruger, df_chrbesaetning, df_relcvradrsted, df_chrejendom , df_relbesadrsted, df_cvr ):
    trace_function_name = "animaldata_cvr_matching"

    # ----------------------------------------------------- 
    # Matching the flocks/besætninger with the CVR numbers:
    # -----------------------------------------------------
    # Multiple paths:
    # A. from BES_STR.dat (BES_ID with counted animals in year X), with BESBRUGER.dat (BES_ID and CVR_ID) and CVR.dat (CVR_ID and CVR_NR) 
    # B. from BES_STR.dat (BES_ID with counted animals in year X), with CHR_BESAETNING.dat (BES_ID, CHR_ID) with CHR_EJENDOM.dat (CHR_NR, ADRSTED_ID) with REL_CVR_ADRSTED.dat (ADRSTED_ID, CVR_ID)
    # C. from BES_STR.dat (BES_ID with counted animals in year X), with REL_BES_ADRSTED (BES_ID, ADRSTED_ID) with REL_CVR_ADRSTED.dat (ADRSTED_ID, CVR_ID)

    # (1) Path A
    df_besbruger_processed = besbruger_processing( df_besbruger.copy(), df_cvr )
    df_animaldata['CVR_NR_A'] = df_animaldata['BES_ID'].map(df_besbruger_processed.set_index('BES_ID')['CVR_NR'])
    Trace(trace_function_name, 'Report on CVR_NR_A from BESBRUGER.dat path')
    do_cvr_report(df_animaldata, 'CVR_NR_A')

    # (2) Path B
    df_relcvradrsted_processed = relcvradrsted_processing(df_relcvradrsted.copy(), df_cvr)
    df_animaldata['CHR_ID'] = df_animaldata['BES_ID'].map(df_chrbesaetning.set_index('ID')['CHR_ID'])
    df_animaldata['ADRSTED_ID'] = df_animaldata['CHR_ID'].map(df_chrejendom.set_index('CHRNR')['ADRSTED_ID'])
    df_animaldata['CVR_NR_B'] = df_animaldata['ADRSTED_ID'].map(df_relcvradrsted_processed.set_index('ADRSTED_ID')['CVR_NR'])
    Trace(trace_function_name, 'Report on CVR_NR_B from CHR_BESAETNING, CHR_EJENDOM (ADRSTED_ID) path')
    do_cvr_report(df_animaldata, 'CVR_NR_B')

    # (3) Path C
    df_relbesadrsted_processed = relbesadrsted_processing(df_relbesadrsted.copy())
    df_animaldata['ADRSTED_ID_2'] = df_animaldata['BES_ID'].map(df_relbesadrsted_processed.set_index('BES_ID')['ADRSTED_ID'])
    df_animaldata['CVR_NR_C'] = df_animaldata['ADRSTED_ID_2'].map(df_relcvradrsted_processed.set_index('ADRSTED_ID')['CVR_NR'])
    Trace(trace_function_name, 'Report on CVR_NR_C (BES_ID) to REL_BES_ADRSTED path')
    do_cvr_report(df_animaldata, 'CVR_NR_C')

    # (4) Create the combined list of possible CVRs
    df_animaldata['CVR_NR_A'] = df_animaldata['CVR_NR_A'].apply(lambda d: [] if pd.isnull(d) else [ d ])  # transform to list, and NaN to [] (empty list)
    df_animaldata['CVR_NR_B'] = df_animaldata['CVR_NR_B'].apply(lambda d: d if isinstance(d, list) else [])  # transform NaN to [] (empty list)
    df_animaldata['CVR_NR_C'] = df_animaldata['CVR_NR_C'].apply(lambda d: d if isinstance(d, list) else [])  # transform NaN to [] (empty list)
    df_animaldata['CVR_NR'] = df_animaldata['CVR_NR_A'] + df_animaldata['CVR_NR_B'] + df_animaldata['CVR_NR_C'] # combine 
    df_animaldata['CVR_NR'] = df_animaldata['CVR_NR'].apply(lambda x: list(dict.fromkeys(x)) ) # remove duplicates
    
    return df_animaldata


def marker_processing( df_fields ):
    trace_function_name = "marker_processing"

    Trace(trace_function_name, "Length of df_fields unprocessed ", len(df_fields.index) )

    df_fields['CVR'] = df_fields['CVR'].replace(' ', -1)
    df_fields['CVR'] = df_fields['CVR'].astype('float')
    df_fields['CVR'] = df_fields['CVR'].astype('Int64')

   # df_fields['AFGKODE'] = [x.replace(',', '.') for x in df_fields['AFGKODE']]
   # df_fields['AFGKODE'] = df_fields['AFGKODE'].astype('float')
   # df_fields['AFGKODE'] = df_fields['AFGKODE'].astype('Int64')
   # df_fields['IMK_AREAL'] = [x.replace(',', '.') for x in df_fields['IMK_AREAL']]
   # df_fields['IMK_AREAL'] = df_fields['IMK_AREAL'].astype('float')

    Trace(trace_function_name, 'Number of fields without crop code: ', len( df_fields.loc[df_fields['AFGKODE']==0]) )
    Trace(trace_function_name, 'Fields with crop code = 0: ', '\n',  
           df_fields.loc[df_fields['AFGKODE']==0] )

    df_fields = df_fields.loc[df_fields['AFGKODE'] != 0] # is this necessary - yes

    Trace(trace_function_name, 'Length of df_fields processed', len(df_fields.index) )

    return df_fields

    
def crop_codes_tole_classification(df_marker,df_cc2tov):
    trace_function_name = "crop_codes_tole_classification"
    Trace(trace_function_name, 'begin')

    # crop codes grouped into sets, the implicit set is 20, tole_field
    
    # Potentially relevant (external) TOLEs (numbers)
    tole_Field = 20 
    tole_Orchard = 56
    tole_PermanentSetaside = 33 
    tole_PermPasture = 35
    tole_PermPastureLowYield = 26
    tole_PermPastureTussocky = 27
    tole_PlantNursery = 214
    
    # New TOLEs
    tole_OOrchard = 503
    tole_BushFruit = 504
    tole_OBushFruit = 505
    tole_ChristmasTrees = 506 
    tole_OChristmasTrees = 507
    tole_EnergyCrop = 508
    tole_OEnergyCrop = 509
    tole_FarmForest = 510
    tole_OFarmForest = 511
    tole_PermPasturePigs = 512
    tole_OPermPasturePigs = 513
    tole_OPermPasture = 514
    tole_OPermPastureLowYield = 515
    tole_FarmYoungForest = 516
    tole_OFarmYoungForest = 517

    # Find all the TOVs with the "_Perm" suffix and make a manual list of corresponding TOLEs
    #df_cc2tov_perm = df_cc2tov.loc[df_cc2tov['TOV_CLASSIF_2019'].str.contains('_Perm')].groupby('TOV_CLASSIF_2019')['AFGKODE'].apply(list).reset_index()
    #df_cc2tov_undef = df_cc2tov.loc[df_cc2tov['TOV_CLASSIF_2019'].str.contains('Undefined')].groupby('TOV_CLASSIF_2019')['AFGKODE'].apply(list).reset_index()
    
    PERMTOV2TOLE = {
        "tov_DKOrchardCrop_Perm": tole_Orchard,
        "tov_DKOOrchardCrop_Perm": tole_OOrchard,
        "tov_DKBushFruit_Perm": tole_BushFruit,
        "tov_DKOBushFruit_Perm": tole_OBushFruit,
        "tov_DKChristmasTrees_Perm": tole_ChristmasTrees,
        "tov_DKOChristmasTrees_Perm": tole_OChristmasTrees,
        "tov_DKEnergyCrop_Perm": tole_EnergyCrop,
        "tov_DKOEnergyCrop_Perm": tole_OEnergyCrop,
        "tov_DKFarmForest_Perm": tole_FarmForest,
        "tov_DKOFarmForest_Perm": tole_OFarmForest,
        "tov_DKGrazingPigs_Perm": tole_PermPasturePigs,
        "tov_DKOGrazingPigs_Perm": tole_OPermPasturePigs,
        "tov_DKUndefined": tole_PermanentSetaside,
        "tov_DKGrassGrazed_Perm": tole_PermPasture,
        "tov_DKOGrassGrazed_Perm": tole_OPermPasture, 
        "tov_DKGrassLowYield_Perm": tole_PermPastureLowYield,
        "tov_DKOGrassLowYield_Perm": tole_OPermPastureLowYield,
        "tov_DKFarmYoungForest_Perm": tole_FarmYoungForest,
        "tov_DKOFarmYoungForest_Perm": tole_OFarmYoungForest,
        "tov_DKPlantNursery_Perm":tole_PlantNursery
    }

    df_cc2tov_conv = df_cc2tov.loc[df_cc2tov['organic']==0]
    df_cc2tov_org = df_cc2tov.loc[df_cc2tov['organic']==1]
    df_marker_conv = df_marker.loc[df_marker['organic']==0]
    df_marker_org = df_marker.loc[df_marker['organic']==1]
    
    df_marker_conv['TOV'] = df_marker_conv['AFGKODE'].map(df_cc2tov_conv.set_index('AFGKODE')['TOV_CLASSIF_2019'])
    df_marker_org['TOV'] = df_marker_org['AFGKODE'].map(df_cc2tov_org.set_index('AFGKODE')['TOV_CLASSIF_2019'])
   
    df_marker = df_marker_conv.append(df_marker_org)
    df_marker['TOLE'] = df_marker['TOV'].apply(lambda x: PERMTOV2TOLE.get(x, tole_Field))  # return tole, or default tole_field

    Trace(trace_function_name, 'end')
    return df_marker

    
def marker2farms( df_marker ): 
    trace_function_name = "marker2farms"

    # here look at the journalnumber for cvr  = -1
    df_fields_missing_cvr = df_marker [ df_marker['CVR'] == -1 ]
    Trace(trace_function_name, 'Nb of fields w/o CVR = ', len(df_fields_missing_cvr))
    df_fields_with_cvr = df_marker [ df_marker['CVR'] != -1 ]
    Trace(trace_function_name, 'Nb of fields with CVR = ', len(df_fields_with_cvr))

    df_farms = df_fields_with_cvr.groupby('CVR')['ID'].apply(list).reset_index()
    #Trace(trace_function_name, 'df_farms \n', df_farms)

    df_farms2 = df_fields_missing_cvr.groupby('JOURNALNR')['ID'].apply(list).reset_index()
    df_farms2['CVR'] = 900000000 + df_farms2.index
    #Trace(trace_function_name, 'df_farms2 \n', df_farms2)
    
    df_farms = df_farms.append(df_farms2).reset_index(drop=True)
    Trace(trace_function_name, 'df_farms complete \n', df_farms)

    return df_farms


def animals2farms( df_animaldata, df_field_farms ):
    trace_function_name = "animals2farms"

    # (0) reorganize/regroup animal data by CVR_NR, with a list of the flocks / BES_ID
    df_stock_farms = df_animaldata.explode('CVR_NR').groupby('CVR_NR')['BES_ID'].apply(list).reset_index() # No duplicate BES_IDs list because BES_ID was key, and no duplicate in the CVRs list of same BES_ID
    Trace(trace_function_name, 'Len of df_stock_farms = ', len(df_stock_farms) )

    # (1) Remove all CVRs that are not Field Farm CVRs (not interested in stock farms w/o fields apparently ! )
    df_field_stock_farms = df_stock_farms.loc[ df_stock_farms['CVR_NR'].isin(df_field_farms['CVR'].tolist()) ]
    Trace(trace_function_name, 'Len of df_field_stock_farms = ', len(df_field_stock_farms) )

    # (2) Make sure not having same BES_IDs on more than one Field Farm CVR
    df_field_stock_farms = df_field_stock_farms.explode('BES_ID').groupby(['BES_ID'])['CVR_NR'].apply(list).reset_index()
    Trace(trace_function_name, 'Nb of BES_ID with multiple Field CVRs:', len(df_field_stock_farms[df_field_stock_farms['CVR_NR'].apply(len)>1])) # Count number of BES_ID with multiple CVRs
    def filter_cvr(row): # this also transforms the CVR_NR list to a simple numeric value
        return row['CVR_NR'][0] # return the first one 
    df_field_stock_farms['CVR_NR'] = df_field_stock_farms.apply(filter_cvr, axis = 1)
    df_field_stock_farms = df_field_stock_farms.groupby('CVR_NR')['BES_ID'].apply(list).reset_index()
    df_field_stock_farms = df_field_stock_farms[ df_field_stock_farms.CVR_NR.isnull() == False ]
    Trace(trace_function_name, 'Len of df_field_stock_farms after reducing BES_IDs with multiple CVRs', len (df_field_stock_farms))

    # (3) I should not have any BES_ID doubles, check ( BES_ID belongs only to one CVR )
    x = df_field_stock_farms['BES_ID'].tolist()
    x = [item for sublist in x for item in sublist] # flatten it 
    Trace(trace_function_name, 'No double BES_ID (should be True) : ', len(x) == len(set(x)) ) 

    return df_field_stock_farms     


def report_match_animals_fields(df_animaldata, df_field_farms):
    trace_function_name = "report_match_animals_fields"

    Trace(trace_function_name,"MATCHED ANIMALS TO FIELD FARMS REPORT")
    Trace(trace_function_name,"-------------------------------------")

    # Statistics on the matching of animals with farms
    animals = pd.pivot_table( df_animaldata , index=['DYREART_ID'], aggfunc={'ANTAL':np.sum } )
    
    # How many animals matched. Put CVR on from field_farms
    df_field_farms = df_field_farms.drop(columns=['JOURNALNR'])
    # fickle way to do it, becuase drops all rows that have missing values on "any" columns, not just BES_ID. keeping for now though.
    df_animaldata["CVR_NR_FARM"] = df_animaldata['BES_ID'].map(df_field_farms.explode('BES_ID').dropna(axis=0, how="any").set_index('BES_ID')['CVR'])
    df_animals_wo_fields = df_animaldata[df_animaldata['CVR_NR_FARM'].isnull()]
    
    animals_wo_fields = pd.pivot_table(df_animals_wo_fields , index=['DYREART_ID'], aggfunc={'ANTAL':np.sum } )
    
    Trace(trace_function_name, 'Percentage of animals w/o field farms \n', animals_wo_fields['ANTAL']/animals['ANTAL']*100)


def create_field_file_for_classif(df_fields, df_field_farms):
    trace_function_name = "create_field_file_for_classif"

    df_fields['FARM_ID'] = df_fields['CVR'].map(df_field_farms.set_index('CVR')['FARM_ID']) 
    #Trace(trace_function_name, df_fields)
    
    #Fill in also FarmIDs coming from the JournalNR matching with fake CVRs
    tmp = df_field_farms[ df_field_farms['JOURNALNR'].isnull() == False]
    df_fields['FARM_ID_2'] = df_fields['JOURNALNR'].map(tmp.set_index('JOURNALNR')['FARM_ID']) 
    
    # combine the two columns
    df_fields["FARM_ID"] = df_fields["FARM_ID"].fillna(0) + df_fields["FARM_ID_2"].fillna(0)

    df_fields = df_fields.rename(columns={"ID": "FIELD_ID"})
    df_fields['FARM_ID'] = df_fields['FARM_ID'].astype(int)
    
    # select onlt the following columns 
    df_fields = df_fields[['FIELD_ID','IMK_AREAL', 'AFGKODE', 'organic', 'FARM_ID']]
    return df_fields 


def create_animal_file_for_classif(df_animal_data, df_field_farms):
    trace_function_name = "create_animal_file_for_classif"
    
    # keep only farms with animals 
    df_animal_farms = df_field_farms.loc[ df_field_farms['BES_ID'].isnull() == False ] 
    
    # Add animal numbers
    # Explode the data, then put dyreart
    # Then filter for cattle first, and add column with numbers for cattles
    df_farms_animals_exp = df_field_farms.explode('BES_ID')
    df_farms_animals_exp['DYREART_ID'] = df_farms_animals_exp['BES_ID'].map(df_animal_data.set_index('BES_ID')['DYREART_ID'])
    df_farms_animals_exp['ANTAL'] = df_farms_animals_exp['BES_ID'].map(df_animal_data.set_index('BES_ID')['ANTAL'])

    # Check here if, for same CVR, I have more than one BES_ID of same animal type ( A: yes )
    # -------------------------------------------------
    #test = df_farms_animals_exp.groupby('CVR')['DYREART_ID'].apply(list).reset_index()
    #def test_multiple(row):
    #    if ( len(row['DYREART_ID']) != len(set(row['DYREART_ID']))  ): # does it contain doubles ?
            #Trace(trace_function_name, 'WARNING !!! For same CVR I have more than one flock of same animal type ! Case not treated --> DEV To Do') 
            #Trace(trace_function_name, '\n', row)
    #test.apply(test_multiple, axis = 1)
    # -------------------------------------------------

    # This handles in this way also the case for multiple bes_id with same dyreart_id on same cvr 

    # select cattle data
    df_cattle = df_farms_animals_exp.loc[ df_farms_animals_exp.DYREART_ID == 12 ]
    df_cattle = df_cattle.rename(columns={"ANTAL": "cattle"})
    df_cattle = df_cattle.groupby('CVR')['cattle'].apply(np.sum).reset_index()

    # select sheep data 
    df_sheep = df_farms_animals_exp.loc[ df_farms_animals_exp.DYREART_ID == 13 ]
    df_sheep = df_sheep.rename(columns={"ANTAL": "sheep"})
    df_sheep = df_sheep.groupby('CVR')['sheep'].apply(np.sum).reset_index()

    # select pigs data 
    df_pigs = df_farms_animals_exp.loc[ df_farms_animals_exp.DYREART_ID == 15 ]
    df_pigs = df_pigs.rename(columns={"ANTAL": "pigs"})
    df_pigs = df_pigs.groupby('CVR')['pigs'].apply(np.sum).reset_index()

    # select other data : 
    df_other = df_farms_animals_exp.loc[ ( df_farms_animals_exp.DYREART_ID == 30 ) | (  df_farms_animals_exp.DYREART_ID == 31 ) | ( df_farms_animals_exp.DYREART_ID == 32 ) ]
    df_other = df_other.rename(columns={"ANTAL": "other"})
    df_other = df_other.groupby('CVR')['other'].apply(np.sum).reset_index()

    # Now put them back on the df_farms_animals 
    df_animal_farms['cattle'] = df_animal_farms['CVR'].map(df_cattle.set_index('CVR')['cattle'])
    df_animal_farms.fillna({'cattle':0}, inplace=True)
    df_animal_farms['sheep'] = df_animal_farms['CVR'].map(df_sheep.set_index('CVR')['sheep'])
    df_animal_farms.fillna({'sheep':0}, inplace=True)
    df_animal_farms['pigs'] = df_animal_farms['CVR'].map(df_pigs.set_index('CVR')['pigs'])
    df_animal_farms.fillna({'pigs':0}, inplace=True)
    df_animal_farms['other'] = df_animal_farms['CVR'].map(df_other.set_index('CVR')['other'])
    df_animal_farms.fillna({'other':0}, inplace=True)

    df_animal_farms['cattle'] = df_animal_farms['cattle'].astype(int)
    df_animal_farms['sheep'] = df_animal_farms['sheep'].astype(int)
    df_animal_farms['pigs'] = df_animal_farms['pigs'].astype(int)
    df_animal_farms['other'] = df_animal_farms['other'].astype(int)
    
    # select onlt the following columns
    df_animal_farms = df_animal_farms[['FARM_ID','cattle', 'sheep', 'pigs', 'other']]
    
    return df_animal_farms


def create_classified_fields(df_farmtype, df_marker, df_field_farms):
    trace_function_name = "create_classified_fields"

    df_marker['FARM_ID'] = df_marker['CVR'].map(df_field_farms.set_index('CVR')['FARM_ID'])
    #Fill in also farmIDs coming from the JournalNR matching with fake CVRs
    tmp = df_field_farms[ df_field_farms['JOURNALNR'].isnull() == False]
    df_marker['FARM_ID_2'] = df_marker['JOURNALNR'].map(tmp.set_index('JOURNALNR')['FARM_ID']) 
    # combine the two columns, they have no overlapping values 
    df_marker["FARM_ID"] = df_marker["FARM_ID"].fillna(0) + df_marker["FARM_ID_2"].fillna(0)
    df_marker = df_marker.drop(columns = ['FARM_ID_2'])
    Trace(trace_function_name, 'Should be zero: ', len( df_marker[df_marker.FARM_ID.isnull()] ) )

    # process farmtype
    df_farmtype['FarmID'] = df_farmtype['FarmID'].astype('Int64')
    df_marker['FarmType'] = df_marker['FARM_ID'].map(df_farmtype.set_index('FarmID')['FarmType'])
    Trace( trace_function_name, 'Should be zero:', len( df_marker[df_marker.FarmType.isnull() ] ) )

    # process farmtype - maybe do it separate , or make in the future a function for it 
    df_marker['CVR_D'] = df_marker['FARM_ID'].map(df_field_farms.set_index('FARM_ID')['CVR'])
    Trace( trace_function_name, 'Should be zero:', len( df_marker[df_marker.CVR_D.isnull() ] ) )

    df_marker['FarmType'] = df_marker['FarmType'].astype('Int64')
    df_marker['FARM_ID'] = df_marker['FARM_ID'].astype('Int64')
    df_marker = df_marker.rename(columns={"ID": "FIELD_ID"})  # rename the ID field, as it clashes later on (as requested)


    return df_marker

