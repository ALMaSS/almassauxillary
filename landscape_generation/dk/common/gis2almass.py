""" Functions to support the conversion of GIS data export into the ALMaSS input format """

import pandas as pd

def Trace( function_name,  *argv ):
    function_name = function_name + '():\t'
    print("GIS2ALMaSS: ", function_name, *argv)

# Build the FarmRef dataframe
# Table Header: <FARMID> <FARMTYPENUMBER> [ ToDo: <FARMTYPESTRING> ]
def make_farms_df(df_fields):
    trace_function_name = "make_farms_df"

    # create list of uniques global farm ids in the window, and create a mapping to [0,n] ids 
    farm_ids = df_fields.FARM_ID.unique() 
    farm_ids.sort(axis=0)
    df_farms = pd.DataFrame(data=farm_ids, index=None, columns=['Global_Farm_ID']) 
    df_farms['FARM_ID_LOCAL'] = df_farms.index 
    df_new_farm_numbers =  df_farms.copy()

    Trace(trace_function_name, 'df_new_farm_numbers = \n', df_new_farm_numbers )

    # now put the farm type on
    def meq(v):
        x = next(iter(set(v)))  # first element of the set (all sets have one elemnt only anyway)
        return x
    df_farm_types = df_fields.groupby('FARM_ID')['FARMTYPE'].apply(meq).reset_index()   # change to FARM_ID
    df_farms['FarmType'] = 32 + df_farms['Global_Farm_ID'].map(df_farm_types.set_index('FARM_ID')['FARMTYPE'])
    
    df_farms = df_farms.drop(columns=['Global_Farm_ID'])

    return df_new_farm_numbers, df_farms


# Build the Polyref dataframe
# Table Header: <PolyType> <PolyRefNum> <Area> <FarmRef> <UnsprayedMarginRef=-1> <SoilType> <Openness=-1> <CentroidX=-1> <CentroidY=-1> <Elevation[float]> <Slope[float]> <Aspect[float]>
def make_polyref_df(df_attr, df_fields, df_soil, df_new_farm_numbers):
    trace_function_name = "make_polyref_df"

    # Start making the polyref dataframe
    df_polyref = df_attr.copy()

    # Put the soil column onto the polyref
    df_polyref['SoilType'] = df_polyref['Value'].map(df_soil.set_index('Value')['MAJORITY'])
    df_polyref['SoilType'] = df_polyref['SoilType'].fillna(-1)
    df_polyref['SoilType'] = df_polyref['SoilType'].astype(int)
    no_soil = df_polyref[ df_polyref.SoilType.isnull() == False ]
    Trace(trace_function_name, ' Soil Check (should be True): ', ( len(no_soil) == len(df_soil) ))
    #Trace(trace_function_name, ' DataFrame df_polyref \n',  df_polyref)

    # Put Elevantion columns ( zere for now, later get data from maibe df_attr)
    df_polyref['Elevation'] = 0
    df_polyref['Slope'] = 0
    df_polyref['Aspect'] = 0

    # Not calulated here, but in ALMaSS - maybe to move in here/LSGEN at some point ?
    df_polyref['UnsprayedMarginRef'] = -1
    df_polyref['Openness'] = -1
    df_polyref['CentroidX'] = -1
    df_polyref['CentroidY'] = -1
    
    # Process the Link/fields reference
    df_polyref_fields = df_polyref[ df_polyref.LINK >= 1000 ]
    df_polyref_wo_fields = df_polyref[ df_polyref.LINK < 1000 ]
    df_polyref_wo_fields = df_polyref_wo_fields.rename(columns={"LINK": "PolyType"})

    # Check fields
    Trace(trace_function_name, ' Fields check. Len of  df_polyref_fields =  ', len(df_polyref_fields), ' and len of df_fields is ', len(df_fields), '.' )

    df_polyref_fields['PolyType'] = df_polyref_fields['LINK'].map(df_fields.set_index('GRIDCODE')['ALMASSELE'])
    df_polyref_fields['FarmRef_global'] = df_polyref_fields['LINK'].map(df_fields.set_index('GRIDCODE')['FARM_ID']) 
    df_polyref_fields['FarmRef'] = df_polyref_fields['FarmRef_global'].map(df_new_farm_numbers.set_index('Global_Farm_ID')['FARM_ID_LOCAL'])
    df_polyref_fields['SHAPE_AREA'] = df_polyref_fields['LINK'].map(df_fields.set_index('GRIDCODE')['SHAPE_AREA']) # ToAsk: Count doesnt fully match with shape_area, but very close 
    df_polyref_fields['FarmType'] = df_polyref_fields['LINK'].map(df_fields.set_index('GRIDCODE')['FARMTYPE'])

    #Trace(trace_function_name, 'df_polyref_wo_fields = \n', df_polyref_wo_fields.head(15))
    #Trace(trace_function_name, 'df_polyref_fields = \n', df_polyref_fields.head(15))
   
    df_polyref_done = df_polyref_wo_fields.append(df_polyref_fields)
   
    # Replace FarmRef NaN with -1 ( no owner in almass is -1), change column names 
    df_polyref_done['FarmRef'] = df_polyref_done['FarmRef'].fillna(-1)
    df_polyref_done['FarmRef'] = df_polyref_done['FarmRef'].astype(int)
    
    df_polyref_done = df_polyref_done.rename(columns={"Value": "PolyRefNum"})
    df_polyref_done = df_polyref_done.rename(columns={"Count": "Area"})
    polyref_done_column_order = ['PolyType', 'PolyRefNum', 'Area', 'FarmRef', 'UnsprayedMarginRef', 'SoilType', 'Openness', 'CentroidX','CentroidY', 'Elevation', 'Slope', 'Aspect' ]

    #Trace(trace_function_name, 'df_polyref_done \n', df_polyref_done.head(15))
    #Trace(trace_function_name, 'df_polyref_done \n', df_polyref_done.tail(15))

    return df_polyref_done, polyref_done_column_order 