import sys
import numpy as np

def read1(ifile, label, type):

    line = ifile.readline()
    l, v = (val for val in line.split())
    if ( l == label ):
        v = type(v)
        print(l, ' : ', v)
        return v
    else:
        print(' read1(): Format error ! ')
        sys.exit()

def ascii2lsb(input_filename, output_filename):

    # This opens a handle to your file, in 'r' read mode
    ifile = open(input_filename, 'r')

    ncols = read1(ifile, 'ncols', int)
    nrows = read1(ifile, 'nrows', int)
    xllcorner = read1(ifile, 'xllcorner', str)
    yllcorner = read1(ifile, 'yllcorner', str)
    cellsize = read1(ifile, 'cellsize', int)
    nodata_value = read1(ifile, 'NODATA_value', int)

    # Open the output file in w mode
    ofile = open(output_filename, 'wb')
    header = "An LSB File"+'\0'
    ofile.write(bytes(header,encoding='utf8'))
    ofile.write(ncols.to_bytes(4,'little'))
    ofile.write(nrows.to_bytes(4,'little'))

    for i in range(nrows):
        line = ifile.readline()
        data = [ int(val) for val in line.split() ]
        ofile.write( np.array(data).tobytes(order='C') )

    ifile.close()
    ofile.close()

    return ncols, nrows, xllcorner, yllcorner, cellsize
