""" This module contains general utility functions for the danish landscape processing """

import pandas as pd

def Trace( function_name,  *argv ):
    function_name = function_name + '():\t'
    print("Read csv File: ", function_name, *argv)

def readcsv(*args, **kwargs):
    trace_function_name = 'readcsv'
    filename = args[0]
    df = pd.read_csv(*args, **kwargs)

    # If I want error logs 
    #    with open( 'U:\\work\\04_toletov_landscape\\DCE_DATA\\' + ntpath.basename(filename) + '.readlog', 'w') as log:
    #        with contextlib.redirect_stderr(log):
    #            df = pd.read_csv(*args, **kwargs)

    # Checks 
    num_lines = sum(1 for line in open(filename, encoding='latin-1')) - 1 # minus the header
    number_of_skipped_rows = num_lines - len(df)
    Trace(trace_function_name, "Number of bad rows in ", filename , ": " , number_of_skipped_rows)

    return df
