/**
\mainpage ALMaSS helper program FarmerTypeologies_NationalDataset

\htmlonly
<style type="text/css">
body {
color: #000000
background: white;
}
h2  {
color: #0022aa
background: white;
}
</style>
<h1>
<small>
<small>
<small>
<small>
Created by<br>
<br>
</small>
</small>
Chris J. Topping<br>
<small>Department of Bioscience<br>
Aarhus University<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
19th May 2014<br>
</small>
</small>
</small>
</h1>
<br>
\endhtmlonly


This program classifies all farms based on a combination of the crops they are growing using data obtained from the General Landbrug Register (GLR), and on the animals they have
which is data from the Central Husdyr Register (CHR). The GLR is a compilation of the data submitted by the farmers in support of EU subsidy payments. The CHR is a register of all
agricultural animals maintained primarily for purposes of disease control.  \n
By combining crop and animal information it is possible to identify major farm types such as pig or arable, or dairy farms. Some less common types are also identifieable e.g. farmers
that grow sugar beet on contract. In addition to this information the GLR also indicates
whether a farm is organic or not and the overall farm size. This extra information provides the basis for classification into 17 farm types. The farm types are listed in the 
enumeration #TTypesOfFarm \n
Rules used to classify the farms. These are found in #AssignFarmType and all farms can be designated as organic or not dependent upon the information on that farm in the GLR:
- Farms with large proportion of vegetables set by #VegFarmMinimum and above 2-ha are organic or conventional vegetable farms, otherwise if small are classified as 'other'
- Farms with a proportion of potatoes or sugar beet not less than #PotatoFarmMinimum or #BeetFarmMinimum are potato or beet farms respectively.
- Farms with animal (cows, sheep and pigs) transformed to standard animal units that have fewer than #HobbyAnimalThreshold and an area less than HobbyAreaThreshold are designated as
hobby farms
- Farms with animal units above #HobbyAnimalThreshold and cattle+sheep above #CattleThreshold they are designated as cattle farms
- Farms with animal units above #HobbyAnimalThreshold and pigs above #PigThreshold, or crop area of grazing pigs above #MinPigFarmGrazing they are designated as pig farms
- Farms with animal units above #HobbyAnimalThreshold, but not pig or cattle farms, are designated as MixedStock
- Farms with no animals registered but with large areas of grazing are assumed to be either cattle farms or mixed stock farms depending on whether grazing is above #CattleGrassThreshold 
or #MixedStockGrassMinimum
- All remaining farms must be arable farms (i.e. large area with few or no animals and little or no grazing).

*/

#include <iostream>
#include <fstream>
#include <time.h>
#include <cmath>
#include <vector>
#include <list>
#include <sstream>
#include <string>
#include <unordered_set>
#include <stdexcept>

#include "CropCodesMapping.h"

using namespace std;

/** \brief If total animals is below this it could be a hobby farm. */
const int HobbyAnimalThreshold = 20;
/** \brief If total area is below this it is a hobby farm, if animals is below  HobbyAnimalThreshold. */
const int HobbyAreaThreshold = 20; // in ha
/** \brief Cattle above this proportion and it is a cattle farm, in % */
const double CattleThreshold = 0.75;
/** \brief Grass or other fodder crop area above this proportion and it is a cattle farm even if it seems like it has no animals (due probably to different CVR numbers) */
const double CattleGrassThreshold = 0.40;
/** \brief More than this proportion of pigs and it is a pig farm */
const double PigThreshold = 0.75;
/** \brief If at least this proportion of grass then we have a stock farm of some type*/
const double MixedStockGrassMinimum = 0.20;
/** \brief If at least this proportion of potatoes then we have a potato farm of some type*/
const double PotatoFarmMinimum = 0.20;
/** \brief If at least this proportion of beet then we have a beet farm of some type*/
const double BeetFarmMinimum = 0.20;
/** \brief If at least this proportion of vegatables then we have a vegatable farm of some type*/
const double VegFarmMinimum = 0.50;
/** \brief If at least this proportion of grazing pigs then we have a pig farm of some type*/
const double MinPigFarmGrazing = 0.15;

#define SIZEOF_FARMCROPAREAS  500


/** \brief The list of possible farm types */
enum class TTypesOfFarm {
	tof_ConvPig = 0,
	tof_ConvCattle,
	tof_ConvArable,
	tof_ConvHobby,
	tof_ConvMixedStock,
	tof_ConvPotato,
	tof_ConvBeet,
	tof_ConvVeg,
	tof_OrgPig,
	tof_OrgCattle,
	tof_OrgArable,
	tof_OrgHobby,
	tof_OrgMixedStock,
	tof_OrgPotato,
	tof_OrgBeet,
	tof_OrgVeg,
	tof_Other, // This is fruit farms, poultry with no crops, mink etc..
	tof_foobar
} ;

double FTCpcts[(int)TTypesOfFarm::tof_foobar][SIZEOF_FARMCROPAREAS];

std::unordered_set<int> new_cropstovs;

string farmtypenames[(int)TTypesOfFarm::tof_foobar] = { "ConvPig", "ConvCattle", "ConvArable", "ConvHobby", "ConvMixedStock", "ConvPotato",
"ConvBeet", "ConvVeg", "OrgPig", "OrgCattle", "OrgArable", "OrgHobby", "OrgMixedStock", "OrgPotato", "OrgBeet", "OrgVeg", "Other" };

/** \brief C++ way to convert int to string */
string convertInt(int number)
{
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}


/** \brief Class to hold and manipulate the farm data for each farm */
class FarmData
{
public:
	// Attributes
	/** \brief Array to hold crop areas */
	double m_cropareas[SIZEOF_FARMCROPAREAS]; // We assumne we don't have more than SIZEOF_FARMCROPAREAS crops
	/** \brief The total field area */
	double m_totalfieldarea;
	/** \brief The total crop area */
	double m_totalcroparea;
	/** \brief The total grass area */
	double m_totalfodderandgrassarea;
	/** \brief The total beet area */
	double m_totalbeetarea;
	/** \brief The total potato area */
	double m_totalpotatoarea;
	/** \brief The total vegetable area */
	double m_totalvegarea;
	/** \brief The area of grazing pigs */
	double m_totalgrazingpig;
	/** \brief Number of cattle on farm */
	double m_cattle;
	/** \brief Number of pigs on farm */
	double m_pigs;
	/** \brief Number of sheep, goats and deer on the farm */
	double m_sheep;
	/** \brief Number of other animals on the farm = poultry, mink etc */
	double m_other;
	/** \brief Is organic */
	int m_organic; // -1: uninitialized, 1: organic,  0: not organic  
	/** \brief Unique identifier */
	long m_FarmID;
	/** \brief The farm type */
	TTypesOfFarm m_type;
	// Methods
	FarmData(long a_ID);
	void AddArea(int a_crop, double a_area, int organic);
};

/** \brief FarmData constructor */
FarmData::FarmData(long a_ID)
{
	/** Initialises all variables, and sets the farm type to tof_foobar (default meaning unknown) */
	m_FarmID = a_ID;
	for (unsigned i = 0; i < SIZEOF_FARMCROPAREAS; i++) m_cropareas[i] = 0.0;
	m_totalfieldarea = 0;
	m_totalcroparea = 0;
	m_cattle = 0;
	m_pigs = 0;
	m_sheep = 0;
	m_other = 0;
	m_organic = -1;
	m_type = TTypesOfFarm::tof_foobar;

	m_totalpotatoarea = 0;
	m_totalbeetarea = 0;
	m_totalvegarea = 0;
	m_totalfodderandgrassarea = 0;
	m_totalgrazingpig = 0;

}

/** \brief Adds grown area to a crop type entry */
void FarmData::AddArea(int a_crop, double a_area, int organic)
{
	m_cropareas[a_crop] += a_area;
	
	// All fields must be organic, otherwise not an organic farm 
	if (m_organic == -1) // initialze with first field value 
		m_organic = organic;
	else if (m_organic == 1)
		m_organic = organic;
	// no override of value 0, from non-organic to organic 
}

/** \brief The list of farms */
vector<FarmData> Farms;
/** \brief The list of crops */
vector<std::string>cropstovs;

/** \brief Function to provide error output to console */
void Error(string str1, string str2)
{
	char ch;
	cout << str1 << endl;
	cout << str2 << endl;
	cout << "Enter a character and RETURN to exit :";
	cin >> ch;
	exit(0);
}

/** \brief Function to provide warnings for a file */
void Warn(string str1, int a_num)
{
	ofstream warnfile("warnings.txt", ios::app);
	warnfile << str1 << '\t' << a_num << endl;
	warnfile.close();
}

/** \brief Translates the crop reference codes to ALMaSS (external TOV) codes */
std::string GetCropCodeMapping(unsigned a_crop, int value = 0)
{
	std::string x;

	try
	{   if (value == 0) 
		{
		x = std::get<0>(CC2TOVANDCCDESC.at(a_crop));   // return the TOV string 
		}
		else if (value == 1)
		{
			x = std::get<1>(CC2TOVANDCCDESC.at(a_crop)); // return the LPIS Crop Code Description
		}
	}
	catch (const std::out_of_range& oor)
	{
		// Building set of missing crop codes 
		std::cout << "[GetAlmassCode] Out of Range error: " << oor.what() << " LPIS Code: " << a_crop << '\n';
		if ( value == 0 ) 
		{
			new_cropstovs.insert(a_crop);
		}
		return "ERROR";
	}

	return x;

}



/** \brief Checks the list of farms to see if we have this one - if not add it */
int CheckFarmIDNoAdd(long a_farmID)
{
	unsigned sz = (unsigned)Farms.size();
	for (unsigned i = 0; i < sz; i++)
	{
		if (Farms[i].m_FarmID == a_farmID) return i;
	}
	return -1;
}

/** \brief Checks the list of farms to see if we have this one - if not add it */
unsigned int CheckFarmID(long a_farmID)
{
	int index = CheckFarmIDNoAdd(a_farmID);
	if (index == -1)
	{
		FarmData f(a_farmID);
		Farms.push_back(f);
		return(unsigned) Farms.size() - 1;
	}
	else 
		return index;
}

/** \brief Checks the list of crops to see if we have this one - if not add it */
unsigned int CheckCropCode(std::string tovstring)
{
	for (unsigned i = 0; i < cropstovs.size(); i++)
	{
		if (cropstovs[i] == tovstring)
			return i;
	}
	cropstovs.push_back(tovstring);
	return (unsigned) cropstovs.size() - 1;
}

/** \brief Reads in the data for the crops. */
bool ReadDataCrops(string fname)
{
	ifstream IFile(fname);
	if (!IFile) {
		Error("Cannot open File ", fname);
	}
	string x;
	IFile >> x >> x >> x >> x >> x;  // Header: FIELD_ID IMK_AREAL AFGKODE organic FARM_ID
	if (strcmp("FARM_ID", x.c_str()) != 0) 
		Error("Column error in input file", "");
	// OK we are good to go
	double area;
	long farmID;
	long lastfarmID = -99999;
	int cropcode, cropcodeIndex, ALMaSSEle;
	std::string tovstring;
	int organic;
	unsigned farmIndex = 0;
	/** Loops through all the farms. */
	while ( ! IFile.eof() )
	{	
		if ( IFile >> x >> area >> cropcode >> organic >> farmID )
		{

			//cout << farmID << endl;
			/** First reads in a line and checks whether we have this farmID if we did not get this farm last time? */
			if (farmID != lastfarmID) // Speed up for when we have 42000 farmers & 620000 fields!
			{
				farmIndex = CheckFarmID(farmID);
				lastfarmID = farmID;
			}
			/** If this is a field of some sort then we can record it, otherwise ignore it, the farm may be one with no fields */
			if ( 1 )  // if (ALMaSSEle == 20 || ALMaSSEle == 35)
			{
				/** Next converts the cropcode to the ALMaSS ETOV code. */
				tovstring = GetCropCodeMapping(cropcode, 0);
				/** Then check if we have this crop, if not it is added.  */
				cropcodeIndex = CheckCropCode(tovstring);
				/** Finally assigns the area to the correct crop code and set the organic status of the farm. */
				Farms[farmIndex].AddArea(cropcodeIndex, area, organic);
			}

		}
	}
	IFile.close();
	return true;
}

/** \brief Reads in the data for the cropanimals. */
bool ReadDataAnimals(string fname)
{
	/*
	* File must be four columns with headers.  Col 0 = farm reference number, col 1 = no. cattle, col 2 = no. pigs, col 3 = no. other animals,.
	* Loops through all the farms, reading the information and assigning it to the respective farm.
	*/

	ifstream IFile(fname);
	if (!IFile) {
		Error("Cannot open File ", fname);
	}
	string x;
	// FARM_ID	cattle	sheep	pigs	other
	IFile >> x >> x >> x >> x >> x;
	if (strcmp("other", x.c_str()) != 0) Error("Column error in input file", "");
	// OK we are good to go
	int cattle, pigs, sheep, other;
	long farmID;
	while (!IFile.eof())
	{
		if ( IFile >> farmID >> cattle >> sheep >> pigs >> other )
		{
			// First find this FarmID
			unsigned sz = (unsigned) Farms.size();
			int farmIndex = CheckFarmIDNoAdd(farmID);
			if (farmIndex != -1) // Ignore if if we don't have fields
			{
				if (cattle < 0) cattle = 0;
				if (pigs < 0) pigs = 0;
				if (sheep < 0) sheep = 0;
				if (other< 0) other = 0;
				Farms[farmIndex].m_cattle = cattle;
				Farms[farmIndex].m_pigs = pigs;
				Farms[farmIndex].m_sheep = sheep / 2.5;
				Farms[farmIndex].m_other = other / 100;
				// if (organic == 1) Farms[farmIndex].m_organic = true;
				// Farms[farmIndex].m_organic = false;
			}
		}
	}
	IFile.close();
	return true;
}

/** \brief Output method for saving farms and their crops by area */
void Output1(string a_fname)
{
	ofstream OFile(a_fname);
	// Make the headers
	OFile << "FarmID" << '\t' << "FarmType" << '\t' << "Total_Field_Area";
	for (unsigned int c = 0; c < cropstovs.size(); c++) OFile << '\t' << cropstovs[c];
	OFile << '\t' << "No.Cattle" << '\t' << "No.Pigs" << '\t' << "No.Sheep" << '\t' << "No.Other" << endl;
	// Print the data
	unsigned sz = (unsigned)Farms.size();
	for (unsigned i = 0; i < sz; i++)
	{
		OFile << Farms[i].m_FarmID << '\t' << (int)Farms[i].m_type << '\t' << Farms[i].m_totalfieldarea;
		for (unsigned int c = 0; c < cropstovs.size(); c++) OFile << '\t' << Farms[i].m_cropareas[c];
		OFile << '\t' << Farms[i].m_cattle << '\t' << Farms[i].m_pigs << '\t' << Farms[i].m_sheep << '\t' << Farms[i].m_other << endl;
	}
	OFile.close();
}

/** \brief  Calculates the proportions of area occupied by all crops per farm */
void CalcCropProportions()
{
	/** Here we want to go through and convert all the crops to percentage total area */
	/** 1. Get total crop areas */
	unsigned sz = (unsigned) Farms.size();
	for (unsigned f = 0; f < sz; f++)
	{
		for (unsigned i = 0; i < SIZEOF_FARMCROPAREAS; i++) Farms[f].m_totalfieldarea += Farms[f].m_cropareas[i];
		/** 2. Then convert to proportions, but only if area is >0.0 */
		if (Farms[f].m_totalfieldarea > 0.0)
		{
			/** The percentages are saved back to the Farms m_cropareas array where the original areas were stored. */
			for (unsigned i = 0; i < SIZEOF_FARMCROPAREAS; i++) Farms[f].m_cropareas[i] = Farms[f].m_cropareas[i] / Farms[f].m_totalfieldarea;
		}
	}
}


/** \brief Classify the farms according to their animals and crop areas */
void AssignFarmType()
{

	/** This is the main part of the program where we have to make an analysis of the data and assign a farm type.\n
	* This is done by applying a set of rules.\n
	* Once a farm type is assigned this farm is removed from the assignment rules. This is controlled by the tof_foobar type, the default assignment.
	* In all cases if organic this is used to designate the organic variant of the conventional farm type.
	\n
	Rules used to classify the farms. These are found in #AssignFarmType and all farms can be designated as organic or not dependent upon the information on that farm in the GLR:
	- Farms with large proportion of vegetables set by #VegFarmMinimum and above 2-ha are organic or conventional vegetable farms, otherwise if small are classified as 'other'
	- Farms with a proportion of potatoes or sugar beet not less than #PotatoFarmMinimum or #BeetFarmMinimum are beet or potatoe farms respectively.
	- Farms with animal (cows, sheep and pigs) transformed to standard animal units that have fewer than #HobbyAnimalThreshold and an area less than HobbyAreaThreshold are designated as
	hobby farms
	- Farms with animal units above #HobbyAnimalThreshold and cattle+sheep above #CattleThreshold they are designated as cattle farms
	- Farms with animal units above #HobbyAnimalThreshold and pigs above #PigThreshold, or crop area of grazing pigs above #MinPigFarmGrazing they are designated as pig farms
	- Farms with animal units above #HobbyAnimalThreshold, but not pig or cattle farms, are designated as MixedStock
	- Farms with no animals registered but with large areas of grazing are assumed to be either cattle farms or mixed stock farms depending on whether grazing is above #CattleGrassThreshold
	or #MixedStockGrassMinimum
	- All remaining farms must be arable farms (i.e. large area with few or no animals).
	*/
	unsigned sz = (unsigned)Farms.size();
	double animals;
	
	for (unsigned f = 0; f < sz; f++)  // for each farm 
	{
		double pctfodderandgrass = 0.0;
		double pctcrops = 0.0;
		double pctveg = 0.0;
		double pctbeet = 0.0;
		double pctpotatoes = 0.0;
		double pctgrazepig = 0.0;

		/* Create the sets for documentation visibility */

		for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
		{
			double area = Farms[f].m_cropareas[c];     // crop area of this farm of this type 
			
			pctcrops += area;

			if ( cropstovs[c].find("Potato") != std::string::npos ) // if tov string contains word potatoes
			{ 
				pctpotatoes += area;				
			} 
			else if ((cropstovs[c]).find("SugarBeet") != std::string::npos) 
			{
				pctbeet += area;				
			}
			else if ((cropstovs[c].find("Cabbage") != std::string::npos) ||
				(cropstovs[c].find("Carrots") != std::string::npos) ||
				(cropstovs[c].find("MixedVeg") != std::string::npos) ||
				(cropstovs[c].find("VegSeed") != std::string::npos) ||
				(cropstovs[c].find("MixedVeg") != std::string::npos)) 
			{
				pctveg += area;				
			}
			else if (cropstovs[c].find("GrazingPig") != std::string::npos) 
			{
				pctgrazepig += area;				
			}
			else if ( (cropstovs[c].find("Silage") != std::string::npos) ||
				      (cropstovs[c].find("_Whole") != std::string::npos) ||
				      (cropstovs[c].find("_Green") != std::string::npos) ||
				      ((cropstovs[c].find("Grass") != std::string::npos) && 
				       (cropstovs[c].find("Seed") == std::string::npos)) ||
				      (cropstovs[c].find("CloverAlfa") != std::string::npos) )
			{
				pctfodderandgrass += area;
			}
	
		}

		// calculate the total areas for each crop type 
		Farms[f].m_totalcroparea = pctcrops; // includes potatoes, beet and veg
		Farms[f].m_totalpotatoarea = pctpotatoes;
		Farms[f].m_totalbeetarea = pctbeet;
		Farms[f].m_totalvegarea = pctveg;
		Farms[f].m_totalgrazingpig = pctgrazepig;
		Farms[f].m_totalfodderandgrassarea = pctfodderandgrass;

		// we can get some odd farms out of the way now.
		// Find the veg farms
		if (Farms[f].m_totalvegarea >= VegFarmMinimum)
		{
			if (Farms[f].m_organic) 
				Farms[f].m_type = TTypesOfFarm::tof_OrgVeg;
			else 
				Farms[f].m_type = TTypesOfFarm::tof_ConvVeg;
		}
		else if ( Farms[f].m_totalfieldarea < 2 ) 
		{
			Farms[f].m_type = TTypesOfFarm::tof_Other;
		}
		else
		// Find the potato farms
		if (Farms[f].m_totalpotatoarea >= PotatoFarmMinimum)
		{
			if (Farms[f].m_organic) 
				Farms[f].m_type = TTypesOfFarm::tof_OrgPotato;
			else 
				Farms[f].m_type = TTypesOfFarm::tof_ConvPotato;
		}
		// Find the beet farms
		else if (Farms[f].m_totalbeetarea >= BeetFarmMinimum)
		{
			if (Farms[f].m_organic) 
				Farms[f].m_type = TTypesOfFarm::tof_OrgBeet;
			else 
				Farms[f].m_type = TTypesOfFarm::tof_ConvBeet;
		}
	}
	
	for (unsigned f = 0; f < sz; f++)
	{
		if (Farms[f].m_type == TTypesOfFarm::tof_foobar)
		{
			// Animal numbers are weighted e.g. 1 cow = 2.5 sheep, so they can be added & dividied out
			animals = Farms[f].m_cattle + Farms[f].m_pigs + Farms[f].m_sheep + Farms[f].m_other;
			if (animals < HobbyAnimalThreshold)
			{
				if (Farms[f].m_totalfieldarea < HobbyAreaThreshold)
				{	
					if (Farms[f].m_organic) 
						Farms[f].m_type = TTypesOfFarm::tof_OrgHobby;
					else 
						Farms[f].m_type = TTypesOfFarm::tof_ConvHobby;
				}
			}
			else // if (animals >= HobbyAnimalThreshold) // must be a pig or cattle or mixed or poultry
			{
				bool cattle = false, pigs = false, other = false;

				if ((Farms[f].m_cattle + Farms[f].m_sheep) / (double)animals >= CattleThreshold) cattle = true; // Has a cattle designation
				if ((Farms[f].m_pigs / (double)animals >= PigThreshold) || (Farms[f].m_totalgrazingpig > MinPigFarmGrazing)) pigs = true; // Has a pig designation
				if (Farms[f].m_organic)
				{
					if (cattle) 
						Farms[f].m_type = TTypesOfFarm::tof_OrgCattle;
					else if (pigs) 
						Farms[f].m_type = TTypesOfFarm::tof_OrgPig;
					else 
						Farms[f].m_type = TTypesOfFarm::tof_OrgMixedStock; // this will include poultry and mink etc..
				}
				else
				{
					if (cattle) 
						Farms[f].m_type = TTypesOfFarm::tof_ConvCattle;
					else if (pigs) 
						Farms[f].m_type = TTypesOfFarm::tof_ConvPig;
					else 
						Farms[f].m_type = TTypesOfFarm::tof_ConvMixedStock;
				}
			}
		}
	}

	// By now all farms with lots of animals should be removed. However, there can be odd ones lurking with a lot of grass and few animals, so we need to catch
	// these since they must be stock farms (with cattle most likely).
	for (unsigned f = 0; f < sz; f++)
	{
		if ((Farms[f].m_type == TTypesOfFarm::tof_foobar))
		{
			double pctareafoddergrass = 0;
			
			if (Farms[f].m_totalfodderandgrassarea >= CattleGrassThreshold)
			{
				if (!Farms[f].m_organic)
					Farms[f].m_type = TTypesOfFarm::tof_ConvCattle;
				else 
					Farms[f].m_type = TTypesOfFarm::tof_OrgCattle;
			} else			
			if (Farms[f].m_totalfodderandgrassarea >= MixedStockGrassMinimum)
				{
					if (!Farms[f].m_organic)
						Farms[f].m_type = TTypesOfFarm::tof_ConvMixedStock;
					else 
						Farms[f].m_type = TTypesOfFarm::tof_OrgMixedStock;
				}
				// Now all farms with animals or a lot of grass have gone - we should be left with arable farms
				else if (Farms[f].m_organic) 
						Farms[f].m_type = TTypesOfFarm::tof_OrgArable;
					 else 
						Farms[f].m_type = TTypesOfFarm::tof_ConvArable;
		}
	}
}

/** \brief Calculates and outputs the mean crop proportions for each farm type */
void CalcFarmTypeCropPct()
{
	double areas[(int)TTypesOfFarm::tof_foobar];
	/**
	* First initialise all data arrays to zero.
	* Then loops through all the farms, summing crop proportions per farm type.
	* Here there is a problem that some farms have zero crop areas, and these must not be used for means.
	* Next it calculates the mean proportion for each crop type per farm type,
	* and finally saves the data in the output file. */
	for (unsigned t = 0; t < (int)TTypesOfFarm::tof_foobar; t++)
	{
		for (unsigned i = 0; i < SIZEOF_FARMCROPAREAS; i++)
			FTCpcts[t][i] = 0.0;
	}
	for (unsigned t = 0; t < (int)TTypesOfFarm::tof_foobar; t++)
		areas[t] = 0;

	// crop propertions 
	for (unsigned f = 0; f < Farms.size(); f++) // for each farm
	{
		unsigned ty = (unsigned)Farms[f].m_type; // farm type
		if (Farms[f].m_totalfieldarea > 0) // 
			areas[ty] ++; 
		for (unsigned i = 0; i < cropstovs.size(); i++)
		{
			FTCpcts[ty][i] += Farms[f].m_cropareas[i];
		}
	}

	// make absolute values to percentages 
	for (unsigned t = 0; t < (int)TTypesOfFarm::tof_foobar; t++)
	{
		for (unsigned i = 0; i < cropstovs.size(); i++)
		{
			if (areas[t] < 0.001) // This is in hectars
				FTCpcts[t][i] = 0; 
			else 
				FTCpcts[t][i] /= areas[t];
		}
	}

	ofstream OFile("FarmTypeCropPercentages.txt");

	/*
	// Make the headers
	OFile << "Crop Type";
	for (unsigned int c = 0; c < cropstovs.size(); c++) 
		OFile << '\t' << cropstovs[c];
	OFile << endl;
	// Print the data
	for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
	{
		OFile << farmtypenames[i];
		for (unsigned int c = 0; c < cropstovs.size(); c++) 
			OFile << '\t' << FTCpcts[i][c];
		OFile << endl;
	}
	OFile.close();
	*/

	// Make the headers
	OFile << "Farm Type:";
	for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
	{
		OFile << '\t' << farmtypenames[i];
	}
	OFile << endl;
	for (unsigned int c = 0; c < cropstovs.size(); c++)
	{
		// Write crop only if there is at least one non-zero fraction
		bool empty = true;
		for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
		{
			if (FTCpcts[i][c] != 0)
			{
				empty = false;
				break;
			}
		}
		if (empty)
		{
			continue;
		}
		OFile << cropstovs[c];
		for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
		{
			OFile << '\t' << FTCpcts[i][c];
		}
		OFile << endl;
	}
	OFile.close();
}

/** \brief Calculates and outputs the mean crop proportions for each farm type*/
void CalcFarmRotationPcts()
{
	/**
	* 1. Permanent Crops Removed and any values < 0.1
	* 2. Percentages Corrected for removal of low % rounded to 1%
	*/

	for (unsigned int c = 0; c < cropstovs.size(); c++)
	{
		for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
		{
			if ( (cropstovs[c].find("_Perm") != std::string::npos) ||     // Set permanent crops to zero 
				 (cropstovs[c].find("Undefined") != std::string::npos) ) 
			{
				FTCpcts[i][c] = 0.0;
			}

			if (FTCpcts[i][c] < 0.01) // If less than 1%, remove 
				FTCpcts[i][c] = 0.0;
		}
	}

	// correcting percentage 
	for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++) // for each type of farm 
	{
		double sum = 0;
		for (unsigned int c = 0; c < cropstovs.size(); c++)
		{
			sum += FTCpcts[i][c];
		}
		if (sum == 0) 
		{
			sum = 0; 
		}
		else 
		{
			sum = 1 / sum;
		}
		for (unsigned int c = 0; c < cropstovs.size(); c++)
		{
			FTCpcts[i][c] *= sum;
		}
	}

	// write to file 
	ofstream OFile("FarmRotationPercentages.txt");
	OFile << "Farm Type:";
	for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
	{
		OFile << '\t' << farmtypenames[i];
	}
	OFile << endl;

	for (unsigned int c = 0; c < cropstovs.size(); c++)
	{
		// Write crop only if there is at least one non-zero percentage
		bool empty = true;
		for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
		{
			if (FTCpcts[i][c] != 0)
			{
				empty = false;
				break;
			}
		}
		if (empty)
		{
			continue;
		}
		OFile << cropstovs[c];
		for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
		{
			OFile << '\t' << (int)floor(100 * FTCpcts[i][c] + 0.5);
		}
		OFile << endl;
	}
	OFile.close();
}

/** \brief Uses the rotation information to make a file ready to be manipulated into ALMaSS .rot files */
void SaveRotationHelperOutput()
{
	ofstream OFile("FarmRotationHelperOutput.txt");
	for (unsigned i = 0; i < (int)TTypesOfFarm::tof_foobar; i++)
	{
		OFile << farmtypenames[i];
		for (unsigned int c = 0; c < cropstovs.size(); c++)
		{
			int no = (int)floor(100 * FTCpcts[i][c] + 0.5);
			for (int n = 0; n < no; n++)
			{
				OFile << '\t' << cropstovs[c];
			}
		}
		OFile << endl;
	}
	OFile.close();
}

std::vector<std::string> findCCfromTOVs(std::string tov)
{
	std::vector<std::string> all_results; 
	for (auto& elem : CC2TOVANDCCDESC) { 	// Iterate through the our map 
		if (std::get<0>(elem.second) == tov)
			all_results.push_back(std::get<1>(elem.second));
	}
	return all_results;
}

void OutputCropRules4Tracing(std::string filename){
	
	ofstream OFile(filename);
	std::unordered_set<std::string> rule_tov_set;
	std::unordered_set<std::string> rule_ccdesc_set;
	std::vector<std::string> ccdesc;

	/* FodderAndGrass Rule */
	for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
	{
		if ((cropstovs[c].find("Silage") != std::string::npos) ||
			(cropstovs[c].find("_Whole") != std::string::npos) ||
			(cropstovs[c].find("_Green") != std::string::npos) ||
			((cropstovs[c].find("Grass") != std::string::npos) &&
				(cropstovs[c].find("Seed") == std::string::npos)) ||
			(cropstovs[c].find("CloverAlfa") != std::string::npos))
		{
			rule_tov_set.insert(cropstovs[c]);
			ccdesc = findCCfromTOVs(cropstovs[c]);
			for (const auto& v : ccdesc) {
				rule_ccdesc_set.insert(v);
			}
		}
	}
	OFile << "Fodder and Grass Rule" << std::endl;
	OFile << "*****************************" << std::endl;
	OFile << "TOVs:\t";
	for (const auto& c : rule_tov_set) {
		OFile << c << ", ";
	}
	OFile << "\nEquiv. Danish Crop Names:\t";
	for (const auto& c : rule_ccdesc_set) {
		OFile << c << ", ";
	}
	OFile << "\n\n";
	// clean the sets for the new rule 
	rule_tov_set.erase(rule_tov_set.begin(), rule_tov_set.end()); // erase the set 
	rule_ccdesc_set.erase(rule_ccdesc_set.begin(), rule_ccdesc_set.end()); // erase the set 


	/* Potato Rule */
	for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
	{
		if (cropstovs[c].find("Potato") != std::string::npos) // if tov string contains word 
		{
			rule_tov_set.insert(cropstovs[c]);
			ccdesc = findCCfromTOVs(cropstovs[c]);
			for (const auto& v : ccdesc) {
				rule_ccdesc_set.insert(v);
			}
		}
	}
	OFile << "Potato Rule" << std::endl;
	OFile << "*****************************" << std::endl;
	OFile << "TOVs:\t";
	for (const auto& c : rule_tov_set) {
		OFile << c << ", ";
	}
	OFile << "\nEquiv. Danish Crop Names:\t";
	for (const auto& c : rule_ccdesc_set) {
		OFile << c << ", ";
	}
	OFile << "\n\n";
	// clean the sets for the new rule 
	rule_tov_set.erase(rule_tov_set.begin(), rule_tov_set.end()); // erase the set 
	rule_ccdesc_set.erase(rule_ccdesc_set.begin(), rule_ccdesc_set.end()); // erase the set 


	/* SugarBeets Rule */
	for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
	{
		if (cropstovs[c].find("SugarBeet") != std::string::npos) // if tov string contains word 
		{
			rule_tov_set.insert(cropstovs[c]);
			ccdesc = findCCfromTOVs(cropstovs[c]);
			for (const auto& v : ccdesc) {
				rule_ccdesc_set.insert(v);
			}
		}
	}
	OFile << "SugarBeet Rule" << std::endl;
	OFile << "*****************************" << std::endl;
	OFile << "TOVs:\t";
	for (const auto& c : rule_tov_set) {
		OFile << c << ", ";
	}
	OFile << "\nEquiv. Danish Crop Names:\t";
	for (const auto& c : rule_ccdesc_set) {
		OFile << c << ", ";
	}
	OFile << "\n\n";
	// clean the sets for the new rule 
	rule_tov_set.erase(rule_tov_set.begin(), rule_tov_set.end()); // erase the set 
	rule_ccdesc_set.erase(rule_ccdesc_set.begin(), rule_ccdesc_set.end()); // erase the set 


	/* Veg Rule */
	for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
	{
		if ((cropstovs[c].find("Cabbage") != std::string::npos) ||
			(cropstovs[c].find("Carrots") != std::string::npos) ||
			(cropstovs[c].find("MixedVeg") != std::string::npos) ||
			(cropstovs[c].find("VegSeed") != std::string::npos) ||
			(cropstovs[c].find("MixedVeg") != std::string::npos))
		{
			rule_tov_set.insert(cropstovs[c]);
			ccdesc = findCCfromTOVs(cropstovs[c]);
			for (const auto& v : ccdesc) {
				rule_ccdesc_set.insert(v);
			}
		}
	}
	OFile << "Veg Rule" << std::endl;
	OFile << "*****************************" << std::endl;
	OFile << "TOVs:\t";
	for (const auto& c : rule_tov_set) {
		OFile << c << ", ";
	}
	OFile << "\nEquiv. Danish Crop Names:\t";
	for (const auto& c : rule_ccdesc_set) {
		OFile << c << ", ";
	}
	OFile << "\n\n";
	// clean the sets for the new rule 
	rule_tov_set.erase(rule_tov_set.begin(), rule_tov_set.end()); // erase the set 
	rule_ccdesc_set.erase(rule_ccdesc_set.begin(), rule_ccdesc_set.end()); // erase the set 


	/* GrazingPig Rule */
	for (unsigned c = 0; c < cropstovs.size(); c++) // for each crop 
	{
		if (cropstovs[c].find("GrazingPig") != std::string::npos)
		{
			rule_tov_set.insert(cropstovs[c]);
			ccdesc = findCCfromTOVs(cropstovs[c]);
			for (const auto& v : ccdesc) {
				rule_ccdesc_set.insert(v);
			}
		}
	}
	OFile << "Grazing Pig Rule" << std::endl;
	OFile << "*****************************" << std::endl;
	OFile << "TOVs:\t";
	for (const auto& c : rule_tov_set) {
		OFile << c << ", ";
	}
	OFile << "\nEquiv. Danish Crop Names:\t";
	for (const auto& c : rule_ccdesc_set) {
		OFile << c << ", ";
	}
	OFile << "\n\n";
	// clean the sets for the new rule 
	rule_tov_set.erase(rule_tov_set.begin(), rule_tov_set.end()); // erase the set 
	rule_ccdesc_set.erase(rule_ccdesc_set.begin(), rule_ccdesc_set.end()); // erase the set 


	OFile.close();
}

int main()
{
	
	ReadDataCrops("fields_data.dat");

	// Write the LPIS Crop Codes unmapped to TOVs (unknown) into a file  - should be empty in the end of the dev
	{
		std::ofstream ncc_file;
		ncc_file.open("Missingcropstovs.txt");
		ncc_file << "cropstovs" << std::endl;
		for (auto c : new_cropstovs)
			ncc_file << c << std::endl;
		ncc_file.close();
	}

	// count how many farms are organic
	{
		int co = 0;
		for (int i = 0; i < (unsigned)Farms.size(); i++)
			if (Farms[i].m_organic == 1)
				co++;
		std::cout << "Nb of organic farms = " << co << "\n";
	}

	ReadDataAnimals("animals_data.dat");
	Output1("FarmsDataRaw.txt");
	CalcCropProportions();
	AssignFarmType();
	Output1("FarmsDataProps.txt");
	CalcFarmTypeCropPct();
	CalcFarmRotationPcts();
	SaveRotationHelperOutput();


	OutputCropRules4Tracing("Trace_CropRules.txt");

	return 0;
}

