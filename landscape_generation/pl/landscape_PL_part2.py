#=======================================================================================================================
### Name: Landscape generator for Poland, part2
### Purpose: The script cleans up the raw landscape map from sliver polygons and creats ALMaSS inputs
### Authors: Elzbieta Ziolkowska - June 2017
### Last large update: January 2019

#===== Chunk: General setup =====#

### Import system modules
import arcpy, traceback, sys, time, gc, shutil, os, csv, math
import numpy as np
import pandas as pd
#reload(sys)
#sys.setdefaultencoding('utf8')
from dbfpy import dbf
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part2 started: " + nowTime
print "... system modules checked"

### Defining all neccesary functions

# Defining dbf to csv conversion function
def dbf_to_csv(out_table):#Input a dbf, output a csv
    csv_fn = out_table[:-4]+ ".csv" #Set the table as .csv format
    with open(csv_fn,'wb') as csvfile: #Create a csv file and write contents from dbf
        in_db = dbf.Dbf(out_table)
        out_csv = csv.writer(csvfile)
        names = []
        for field in in_db.header.fields: #Write headers
            names.append(field.name)
        out_csv.writerow(names)
        for rec in in_db: #Write records
            out_csv.writerow(rec.fieldData)
        in_db.close()
    return csv_fn

# Defining table to pandas data frame conversion
def table_to_data_frame(in_table, input_fields=None, where_clause=None):
    """Function will convert an arcgis table into a pandas dataframe with an object ID index, and the selected
    input fields using an arcpy.da.SearchCursor."""
    OIDFieldName = arcpy.Describe(in_table).OIDFieldName
    if input_fields:
        final_fields = [OIDFieldName] + input_fields
    else:
        final_fields = [field.name for field in arcpy.ListFields(in_table)]
    data = [row for row in arcpy.da.SearchCursor(in_table, final_fields, where_clause=where_clause)]
    fc_dataframe = pd.DataFrame(data, columns=final_fields)
    fc_dataframe = fc_dataframe.set_index(OIDFieldName, drop=True)
    return fc_dataframe


### Set fixed paths
dst = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas_new/"  # Output destination
reclasstable = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas/reclasstable_new.txt"     # Reclass ascii table
agriDB = "D:/dane_GIS/ARiMR_PL/agricultural_register/"
woj = "D:/dane_GIS/PRG_jednostki_administracyjne_v3/wojewodztwa.shp"
arcpy.MakeFeatureLayer_management(woj, "woj_lyr")
#soilraster = XXX      # Soil data

# We need to set these each time we loop through otherwise we recycle the ones from the previous run  
defaultextent = arcpy.env.extent
defaultmask = arcpy.env.mask

outCS = arcpy.SpatialReference(2180)

### Setting a list with all the landscapes to process
landscapes = ["KR_GS", "NG_GS"]

#===== End chunk: General setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
    print "Processing landscape " + landscapes[index] + " ..."

    #===== Chunk: Landscape-specific setup =====#

    arcpy.env.extent = defaultextent
    arcpy.env.mask = defaultmask

    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    outPath = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
    gisDB = os.path.join(dst, landscapes[index], landscapes[index] + ".gdb/")
    scratchDB = os.path.join(dst, landscapes[index], "outputs/scratch.gdb/")  # scratch folder for tempfiles
    scratch = os.path.join(dst, landscapes[index], "outputs/scratch/")  # scratch folder for tempfiles
    #localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")   # project folder with mask
    localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask_buffered1km")   # project folder with mask

    asciifile = "ASCII_" + landscapes[index] + ".txt"
    asciiexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", asciifile)  # export in ascii (for ALMaSS)
    attrtable = "ATTR_" + landscapes[index] + ".csv"  # Name of attribute table
    attrexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", attrtable)  # full path 
    soiltable = "SOIL_" + landscapes[index] + ".csv"  # Name of soil type table
    soilexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", soiltable)  # full path
    fieldstable = "FIELDS_" + landscapes[index] + ".dbf"  # Name of fields table
    fieldsexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", fieldstable)  # full path
    permtable = "PERM_CROPS_" + landscapes[index] + ".csv"  # Name of permanent crops table
    permexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files")  # folder

    # Deleting folder with ALMaSS outputs if exists
    if os.path.exists(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files")):
        shutil.rmtree(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))
    print "... deleting existing folder"
    os.mkdir(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"
    print " "

    #===== End chunk: Landscape-specific setup =====#

    try:

    #===== Chunk: Elimination of sliver polygons =====#
        print "Eliminating sliver polygons ..."

        #step0: removing water from the process (111-113: drainage ditches, 121-123: channels, 131-133: streams, 141: rivers, 143: lakes, 151-155: dikes)
        print "Step 0: Removing water from the process ..."
        map0_NW = SetNull(outPath + "MapRaw", outPath + "MapRaw", "VALUE IN (111, 112, 113, 121, 122, 123, 131, 132, 133, 141, 143, 151, 152, 153, 154, 155)")
        map0_NW.save(scratchDB + "map0_NW")
        arcpy.RasterToPolygon_conversion(map0_NW, scratchDB + "map0_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 0 finished ..."

        #step1: checking if any big background polygons (> 1ha) exists and if so convert them into fields
        print "Step 1: Checking for big (>1ha) background polygons ..."
        arcpy.Select_analysis("PTTR_A", scratchDB + "arable_land", "x_kod = 'PTTR02'")
        arcpy.PolygonToRaster_conversion(scratchDB + "arable_land", "OBJECTID", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
        outCon = Con(IsNull(Raster(scratchDB + "tmpRaster")), 0, 1)

        map0_NW_MAXResult = arcpy.GetRasterProperties_management(scratchDB + "map0_NW", "MAXIMUM")
        map0_NW_MAX = map0_NW_MAXResult.getOutput(0)

        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map0_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection")
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "SUBSET_SELECTION", "(Shape_Area >= 10000) AND (Shape_Area/Shape_Length>5)")
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection2")

        if int(arcpy.GetCount_management(scratchDB + "map0_NW_selection2").getOutput(0))>0:
            ZonalStatisticsAsTable(scratchDB + "map0_NW_selection2", "Id", outCon, scratchDB + "map0_NW_table", "NODATA", "ALL")
            arcpy.JoinField_management (scratchDB + "map0_NW_vector", "Id", scratchDB + "map0_NW_table", "Id", ["MEAN"])
            cursor = arcpy.UpdateCursor(scratchDB + "map0_NW_vector")
            i = 1
            for row in cursor:
                if row.getValue('MEAN')>=0.8:
                    print int(map0_NW_MAX) + i
                    row.setValue('gridcode', int(map0_NW_MAX) + i)   # setting gridcode for a new field
                    i = i+1
                cursor.updateRow(row)

        # Updating field1000 layer and adding info on farm type (all new fields belongs to one big farm of most common type 1)
        arcpy.Update_analysis("fields_final", scratchDB + "map0_NW_selection2", scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields", "BORDERS")
        cursor=arcpy.SearchCursor(scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields")
        farmTypeList=[]
        for row in cursor:
            farmTypeList.append(row.getValue("FarmType"))
        farmTypeList[:] = (value for value in farmTypeList if value != 0)
        farmTypeMajority = max(set(farmTypeList), key = farmTypeList.count)

        cursor = arcpy.UpdateCursor(scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields")
        j=1
        for row in cursor:
            if ((row.getValue('FarmType')==0) and (row.getValue('ZAKODOWANY_NR_PRODUCENTA')==0)):
                row.setValue('IDENT_DZIALKI_EWIDENCYJNEJ', "added_field_no" + str(j))
                row.setValue('FarmType', farmTypeMajority)   # setting FarmType for a new field
                row.setValue('gridcode', int(map0_NW_MAX) + j)   # setting gridcode for a new field
                row.setValue('ALMaSScode', 20)   # setting gridcode for a new field
                j=j+1
            cursor.updateRow(row)

        arcpy.PolygonToRaster_conversion(scratchDB + "map0_NW_vector", "gridcode", scratchDB + "map1_NW", "", "", 1)
        arcpy.Delete_management(outPath + "tmpRaster")
        print "Step 1 finished ..."

        #step2: removing elongated background polygons
        print "Step 2: Removing elongated background polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map1_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map1_NW_lyr", scratchDB + "map1_NW_selection")

        arcpy.MinimumBoundingGeometry_management(scratchDB + "map1_NW_selection", scratchDB + "map1_NW_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map1_NW_selection", "Id", scratchDB + "map1_NW_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map1_NW_selection", "map1_NW_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4")
        #arcpy.SelectLayerByAttribute_management("map1_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4 AND Shape_Area < 1500")
        ##arcpy.PolygonToRaster_conversion("map1_NW_selection_lyr", "gridcode", scratchDB + "map1_NW_selection_lyr_raster", "", "", 1)
        ##Reclass2 = Con(IsNull(scratchDB + "map1_NW_selection_lyr_raster"), 2, 1)
        ##toNibble2 = SetNull(Reclass2, 2, "Value = 1")
        ##map2_NW = Nibble(scratchDB + "map1_NW", toNibble2, "DATA_ONLY")
        ##map2_NW.save(scratchDB + "map2_NW")
        ##arcpy.RasterToPolygon_conversion(map2_NW, scratchDB + "map2_NW_vector", "NO_SIMPLIFY", "VALUE")

        # new way of dealing with elongated background polygons without Nibble
        arcpy.CopyFeatures_management("map1_NW_selection_lyr", scratchDB + "map1_NW_selection2")
        arcpy.PolygonNeighbors_analysis(scratchDB + "map0_NW_vector", scratchDB + "map1_NW_neighbors", ["Id", "gridcode"])
        map1_NW_neighbors=table_to_data_frame(scratchDB + "map1_NW_neighbors")
        map1_NW_neighbors=map1_NW_neighbors[map1_NW_neighbors.groupby(['src_Id'])['LENGTH'].transform(max) == map1_NW_neighbors['LENGTH']]
        map1_NW_neighbors.to_csv(scratch + "map1_NW_neighbors_grouped.txt", sep="\t")
        arcpy.CopyRows_management(scratch + "map1_NW_neighbors_grouped.txt", scratchDB + "map1_NW_neighbors_grouped_table")
        arcpy.JoinField_management(scratchDB + "map1_NW_selection2", "Id", scratchDB + "map1_NW_neighbors_grouped_table", "src_Id", "")
        arcpy.PolygonToRaster_conversion(scratchDB + "map1_NW_selection2", "nbr_gridcode", scratchDB + "map1_NW_selection2_raster", "", "", 1)
        neighborCon = Con(scratchDB + 'map1_NW_selection2_raster', 900,570, "Value>1000") #if the background polygons is neighboring with the field then convert it to field margins, otherwise set is as 'wasteland'
        map2_NW = Con(IsNull(neighborCon), scratchDB + "map1_NW", neighborCon)
        map2_NW.save(scratchDB + "map2_NW")
        arcpy.RasterToPolygon_conversion(map2_NW, scratchDB + "map2_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 2 finished ..."

        #step3: removing small parts of multi-divided fields
        print "Step 3: Checking for small parts of multi-divided fields ..."
        cursor=arcpy.SearchCursor(scratchDB + "map2_NW_vector")
        fields_list=[]
        duplicates=[]
        for row in cursor:
            a=row.getValue("gridcode")
            if a>=1000:
                if a in fields_list:
                    duplicates.append(a)
                else:
                    fields_list.append(a)
              
        duplicates=list(dict.fromkeys(duplicates))

        if len(duplicates)>0:
            print "multi-divided fields found ..."
            whereClause = "VALUE IN (" + str(duplicates)[1:-1] + ")"
            toCheck3 = Con(scratchDB + "map2_NW", scratchDB + "map2_NW", 0, whereClause)
            regGr3 = RegionGroup(toCheck3, "FOUR", "WITHIN", "", 0)
            ##toNibble3 = SetNull(regGr3, 1, "COUNT < 1000")
            ##map3_NW = Nibble(scratchDB + "map2_NW", toNibble3, "DATA_ONLY")
            
            # new way without Nibble
            map3_NW = Con(regGr3, 570, scratchDB + "map2_NW", "COUNT < 1000")
            map3_NW.save(scratchDB + "map3_NW")
            arcpy.RasterToPolygon_conversion(map3_NW, scratchDB + "map3_NW_vector", "NO_SIMPLIFY", "VALUE")
            print "small multi-divided fields removed ..."    
        else:
            print "no multi-divided fields found ..."
            arcpy.CopyRaster_management(scratchDB + "map2_NW", scratchDB + "map3_NW")
            arcpy.CopyFeatures_management(scratchDB + "map2_NW_vector", scratchDB + "map3_NW_vector")
        print "Step 3 finished ..."

        #step4: adding the rivers back
        print "Step 4: Adding water ..."
        map4 = Con(IsNull(scratchDB + "map0_NW"), Raster(outPath + "MapRaw"), Raster(scratchDB + "map3_NW"))
        map4.save(scratchDB + "map4")
        # assigning riverside plants to all gaps, i.e., 'NODATA' values
        map4_fill = Con(IsNull(map4), 170, map4) # setting new category 'river side plants'
        map4_fill.save(scratchDB + "map4_fill")
        arcpy.RasterToPolygon_conversion(map4_fill, scratchDB + "map4_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 4 finished ..."

        #step5: filling the background gaps
        print "Step 5: Filling the remaining gaps ..."
        gridcodeValues = []
        cursor = arcpy.SearchCursor(scratchDB + "map4_vector")
        for row in cursor:
            gridcodeValues.append(row.getValue('gridcode'))

        if 1 in gridcodeValues:
            print "Still some background pixels left ..."
            arcpy.PolygonNeighbors_analysis(scratchDB + "map4_vector", scratchDB + "map4_vector_neighbors")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors", "src_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors", scratchDB + "map4_vector_neighbors_selection", "gridcode = 1")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors_selection", "nbr_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors_selection", scratchDB + "map4_vector_neighbors_selection2", "gridcode_1 >= 411 AND gridcode_1 <= 472")
            arcpy.JoinField_management (scratchDB + "map4_vector", "OBJECTID", scratchDB + "map4_vector_neighbors_selection2", "src_OBJECTID")
            cursor = arcpy.UpdateCursor(scratchDB + "map4_vector")
            for row in cursor:
                if row.getValue('gridcode')==1:
                    if row.getValue('src_OBJECTID')>0:
                        row.setValue('gridcode', 440)   # setting category 'yards' to selected background objects
                    else:
                        row.setValue('gridcode', 570)   # setting category 'wasteland' to the rest of 'background' objects
                cursor.updateRow(row)
        else:
            print "No background pixels found ..."

            
        arcpy.PolygonToRaster_conversion(scratchDB + "map4_vector", "gridcode", scratchDB + "map5", "", "", 1)
        map5 = Raster(scratchDB + "map5")
        print "Step 5 finished ..."

        #step6: Cleaning of elongated sliver field polygons, e.g. left overs located between line of trees/hedgerow and road verge
        print "Step 6: Cleaning of elongated sliver field polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_vector", "map4_lyr")
        arcpy.SelectLayerByAttribute_management("map4_lyr", "NEW_SELECTION", "gridcode >= 1000")
        arcpy.CopyFeatures_management("map4_lyr", scratchDB + "map4_selection")
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map4_selection", scratchDB + "map4_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map4_selection", "Id", scratchDB + "map4_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_selection", "map4_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map4_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/Shape_Length<4")
        arcpy.CopyFeatures_management("map4_selection_lyr", scratchDB + "map4_selection2")
        if int(arcpy.GetCount_management(scratchDB + "map4_selection2").getOutput(0))>0:
            arcpy.PolygonToRaster_conversion(scratchDB + "map4_selection2", "gridcode", scratchDB + "map4_selection2_raster", "", "", 1)
            #Reclass6 = Con(IsNull(scratchDB + "map4_selection2_raster"), 2, 1)
            #toNibble6 = SetNull(Reclass6, 2, "Value = 1")
            #map6 = Nibble(map5, toNibble6, "DATA_ONLY")
            # new way without Nibble
            map6 = Con(IsNull(scratchDB + "map4_selection2_raster"), scratchDB + "map5", 570) # setting category 'wasteland' to the elongated sliver field polygons
            map6.save(scratchDB + "map6")
            arcpy.RasterToPolygon_conversion(map6, scratchDB + "map6_vector", "NO_SIMPLIFY", "VALUE")
        else:
            print "no cleaning needed ..."
            arcpy.CopyRaster_management(scratchDB + "map5", scratchDB + "map6")
            arcpy.CopyFeatures_management(scratchDB + "map4_vector", scratchDB + "map6_vector")
        print "Step 6 finished ..."

        #step7: Splitting the fields with multiple entries in the agricultural register
        print "Step 7: Splitting fiels with multiple entries in the agricultural register ..."
        arcpy.Dissolve_management(scratchDB + "map6_vector", scratchDB + "map6_vector_dissolve", ["gridcode"], "", "", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map6_vector_dissolve", "map6_vector_dissolve_lyr")
        arcpy.SelectLayerByAttribute_management('map6_vector_dissolve_lyr', "NEW_SELECTION", "gridcode >= 1000")
        arcpy.CopyFeatures_management('map6_vector_dissolve_lyr', scratchDB + "fields")
        arcpy.JoinField_management(scratchDB + "fields", "gridcode", scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields", "gridcode", ["IDENT_DZIALKI_EWIDENCYJNEJ", "ZAKODOWANY_NR_PRODUCENTA", "FarmType", "ALMaSScode"])
        arcpy.Project_management(scratchDB + "fields", scratchDB + "fields_projected", outCS)
        arcpy.MinimumBoundingGeometry_management(scratchDB + "fields_projected", scratchDB + "fields_projected_mbg", "RECTANGLE_BY_WIDTH", "", "", "MBG_FIELDS")
        arcpy.JoinField_management (scratchDB + "fields_projected", "OBJECTID", scratchDB + "fields_projected_mbg", "OBJECTID", ["MBG_Width", "MBG_Length", "MBG_Orientation"])
        arcpy.AddField_management(scratchDB + "fields_projected", "crop", "TEXT")
        arcpy.CreateFeatureclass_management(scratchDB, "fields_splitted", "POLYGON", scratchDB + "fields_projected")
        arcpy.DefineProjection_management(scratchDB + "fields_splitted", outCS)
        poly = scratchDB + "fields_projected"
        poly_count = arcpy.GetCount_management(poly)
        poly2 = scratchDB + "fields_splitted"

        # Select the proper 'woj'
        print "Selecting woj intersecting the study area ..."
        arcpy.SelectLayerByLocation_management('woj_lyr', 'intersect', localSettings)
        arcpy.CopyFeatures_management('woj_lyr', scratchDB + 'woj_selected')
        wojIDList = []
        agri_register = pd.DataFrame()
        cursor_woj = arcpy.SearchCursor(scratchDB + 'woj_selected')
        for row_woj in cursor_woj:
            wojID = row_woj.getValue('jpt_kod_je')
            wojIDList.append(wojID)
        for wojID in wojIDList:
            if len(str(wojID)) <2:
                IDfull = "0" + str(wojID)
            else:
                IDfull = str(wojID)
                print "Processing woj " + IDfull

            agri_register_woj = pd.read_csv(agriDB + "agri_register2018_2017_woj" + IDfull + "_eng.csv", sep = ",", decimal=',')
            agri_register = agri_register.append(agri_register_woj,ignore_index=True)

        #        fields_area_sums = agri_register.groupby(['IDENT_DZIALKI_EWIDENCYJNEJ']).sum()['POWIERZCHNIA']
        fields_area_sums = agri_register.groupby(['IDENT_DZIALKI_EWIDENCYJNEJ']).sum()['POWIERZCHNIA_2017']
        agri_register_join = agri_register.join(fields_area_sums, on="IDENT_DZIALKI_EWIDENCYJNEJ", how="left", lsuffix='l', rsuffix='r')
        #        agri_register_join["POWIERZCHNIA_per"]=agri_register_join["POWIERZCHNIAl"]/agri_register_join["POWIERZCHNIAr"]*100
        agri_register_join["POWIERZCHNIA_per"]=agri_register_join["POWIERZCHNIA_2017l"]/agri_register_join["POWIERZCHNIA_2017r"]*100
        print "Agricultural register for proper woj uploaded ..."

        # get polygon and extent properties
        counter1 = 0 #not found in the agri register
        counter2 = 0 #found in agri register
        counter3 = 0 #need to be cutted
        counter4 = 0 #cutting failed

        with arcpy.da.SearchCursor(poly, ["SHAPE@", "MBG_Length", "MBG_Width", "MBG_Orientation", "IDENT_DZIALKI_EWIDENCYJNEJ", "ZAKODOWANY_NR_PRODUCENTA", "FarmType", "ALMaSScode"]) as pcursor:  
            for prow in pcursor:
                polygon = prow[0]    # polygon to cut  
                e = polygon.extent   # bounding extent of polygon  
                #print e.XMin,e.YMin,e.XMax,e.YMax
                MBG_Length = prow[1]
                MBG_Width = prow[2]
                MBG_Orient = prow[3]
                Poly_Width = MBG_Width
                Poly_Length = MBG_Length
                field_ID = prow[4]
                owner_ID = prow[5]
                farmtype = prow[6]
                almasscode = prow[7]
                print "Processing field " + field_ID
                print "orientation ..." + str(MBG_Orient) + " degrees"

                icursor = arcpy.da.InsertCursor(poly2, ["SHAPE@", "IDENT_DZIALKI_EWIDENCYJNEJ", "ZAKODOWANY_NR_PRODUCENTA", "FarmType", "crop", "ALMaSScode"])
              
                # looking if to devide fields and how many parts are needed
                agri_entries = agri_register_join.index[agri_register_join['IDENT_DZIALKI_EWIDENCYJNEJ']==field_ID].tolist()
                if len(agri_entries) == 0:
                    print "Agri entries empty"
                    icursor.insertRow([polygon, field_ID, owner_ID, farmtype, "", almasscode])
                    del icursor
                    counter1 +=1
                else:
                    print "Processing ..."
                    counter2 += 1
                 
                    # split steps
                    splits = agri_register_join.loc[agri_entries,"POWIERZCHNIA_per"].tolist()                
                    #crops = agri_register_join.loc[agri_entries,"eng_group"].tolist()
                    crops = agri_register_join.loc[agri_entries,"eng_group_2017"].tolist()
                    splits_array = np.array(splits)
                    crops_array = np.array(crops)
                    idx = np.argsort(splits_array)
                    splits_sorted = list(splits_array[idx])
                    crops_sorted = list(crops_array[idx])
                    print splits_sorted
                    print crops_sorted
                          
                    if len(splits)<2:
                        print "No cutting needed ..."
                        icursor.insertRow([polygon, field_ID, owner_ID, farmtype, crops[0], almasscode])
                        del icursor
                    else:
                        print "Cutting into " + str(len(splits)) + " parts ..."
                        counter3 += 1
                        cutpoly = polygon

                        # geometric cut test iteration step size - modify value to get better accuracy  
                        stepsize = 1.0
                    
                        if MBG_Orient == 90:
                            print "special case"
                            X1 = e.XMin
                            X2 = e.XMax
                            Y1 = e.YMin
                            Y2 = e.YMin
                            shift1 = 0
                            shift2 = 0
                            stepsizeX = 0
                            stepsizeY = stepsize
                        else:
                            X1 = e.XMin
                            X2 = e.XMin
                            Y1 = e.YMax
                            Y2 = e.YMin
                            stepsizeX = stepsize
                            stepsizeY = 0
                            if MBG_Orient == 0:
                                shift1=0
                                shift2=0  
                            else:
                                # convert polygon into vertices
                                arcpy.FeatureVerticesToPoints_management(cutpoly, scratchDB + "points", "ALL")
                                # find y minimum
                                list_points = []

                                with arcpy.da.SearchCursor(scratchDB + "points", ['SHAPE@X', 'SHAPE@Y']) as cursor:
                                    for row in cursor:
                                        list_points.append(row)
                                del cursor

                                miny_point = min(list_points)[1]

                              # defining shifts                  
                                if MBG_Orient < 90:
                                    shift1 = (miny_point - Y2) * math.tan(math.radians(MBG_Orient))
                                    shift2 = (Y1 - miny_point) * math.tan(math.radians(MBG_Orient))
                                else:
                                    shift1 = (miny_point - Y2) * math.tan(math.radians(MBG_Orient - 90))
                                    shift2 = (Y1 - miny_point) * math.tan(math.radians(MBG_Orient - 90))

                        # iterate cut percentage list
                        try:
                            print "Trying to cut along the long axis ..."
                            for i in range(len(splits_sorted)-1):
                                print splits_sorted[i]  
                                tol = 0
                                while tol < splits_sorted[i]:          
                                    # construct NS line  
                                    pntarray = arcpy.Array()  
                                    pntarray.add(arcpy.Point(X1 + shift2 + stepsizeX, Y1 + stepsizeY))  
                                    pntarray.add(arcpy.Point(X2 - shift1 + stepsizeX, Y2 + stepsizeY))  
                                    pline = arcpy.Polyline(pntarray,arcpy.SpatialReference(2180))
                                    # arcpy.CopyFeatures_management(pline, "line" + str(int(stepsize)))
                                    # cut polygon and get split-parts  
                                    cutlist = cutpoly.cut(pline)
                                    tol = 100 * cutlist[1].area / polygon.area
                                    if stepsizeX > 0:
                                        stepsizeX += stepsize
                                    if stepsizeY > 0:
                                        stepsizeY += stepsize
                                 #stepsize=stepsize+1
                                cutpoly = cutlist[0]
                                print "done cutting at stepsizeX ... " + str(stepsizeX) + " and stepsizeY ... " + str(stepsizeY)
                                # part 0 is on the right side and part 1 is on the left side of the cut  
                                icursor.insertRow([cutlist[1], field_ID, owner_ID, farmtype, crops_sorted[i], almasscode])
                                print "done"
                            # insert last cut remainder  
                            icursor.insertRow([cutlist[0], field_ID, owner_ID, farmtype, crops_sorted[-1], almasscode])
                            del icursor
                            print "Cutting done ..."
                        except RuntimeError:
                            counter4 += 1
                            print "Cutting failed ..."
                print str(counter1 + counter2) + " fields processed out of " + poly_count.getOutput(0)
            del pcursor

        # Dealing with non-processed polygons (if any due to errors)
        arcpy.Erase_analysis (poly, poly2, scratchDB + "fields_projected_leftovers")
        arcpy.Merge_management ([poly2, scratchDB + "fields_projected_leftovers"], scratchDB + "fields_splitted_all")
        print "Cutting fields finished ..."

        # Joining fields with the same management (crop type) and owner
        arcpy.MultipartToSinglepart_management (scratchDB + "fields_splitted_all", scratchDB + "fields_splitted_all_singlepart")
        arcpy.MakeFeatureLayer_management(scratchDB + "fields_splitted_all_singlepart", "fields_splitted_all_lyr")
        arcpy.SelectLayerByAttribute_management("fields_splitted_all_lyr", "NEW_SELECTION", "crop <> '' AND crop IS NOT NULL")
        arcpy.CopyFeatures_management("fields_splitted_all_lyr", scratchDB + "fields_splitted_all_selection")
        arcpy.Dissolve_management (scratchDB + "fields_splitted_all_selection", scratchDB + "fields_splitted_selection_dissolve", ["ZAKODOWANY_NR_PRODUCENTA", "crop"], [["FarmType", "FIRST"], ["ALMaSScode", "FIRST"]], "SINGLE_PART")
        arcpy.Update_analysis(scratchDB + "fields_splitted_all_singlepart", scratchDB + "fields_splitted_selection_dissolve", scratchDB + "fields_splitted_dissolve")
        arcpy.PolygonToRaster_conversion(scratchDB + "fields_splitted_dissolve", "OBJECTID", scratchDB + "fields_after_processing", "", "", 1)
        print "Joining fields with the same management finished ..."

        # Removing small leftovers (< 100px)
        toCheck = Con(scratchDB + "fields_after_processing", scratchDB + "fields_after_processing", 0, "Count > 100")
        outCon = Con(IsNull(toCheck), 1, toCheck)
        toNibble = SetNull(outCon, 1, "Value = 0")
        toNibble.save(scratchDB + "toNibble")
        result = Nibble(scratchDB + "fields_after_processing", scratchDB + "toNibble", "DATA_ONLY")
        result.save(scratchDB + "result")
        resultPlus = Plus(result, 1000)
        resultPlus.save(scratchDB + "resultPlus")

        arcpy.RasterToPolygon_conversion(result, scratchDB + "fields_after_processing_final", "NO_SIMPLIFY", "VALUE")
        arcpy.JoinField_management(scratchDB + "fields_after_processing_final", "gridcode", scratchDB + "fields_splitted_dissolve", "OBJECTID")

        with arcpy.da.UpdateCursor(scratchDB + "fields_after_processing_final", ['gridcode']) as cursor:
            for row in cursor:
                row[0] = row[0] + 1000
                cursor.updateRow(row)
        del cursor

        outMerge = Con(IsNull(scratchDB + "resultPlus"), scratchDB + "map6", scratchDB + "resultPlus")
        #outMerge.save(outPath + "map_final")
        outMerge.save(outPath + "map_final2")
        print "Removing small leftovers finished ..."

        # Exporting fields to shp in final output location
        ##arcpy.CopyFeatures_management(scratchDB + "fields_splitted_clean_vector", outPath + "FIELDS_" + landscapes[index])
        arcpy.CopyFeatures_management(scratchDB + "fields_after_processing_final", outPath + "FIELDS2_" + landscapes[index])
        #arcpy.FeatureClassToShapefile_conversion(outPath + "FIELDS_" + landscapes[index], os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))
        arcpy.FeatureClassToShapefile_conversion(outPath + "FIELDS2_" + landscapes[index], os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))
        #dbf_to_csv(fieldsexp)
        print "Exporting fields and thier attribute table finished ..."

        print "Step 7 finished ..."
        nowTime = time.strftime('%X %x')
        print "Sliver polygons elimination done ..." + nowTime

    #===== End chunk: Elimination of sliver polygons =====#

    #===== Chunk: Finalize =====#
        # Reclassify to ALMaSS raster values
        # ALMaSS uses different values for the landcover types, so this step simply translates
        # the numeric values.
        #reclass_land = ReclassByASCIIFile(outPath + "map_final", reclasstable, "DATA")
        #reclass_land.save(outPath + "map_final_reclass")
        reclass_land = ReclassByASCIIFile(outPath + "map_final2", reclasstable, "DATA")
        reclass_land.save(outPath + "map_final_reclass2")
        nowTime = time.strftime('%X %x')
        print "Reclassification done ..." + nowTime

        # clipping back to the original extent
        outExtractByMask = ExtractByMask(reclass_land, os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask"))
        #outExtractByMask.save(outPath + "map_final_reclass_mask")
        outExtractByMask.save(outPath + "map_final_reclass2_mask")

        arcpy.env.extent = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")
        arcpy.env.mask = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")
        arcpy.env.cellSize = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")

        # regionalise map
        regionALM = RegionGroup(outPath + "map_final_reclass_mask","EIGHT","WITHIN","ADD_LINK","")
        regionALM.save(outPath + "Map_ALMaSS")
        nowTime = time.strftime('%X %x')
        print "Regionalisation done ..." + nowTime

        # export attribute table 
        table = outPath + "Map_ALMaSS"
        # Write an attribute tabel - based on this answer:
        # https://geonet.esri.com/thread/83294
        # List the fields
        fields = arcpy.ListFields(table)  
        field_names = [field.name for field in fields]  
          
        with open(attrexp,'wb') as f:  
            w = csv.writer(f)  
            # Write the headers
            w.writerow(field_names)  
            # The search cursor iterates through the 
            for row in arcpy.SearchCursor(table):  
                field_vals = [row.getValue(field.name) for field in fields]  
                w.writerow(field_vals)  
                del row
        print "Attribute table exported..." + nowTime

        ##    # Find soil types
        ##        fields = arcpy.ListFields(outTable)  
        ##        field_names = [field.name for field in fields]
        ##        with open(soilexp,'wb') as s:  
        ##            w = csv.writer(s)  
        ##            # Write the headers
        ##            w.writerow(field_names)  
        ##            for row in arcpy.SearchCursor(outTable):  
        ##                field_vals = [row.getValue(field.name) for field in fields]  
        ##                w.writerow(field_vals)  
        ##                del row
        ##        nowTime = time.strftime('%X %x')
        ##        print "Soiltype table exported..." + nowTime

        # convert regionalised map to ascii
        arcpy.RasterToASCII_conversion(regionALM, asciiexp)
        print "Conversion to ASCII done ..." + nowTime

        # new version for fixing permanent crop staff
        permCropRegions = Con(regionALM, regionALM, 0, "LINK IN (35, 55, 56, 214)")
        perCropsNull = SetNull(permCropRegions, permCropRegions, "VALUE = 0")

        arcpy.PolygonToRaster_conversion(gisDB + "fields_final", "ZAKODOWANY_NR_PRODUCENTA", scratchDB + "fields_owners", "", "", 1)
        outZSaT = ZonalStatisticsAsTable(perCropsNull, "VALUE", scratchDB + "fields_owners", scratchDB + "permanent_crops_zonal", "DATA", "MAJORITY")
        arcpy.TableToTable_conversion(scratchDB + "permanent_crops_zonal", permexp, permtable)


        print "Attribute table of permanent crops exported..." + nowTime

        endTime = time.strftime('%X %x')
        print ""
        print "Landscape generated: " + endTime
    #===== End Chunk: Finalize =====#

    except:
        if arcpy.Exists(outPath + "tmpRaster"):
          arcpy.Delete_management(outPath + "tmpRaster")
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

        arcpy.AddMessage(arcpy.GetMessages(1))
        print arcpy.GetMessages(1)
