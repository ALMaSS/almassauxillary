#=======================================================================================================================
### Name: Landscape generator for Poland, part3
### Purpose: The script creating maps for landscape analysis (calculation of landscape metrics within Fragstats)
### Authors: Elzbieta Ziolkowska - June 2017
### Last large update: September 2019

#===== Chunk: General setup =====#

### Import system modules
import arcpy, traceback, sys, time, gc, shutil, os, csv
#reload(sys)
#sys.setdefaultencoding('utf8')
from dbfpy import dbf
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part3 started: " + nowTime
print "... system modules checked"

### Set fixed paths
dst = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas/"  # Output destination
reclasstableSDI = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas/landscape_metrics/reclass_for_SDI.txt"     # Reclass ascii table

# We need to set these each time we loop through otherwise we recycle the ones from the previous run  
defaultextent = arcpy.env.extent
defaultmask = arcpy.env.mask

### Setting a list with all the landscapes to process
landscapes = ["Proszowice2"]

#===== End chunk: General setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
    print "Processing landscape " + landscapes[index] + " ..."

    #===== Chunk: Landscape-specific setup =====#

    arcpy.env.extent = defaultextent
    arcpy.env.mask = defaultmask

    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    inDB = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
    outLandscapes = os.path.join(dst, "landscape_metrics/landscapes/")
    outSDI = os.path.join(dst, "landscape_metrics/landscapes_for_SDI/")
    outFields = os.path.join(dst, "landscape_metrics/landscapes_for_fields_stats/")
    
    scratchDB = os.path.join(dst, "landscape_metrics/scratch.gdb/")  # scratch folder for tempfiles
    localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")   # project folder with mask

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = inDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"
    print " "

    #===== End chunk: Landscape-specific setup =====#

    try:
        print 'Copying ALMaSS landscape ...'
        arcpy.CopyRaster_management("map_final_reclass", outLandscapes + landscapes[index] + ".tif")
        print 'Reclassifying for SDI ...'
        Reclass = ReclassByASCIIFile(outLandscapes + landscapes[index] + ".tif", reclasstableSDI, "DATA")
        #Reclass.save(scratchDB + landscapes[index] + "_reclass")
        Con = Con(Reclass >= 1000, 6, Reclass)
        Con.save(outSDI + landscapes[index] + "_for_SDI.tif")
        print 'Reclassifying for fields stats ...'
        Con2 = SetNull(Raster(outLandscapes + landscapes[index] + ".tif") < 1000, Raster(outLandscapes + landscapes[index] + ".tif"))
        Con2.save(outFields + landscapes[index] + "_fields.tif")
        arcpy.RasterToPolygon_conversion(outFields + landscapes[index] + "_fields.tif", outFields + landscapes[index] + "_fields.shp", "NO_SIMPLIFY", "VALUE")
        arcpy.AddField_management(outFields + landscapes[index] + "_fields.shp", 'AREA', "DOUBLE")
        arcpy.AddField_management(outFields + landscapes[index] + "_fields.shp", 'PERIM', "DOUBLE")
        
        CursorFieldNames = ["SHAPE@AREA","AREA", "SHAPE@LENGTH", "PERIM"]
        cursor = arcpy.da.UpdateCursor(outFields + landscapes[index] + "_fields.shp",CursorFieldNames)
        for row in cursor:
            row[1] = row[0]
            row[3] = row[2]
            cursor.updateRow(row)
        del row, cursor #Clean up cursor objects
        arcpy.Dissolve_management(outFields + landscapes[index] + "_fields.shp", outFields + landscapes[index] + "_fields_dissolve.shp", ["GRIDCODE"], [["AREA", "SUM"], ["PERIM", "SUM"]])
        arcpy.AddField_management(outFields + landscapes[index] + "_fields_dissolve.shp", 'perm_area', "FLOAT")
        CursorFieldNames2 = ["SUM_AREA", "SUM_PERIM", "perm_area"]
        cursor2 = arcpy.da.UpdateCursor(outFields + landscapes[index] + "_fields_dissolve.shp",CursorFieldNames2)
        for row2 in cursor2:
            row2[2] = row2[1]/row2[0]
            cursor2.updateRow(row2)
        del row2, cursor2 #Clean up cursor objects
        print 'Finished for landscape ' + landscapes[index] + ' ...'

        
    except:
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

        arcpy.AddMessage(arcpy.GetMessages(1))
        print arcpy.GetMessages(1)
