#=======================================================================================================================
### Name: Landscape generator for Poland, part 1
### Purpose: The script convert feature layers to rasters and assemble a raw surface covering land-use map. Further analysis, including cleaning of sliver polygons and creating ALMaSS input landscape map are covered by separate script due to processing issues.
### Authors: original script for Denmark by Flemming Skov & Lars Dalby - Oct-Dec 2014; modified for Poland by Elzbieta Ziolkowska - February 2017
### Last large update: August 2020

#===== Chunk: General setup =====#

### Import system modules
import arcpy, traceback, sys, time, gc, os, shutil, csv, fnmatch
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
arcpy.env.overwriteOutput = True
sr = arcpy.SpatialReference(2180)
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part1 started: " + nowTime
print "... system modules checked"

### Defining all neccesary functions

# Define function to construct WHERE clause:
def buildWhereClause(table, field, value):
  """Constructs a SQL WHERE clause to select rows having the specified value
  within a given field and table."""

  # Add DBMS-specific field delimiters
  fieldDelimited = arcpy.AddFieldDelimiters(table, field)
  # Determine field type
  fieldType = arcpy.ListFields(table, field)[0].type
  # Add single-quotes for string field values
  if str(fieldType) == 'String':
      value = "'%s'" % value
  # Format WHERE clause
  whereClause = "%s = %s" % (fieldDelimited, value)
  return whereClause

### Set paths

# Path to the template directory
template = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas_new/test_area/" 

# Set fixed paths
dst = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas_new/"  # Output destination
nationalDB = "D:/dane_GIS/BDOT_PL/"  # Folder with folders with shapefiles for 'poviats'
pzDB = "D:/dane_GIS/ARiMR_PL/PZ2018/PZ.gdb/"  # Database with PZ (pola zagospodarowanaia)
goDB = "D:/dane_GIS/ARiMR_PL/GO2018/"  # Folder with GO (kataster data / field data)
agriDB = "D:/dane_GIS/ARiMR_PL/agricultural_register/"
poviats = "D:/dane_GIS/PRG_jednostki_administracyjne_v3/powiaty.shp"
woj = "D:/dane_GIS/PRG_jednostki_administracyjne_v3/wojewodztwa.shp"
arcpy.MakeFeatureLayer_management(poviats, "poviats_lyr")
arcpy.MakeFeatureLayer_management(woj, "woj_lyr")

# We need to set these each time we loop through otherwise we recycle the ones fromt the previous run  
defaultextent = arcpy.env.extent
defaultmask = arcpy.env.mask

### Setting a list with all the landscapes to process
landscapes = ["KR_GS", "NG_GS"]

#===== End chunk: General setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
  print "Processing landscape " + landscapes[index] + " ..."

  #===== Chunk: Landscape-specific setup =====#
  
  arcpy.env.extent = defaultextent
  arcpy.env.mask = defaultmask

##  ##  THIS PART MAKES THE FOLDERS NECESSARY - REMEMBER TO UN-COMMENT FIRST TIME
##  dstpath = os.path.join(dst, landscapes[index])
##  shutil.copytree(template, dstpath)
##  gdbpath = os.path.join(dstpath, "test_area.gdb")
##  newgdbpath = os.path.join(dstpath, landscapes[index])
##  os.rename(gdbpath, newgdbpath + ".gdb")
##  gdbpath2 = os.path.join(dstpath, "outputs", "test_area.gdb")
##  newgdbpath2 = os.path.join(dstpath, "outputs", landscapes[index])
##  os.rename(gdbpath2, newgdbpath2 + ".gdb")


  # Data - paths to data, output gdb, scratch folder and simulation landscape mask
  outPath = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
  gisDB = os.path.join(dst, landscapes[index], landscapes[index] + ".gdb/")
  scratchDB = os.path.join(dst, landscapes[index], "outputs/scratch.gdb/")  # scratch folder for shp tempfiles
  scratch = os.path.join(dst, landscapes[index], "outputs/scratch/")  # scratch folder for raster tempfiles

  # Select the landscape and convert to raster
  # Set local variables
  in_features = "D:/ALMaSS_landscapes/Polish_landscapes/landscapes.gdb/study_landscapes_GS"
  out_feature = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")
  fieldname = "nazwa"
  fieldvalue = landscapes[index]
  where_clause = buildWhereClause(in_features, fieldname, fieldvalue)

  # Execute Select
  arcpy.Select_analysis(in_features, out_feature, where_clause)
  arcpy.DefineProjection_management(out_feature, sr)

  out_feature_buffered1km = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask_buffered1km")
  with arcpy.da.SearchCursor(out_feature, ["SHAPE@"]) as pcursor:
     for prow in pcursor:
         polygon=prow[0]
         e=polygon.extent
         print e.XMin,e.YMin,e.XMax,e.YMax
         # A list of features and coordinate pairs of the extended mask
         feature_info = [[[e.XMin - 1000, e.YMax + 1000], [e.XMax + 1000, e.YMax + 1000], [e.XMax + 1000, e.YMin - 1000], [e.XMin - 1000, e.YMin - 1000]]]
         # A list that will hold each of the Polygon objects
         features = []
         # Create Polygon objects based an the array of points
         for feature in feature_info:
           array = arcpy.Array([arcpy.Point(*coords) for coords in feature])
           # Add the first coordinate pair to the end to close polygon
           array.append(array[0])
           features.append(arcpy.Polygon(array))
         # Persist a copy of the geometry objects using CopyFeatures
         arcpy.CopyFeatures_management(features, out_feature_buffered1km)
         
  mask = os.path.join(dst, landscapes[index], "outputs/project.gdb/mask")
  mask_buffered1km = os.path.join(dst, landscapes[index], "outputs/project.gdb/mask_buffered1km")
  arcpy.PolygonToRaster_conversion(out_feature, "OBJECTID", mask, "CELL_CENTER", "NONE", "1")
  arcpy.PolygonToRaster_conversion(out_feature_buffered1km, "OBJECTID", mask_buffered1km, "CELL_CENTER", "NONE", "1")

  #localSettings = out_feature   # project folder with mask
  localSettings = out_feature_buffered1km

  # Model settings
  arcpy.env.overwriteOutput = True
  arcpy.env.workspace = gisDB
  arcpy.env.scratchWorkspace = scratch
  arcpy.env.extent = localSettings
  arcpy.env.mask = localSettings
  arcpy.env.cellSize = localSettings
  print "... model settings read"

  # Model execution - controls which processes to run:
  default = 1  # 1 -> run process; 0 -> do not run process

  # Mosaic
  road = default      #create road theme
  builtup = default #create built up theme
  nature = default       #create nature theme
  water = default   #create fresh water theme
  cultural = default      #create cultural feature theme
  cultivable = default #create cultivable land theme
  finalmap = default      #assemble final map

  print " "
  #===== End chunk: Landscape-specific setup =====#

#####################################################################################################

  try:
    
  #===== Chunk: Clipping =====#
  # from national data to a given study area

    print "Copying files to process to gisDB ..."
    print "Processing BDOT10k data ..."

    # Select the proper 'poviats' to copy to gisDB
    print "Selecting poviats intersecting the study area ..."
    arcpy.SelectLayerByLocation_management('poviats_lyr', 'intersect', out_feature_buffered1km)
    arcpy.CopyFeatures_management('poviats_lyr', scratchDB + 'poviats_selected')
    poviatIDList = []
    cursor_pov = arcpy.SearchCursor(scratchDB + 'poviats_selected')
    for row_pov in cursor_pov:
      poviatID = row_pov.getValue('jpt_kod_je')
      poviatIDList.append(poviatID)

    # Copying files to gisDB
    for poviatID in poviatIDList:
      print "Processing poviat " + poviatID
      for root, dirnames, filenames in os.walk(nationalDB):
        for filename in fnmatch.filter(filenames, '*' + poviatID[-4:] + '*.shp'):
          arcpy.FeatureClassToGeodatabase_conversion(os.path.join(root, filename), gisDB)

    # Listing all layers from BDOT10k to process
    print "Clipping layers to the landscape extent ..."
    featureclasses = arcpy.ListFeatureClasses()

    layerList=[]
    for feature in featureclasses:
      layer = feature[-6:]
      print layer
      if layer in layerList:
        print layer + " already listed"
        print ""
      else:
        print layer + " added to list"
        layerList.append(layer)

    for layer in layerList:
    # For each layer creating a list of files to merge
      fileList=[]
      for item in featureclasses:
        if layer in item:
          fileList.append(item)
      arcpy.Merge_management(fileList, scratchDB + layer)
      arcpy.RepairGeometry_management(scratchDB + layer)


      # Clipping to test area
      arcpy.Clip_analysis(scratchDB + layer, out_feature_buffered1km, gisDB + layer)

      # Defining projection
      arcpy.DefineProjection_management(gisDB + layer, sr)
      #print "Layer " + layer + " processed ..."
    print "End of BDOT10k data processing ..."

    # Clip PZ (pola zagospodarowania) layer (from ARiMR)
    arcpy.Clip_analysis(pzDB + "PZ", out_feature_buffered1km, gisDB + "PZ")

    # Processing cadastral data (from ARiMR)
    print "Processing cadastral data ..."
    arcpy.Select_analysis("PTTR_A", scratchDB + "arable_land", "x_kod = 'PTTR02'")
    arcpy.PolygonToRaster_conversion(scratchDB + "arable_land", "OBJECTID", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
    outCon = Con(IsNull(Raster(scratchDB + "tmpRaster")), 0, 1)

    # Select the proper 'woj' [assumption: study area located within 1 woj]
    print "Selecting woj intersecting the study area ..."
    arcpy.SelectLayerByLocation_management('woj_lyr', 'intersect', out_feature_buffered1km)
    arcpy.CopyFeatures_management('woj_lyr', scratchDB + 'woj_selected')
    wojIDList = []
    cursor_woj = arcpy.SearchCursor(scratchDB + 'woj_selected')
    for row_woj in cursor_woj:
      wojID = row_woj.getValue('jpt_kod_je')
      wojIDList.append(wojID)
      
    # Copying cadastral files to gisDB
    fileList_GO = []
    fileList_Fields =[]
    for wojID in wojIDList:
      if len(str(wojID)) <2:
        IDfull = "0" + str(wojID)
      else:
        IDfull = str(wojID)
      print "Processing woj " + IDfull
      
      for root, dirnames, filenames in os.walk(goDB):
        for filename in fnmatch.filter(filenames, '*' + IDfull + '.shp'):
          print filename
          arcpy.FeatureClassToGeodatabase_conversion(os.path.join(root, filename), gisDB)
          arcpy.Clip_analysis(gisDB + filename[:-4], out_feature_buffered1km, gisDB + filename[:-4] + '_clip')
          fileList_GO.append(gisDB + filename[:-4] + '_clip')

          print "Adding info from agricultural register on crops ..."
          arcpy.CopyRows_management(agriDB + "agri_register2018_woj" + IDfull + "_eng.csv", scratchDB + "agri_register2018_woj" + IDfull)
          arcpy.AddField_management(scratchDB + "agri_register2018_woj" + IDfull, 'dzialka_agri', "TEXT", "", "", 30)
          cursor_agri_table = arcpy.UpdateCursor(scratchDB + "agri_register2018_woj" + IDfull)    
          for row in cursor_agri_table:
            parcel_no = row.getValue('IDENT_DZIALKI_EWIDENCYJNEJ')
            #print parcel_no
            row.setValue('dzialka_agri', parcel_no)
            cursor_agri_table.updateRow(row)         
          arcpy.JoinField_management(gisDB + filename[:-4] + '_clip', "dzialka", scratchDB + "agri_register2018_woj" + IDfull, "dzialka_agri", "")
          
          print "Adding info on farm owners ..."
          arcpy.CopyRows_management(agriDB + "owners_2018_woj" + IDfull + ".txt", scratchDB + "owners_2018_woj" + IDfull)
          arcpy.AddField_management(scratchDB + "owners_2018_woj" + IDfull, 'dzialka_owner', "TEXT", "", "", 30)
          cursor_owner_table = arcpy.UpdateCursor(scratchDB + "owners_2018_woj" + IDfull)    
          for row in cursor_owner_table:
            parcel_no = row.getValue('IDENT_DZIALKI_EWIDENCYJNEJ')
            row.setValue('dzialka_owner', parcel_no)
            cursor_owner_table.updateRow(row)          
          arcpy.JoinField_management(gisDB + filename[:-4] + '_clip', "dzialka", scratchDB + "owners_2018_woj" + IDfull, "dzialka_owner", "")
          
          print "Selecting agricultural parcels according to agricultural register ..."
          arcpy.MakeFeatureLayer_management(gisDB + filename[:-4] + '_clip', filename[:-4] + '_clip_lyr')
          arcpy.SelectLayerByAttribute_management(filename[:-4] + '_clip_lyr', "NEW_SELECTION", "dzialka_agri IS NOT NULL")
          arcpy.CopyFeatures_management(filename[:-4] + '_clip_lyr', scratchDB + "cadastral_data_" + IDfull)
          fileList_Fields.append(scratchDB + "cadastral_data_" + IDfull)
      
          print "Selecting rest of parcels for further checking ..."
          arcpy.SelectLayerByAttribute_management(filename[:-4] + '_clip_lyr', "NEW_SELECTION", "dzialka_agri IS NULL")
          arcpy.CopyFeatures_management(filename[:-4] + '_clip_lyr', scratchDB + "cadastral_data_" + IDfull + "to_check")

          if int(arcpy.GetCount_management(scratchDB + "cadastral_data_" + IDfull + "to_check").getOutput(0))>0:
            print "Selecting all parcels with agricultural land use >= 80% ..."
            ZonalStatisticsAsTable(scratchDB + "cadastral_data_" + IDfull + "to_check", "dzialka", outCon, scratchDB + "cadastral_data_" + IDfull + "to_check_table", "NODATA", "ALL")
            arcpy.JoinField_management (scratchDB + "cadastral_data_" + IDfull + "to_check", "dzialka", scratchDB + "cadastral_data_" + IDfull + "to_check_table", "dzialka", ["MEAN"])
            arcpy.MakeFeatureLayer_management(scratchDB + "cadastral_data_" + IDfull + "to_check", "cadastral_data_" + IDfull + "to_check_lyr")
            arcpy.SelectLayerByAttribute_management("cadastral_data_" + IDfull + "to_check_lyr", "NEW_SELECTION", "MEAN >= 0.8")
            arcpy.CopyFeatures_management("cadastral_data_" + IDfull + "to_check_lyr", scratchDB + "cadastral_data_" + IDfull + "to_check_selected")
            print "Removing elongated and too small parcels ..."
            arcpy.MinimumBoundingGeometry_management(scratchDB + "cadastral_data_" + IDfull + "to_check_selected", scratchDB + "cadastral_data_" + IDfull + "to_check_selected_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
            arcpy.JoinField_management(scratchDB + "cadastral_data_" + IDfull + "to_check_selected", "dzialka", scratchDB + "cadastral_data_" + IDfull + "to_check_selected_bounding", "dzialka", "")
            arcpy.MakeFeatureLayer_management(scratchDB + "cadastral_data_" + IDfull + "to_check_selected", "cadastral_data_" + IDfull + "to_check_selected_lyr")
            arcpy.SelectLayerByAttribute_management("cadastral_data_" + IDfull + "to_check_selected_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 > 0.05 AND Shape_Area > 1000")
            arcpy.CopyFeatures_management("cadastral_data_" + IDfull + "to_check_selected_lyr", scratchDB + "cadastral_data_" + IDfull + "to_add")
            fileList_Fields.append(scratchDB + "cadastral_data_" + IDfull + "to_add")
             
    #print fileList_GO
    #print fileList_Fields

    print "Merging files to cover the whole study area ..."
    arcpy.Merge_management(fileList_GO, gisDB + 'cadastral_data')
    arcpy.Merge_management(fileList_Fields, gisDB + 'fields')

    print "Adding new fields and updating info ..."
    arcpy.AddField_management(gisDB + 'fields', 'gridcode', "SHORT")
    arcpy.AddField_management(gisDB + 'fields', 'FarmType', "SHORT")
    arcpy.AddField_management(gisDB + 'fields', 'ALMaSScode', "SHORT")
    cursor = arcpy.UpdateCursor(gisDB + 'fields')
    j=1000
    for row in cursor:
      if row.getValue('ZAKODOWANY_NR_PRODUCENTA') == None:
        row.setValue('ZAKODOWANY_NR_PRODUCENTA', 1)
      row.setValue('FarmType', 32)  #arable farm for now assigned to all farms
      row.setValue('ALMaSScode', 20)
      row.setValue('gridcode', j)   # setting gridcode for a new field
      j=j+1
      cursor.updateRow(row)

    arcpy.MakeFeatureLayer_management(gisDB + 'fields', 'fields_lyr', "", "", "OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;dzialka IDENT_DZIALKI_EWIDENCYJNEJ VISIBLE NONE;powierzchn powierchnia VISIBLE NONE;powiat powiat VISIBLE NONE;gmina gmina VISIBLE NONE;IDENT_DZIALKI_EWIDENCYJNEJ IDENT_DZIALKI_EWIDENCYJNEJ HIDDEN NONE;NAZWA_WOJEWODZTWA NAZWA_WOJEWODZTWA HIDDEN NONE;NAZWA_POWIATU NAZWA_POWIATU HIDDEN NONE;NAZWA_GMINY NAZWA_GMINY HIDDEN NONE;POWIERZCHNIA POWIERZCHNIA HIDDEN NONE;OPIS_UPRAWA OPIS_UPRAWA VISIBLE NONE;OPIS_GR_UPRAWY OPIS_GR_UPRAWY VISIBLE NONE;IDENT_DZIALKI_EWIDENCYJNEJ_1 IDENT_DZIALKI_EWIDENCYJNEJ_1 HIDDEN NONE;ZAKODOWANY_NR_PRODUCENTA ZAKODOWANY_NR_PRODUCENTA VISIBLE NONE;LP_W_RAMACH_NR_PRODUCENTA LP_W_RAMACH_NR_PRODUCENTA HIDDEN NONE;MEAN MEAN HIDDEN NONE;dzialka_1 dzialka_1 HIDDEN NONE;powierzchn_1 powierzchn_1 HIDDEN NONE;powiat_1 powiat_1 HIDDEN NONE;gmina_1 gmina_1 HIDDEN NONE;IDENT_DZIALKI_EWIDENCYJNEJ_12 IDENT_DZIALKI_EWIDENCYJNEJ_12 HIDDEN NONE;NAZWA_WOJEWODZTWA_1 NAZWA_WOJEWODZTWA_1 HIDDEN NONE;NAZWA_POWIATU_1 NAZWA_POWIATU_1 HIDDEN NONE;NAZWA_GMINY_1 NAZWA_GMINY_1 HIDDEN NONE;POWIERZCHNIA_1 POWIERZCHNIA_1 HIDDEN NONE;OPIS_UPRAWA_1 OPIS_UPRAWA_1 HIDDEN NONE;OPIS_GR_UPRAWY_1 OPIS_GR_UPRAWY_1 HIDDEN NONE;IDENT_DZIALKI_EWIDENCYJNEJ_12_13 IDENT_DZIALKI_EWIDENCYJNEJ_12_13 HIDDEN NONE;ZAKODOWANY_NR_PRODUCENTA_1 ZAKODOWANY_NR_PRODUCENTA_1 HIDDEN NONE;LP_W_RAMACH_NR_PRODUCENTA_1 LP_W_RAMACH_NR_PRODUCENTA_1 HIDDEN NONE;MEAN_1 MEAN_1 HIDDEN NONE;ORIG_FID ORIG_FID HIDDEN NONE;MBG_Diameter MBG_Diameter HIDDEN NONE;Shape_Length_1 Shape_Length_1 HIDDEN NONE;Shape_Area_1 Shape_Area_1 HIDDEN NONE;numer numer HIDDEN NONE;numer_1 numer_1 HIDDEN NONE;Shape_Length Shape_Length HIDDEN NONE;Shape_Area Shape_Area HIDDEN NONE;gridcode gridcode VISIBLE NONE;FarmType FarmType VISIBLE NONE;ALMaSScode ALMaSScode VISIBLE NONE")
    arcpy.CopyFeatures_management('fields_lyr', gisDB + 'fields_final')
             
    # converting to raster
    print "Converting to raster ..."
    arcpy.PolygonToRaster_conversion(gisDB + 'fields_final', "gridcode", scratchDB + "fields_final", "", "", 1)
    Reclass = Con(IsNull(scratchDB + "fields_final"), 1, scratchDB + 'fields_final')
    Reclass.save(outPath + "fields_final")
    print "End of cadastral data processing ..."

  #===== End chunk: Clipping =====#

  #===== Chunk: Conversion =====#
  # from feature layers to raster

    print "Conversion chunk started ..."

    # Conversion  - features to raster layers
    # Check which layers exists in gisDB
    layersToProcess = ["PZ", "SWRM_L", "SWKN_L", "SWRS_L", "PTWP_A", "OIMK_A", "SKJZ_L", "PTKM_A", "SKTR_L", "BUWT_P", "OIPR_L", "OIPR_P", "KUSK_A", "PTUT_A", "BUZM_L", "OIOR_A", "BUBD_A", "BUSP_A", "KUSC_A", "KUMN_A", "PTSO_A", "PTPL_A", "PTZB_A", "PTLZ_A", "PTRK_A", "PTTR_A", "PTGN_A", "PTNZ_A"]
    for layer in layersToProcess:
      if arcpy.Exists(gisDB + layer):
        if int(arcpy.GetCount_management(gisDB + layer).getOutput(0))>0:
          exec("%s = %d" % (layer,1))
        else:
          exec("%s = %d" % (layer,0))
      else:
        print "layer does not exists in gisDB"
        exec("%s = %d" % (layer,0))

    landsea = default
         
    # land
    if landsea == 1:
      print "Processing base map (land/sea) ..."
      if arcpy.Exists(outPath + "land"):
        arcpy.Delete_management(outPath + "land")
        print "... deleting existing raster"
      #arcpy.PolygonToRaster_conversion(localSettings, "OBJECTID", outPath + "land", "CELL_CENTER", "", 1)
      rasTemp = Con(Raster(mask_buffered1km) >= 0, 1, 0)
      rasTemp.save(outPath + "land")

    ## THEME WATER (1)

    rasterList_water = []

    # 11 - drainage ditch (SWRM_L)
    if SWRM_L == 1:
      print "Processing drainage ditch ..."
    ##
      arcpy.Select_analysis("SWRM_L", scratchDB + "SWRM_ss", """"szerokosc" <= 1""")
      if int(arcpy.GetCount_management(scratchDB + "SWRM_ss").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRM111"):
          arcpy.Delete_management(outPath + "SWRM111")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRM_ss","","1","")
        rasTemp = Con(eucDistTemp < 0.5, 111, 1)
        rasTemp.save(outPath + "SWRM111")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRM111", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRM111")

      arcpy.Select_analysis("SWRM_L", scratchDB + "SWRM_s", """"szerokosc" > 1 AND "szerokosc" <= 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWRM_s").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRM112"):
          arcpy.Delete_management(outPath + "SWRM112")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRM_s","","1","")
        rasTemp = Con(eucDistTemp < 1.01, 112, 1)
        rasTemp.save(outPath + "SWRM112")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRM112", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRM112")

      arcpy.Select_analysis("SWRM_L", scratchDB + "SWRM_m", """"szerokosc" > 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWRM_m").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRM113"):
          arcpy.Delete_management(outPath + "SWRM113")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRM_m","","1","")
        rasTemp = Con(eucDistTemp < 2, 113, 1)
        rasTemp.save(outPath + "SWRM113")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRM113", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRM113")

    # 12 - channels (SWKN_L)
    if SWKN_L == 1:
      print "Processing channels ..."

      arcpy.Select_analysis("SWKN_L", scratchDB + "SWKN_ss", """"szerokosc" <= 1""")
      if int(arcpy.GetCount_management(scratchDB + "SWKN_ss").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWKN121"):
          arcpy.Delete_management(outPath + "SWKN121")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWKN_ss","","1","")
        rasTemp = Con(eucDistTemp < 0.5, 121, 1)
        rasTemp.save(outPath + "SWKN121")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWKN121", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWKN121")

      arcpy.Select_analysis("SWKN_L", scratchDB + "SWKN_s", """"szerokosc" > 1 AND "szerokosc" <= 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWKN_s").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWKN122"):
          arcpy.Delete_management(outPath + "SWKN122")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWKN_s","","1","")
        rasTemp = Con(eucDistTemp < 1.01, 122, 1)
        rasTemp.save(outPath + "SWKN122")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWKN122", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWKN122")

      arcpy.Select_analysis("SWKN_L", scratchDB + "SWKN_m", """"szerokosc" > 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWKN_m").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWKN123"):
          arcpy.Delete_management(outPath + "SWKN123")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWKN_m","","1","")
        rasTemp = Con(eucDistTemp < 2, 123, 1)
        rasTemp.save(outPath + "SWKN123")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWKN123", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWKN123")
      
    # 13 - rivers (SWRS_L)
    if SWRS_L == 1:
      print "Processing rivers ..."
      arcpy.MakeFeatureLayer_management("SWRS_L", "SWRS_layer", "", "", "")
      arcpy.Select_analysis("PTWP_A", scratchDB + "PTWP02", "x_kod = 'PTWP02'")
      if int(arcpy.GetCount_management(scratchDB + "PTWP02").getOutput(0))>0:
        arcpy.MakeFeatureLayer_management(scratchDB + "PTWP02", "PTWP02_layer", "", "", "")
        arcpy.SelectLayerByLocation_management("SWRS_layer", "WITHIN", "PTWP02_layer", "", "NEW_SELECTION", "INVERT")
        arcpy.CopyFeatures_management("SWRS_layer", scratchDB + "SWRS_selection")
      else:
        arcpy.CopyFeatures_management("SWRS_layer", scratchDB + "SWRS_selection")

      arcpy.Select_analysis(scratchDB + "SWRS_selection",scratchDB +  "SWRS_ss", """"szerokosc" <= 1""")
      if int(arcpy.GetCount_management(scratchDB + "SWRS_ss").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRS131"):
          arcpy.Delete_management(outPath + "SWRS131")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRS_ss","","1","")
        rasTemp = Con(eucDistTemp < 0.5, 131, 1)
        rasTemp.save(outPath + "SWRS131")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRS131", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRS131")

      arcpy.Select_analysis(scratchDB + "SWRS_selection", scratchDB + "SWRS_s", """"szerokosc" > 1 AND "szerokosc" <= 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWRS_s").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRS132"):
          arcpy.Delete_management(outPath + "SWRS132")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRS_s","","1","")
        rasTemp = Con(eucDistTemp < 1.01, 132, 1)
        rasTemp.save(outPath + "SWRS132")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRS132", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRS132")

      arcpy.Select_analysis(scratchDB + "SWRS_selection", scratchDB + "SWRS_m", """"szerokosc" > 2""")
      if int(arcpy.GetCount_management(scratchDB + "SWRS_m").getOutput(0))>0:
        if arcpy.Exists(outPath + "SWRS133"):
          arcpy.Delete_management(outPath + "SWRS133")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SWRS_m","","1","")
        rasTemp = Con(eucDistTemp < 2, 133, 1)
        rasTemp.save(outPath + "SWRS133")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SWRS133", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "SWRS133")

    # 14 - rivers and lakes from PT (PTWP_A)
    if PTWP_A == 1:
      print "Processing rivers and lakes from PT ..."

      print "Rivers from PT ..."
      arcpy.Select_analysis("PTWP_A", scratchDB + "PTWP02", "x_kod = 'PTWP02'")
      if int(arcpy.GetCount_management(scratchDB + "PTWP02").getOutput(0))>0:
        if arcpy.Exists(outPath + "PTWP141"):
          arcpy.Delete_management(outPath + "PTWP141")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTWP02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 141)
        rasTemp.save(outPath + "PTWP141")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTWP141", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "PTWP141")

      print "Lakes from PT ..."
      arcpy.Select_analysis("PTWP_A", scratchDB + "PTWP03", "x_kod = 'PTWP03'")
      if int(arcpy.GetCount_management(scratchDB + "PTWP03").getOutput(0))>0:
        if arcpy.Exists(outPath + "PTWP143"):
          arcpy.Delete_management(outPath + "PTWP143")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTWP03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 143)
        rasTemp.save(outPath + "PTWP143")
        arcpy.Delete_management(outPath + "tmpRaster")

        if arcpy.Exists(outPath + "PTWP_verge110"):
          arcpy.Delete_management(outPath + "PTWP_verge110")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "PTWP03","","1","")
        rasTemp = Con((eucDistTemp < 2.05) & (eucDistTemp > 0), 110, 1)
        rasTemp.save(outPath + "PTWP_verge110") 
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTWP143", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTWP_verge110", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "PTWP143")
      rasterList_water.append(outPath + "PTWP_verge110")

    # 15 - hydrologic structures (BUZM03)
    if BUZM_L == 1:
      print "Processing embankments/dikes ..."

      arcpy.Select_analysis("BUZM_L", scratchDB + "BUZM03_class0", "x_kod = 'BUZM03' AND szerPodsta< 4")
      if int(arcpy.GetCount_management(scratchDB + "BUZM03_class0").getOutput(0))>0:
        if arcpy.Exists(outPath + "BUZM151"):
          arcpy.Delete_management(outPath + "BUZM151")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUZM03_class0","","1","")
        rasTemp = Con(eucDistTemp < 1.5, 151, 1)
        rasTemp.save(outPath + "BUZM151")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUZM151", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "BUZM151")

      arcpy.Select_analysis("BUZM_L", scratchDB + "BUZM03_class1", "x_kod = 'BUZM03' AND (szerPodsta >= 4 AND szerPodsta < 6)")
      if int(arcpy.GetCount_management(scratchDB + "BUZM03_class1").getOutput(0))>0:
        if arcpy.Exists(outPath + "BUZM152"):
          arcpy.Delete_management(outPath + "BUZM152")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUZM03_class1","","1","")
        rasTemp = Con(eucDistTemp < 2.5, 152, 1)
        rasTemp.save(outPath + "BUZM152")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUZM152", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "BUZM152")

      arcpy.Select_analysis("BUZM_L", scratchDB + "BUZM03_class2", "x_kod = 'BUZM03' AND (szerPodsta >= 6 AND szerPodsta < 10)")
      if int(arcpy.GetCount_management(scratchDB + "BUZM03_class2").getOutput(0))>0:
        if arcpy.Exists(outPath + "BUZM153"):
          arcpy.Delete_management(outPath + "BUZM153")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUZM03_class2","","1","")
        rasTemp = Con(eucDistTemp < 4.25, 153, 1)
        rasTemp.save(outPath + "BUZM153")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUZM153", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "BUZM153")

      arcpy.Select_analysis("BUZM_L", scratchDB + "BUZM03_class3", "x_kod = 'BUZM03' AND (szerPodsta >= 10 AND szerPodsta < 15)")
      if int(arcpy.GetCount_management(scratchDB + "BUZM03_class3").getOutput(0))>0:
        if arcpy.Exists(outPath + "BUZM154"):
          arcpy.Delete_management(outPath + "BUZM154")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUZM03_class3","","1","")
        rasTemp = Con(eucDistTemp < 7, 154, 1)
        rasTemp.save(outPath + "BUZM154")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUZM154", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "BUZM154")

      arcpy.Select_analysis("BUZM_L", scratchDB + "BUZM03_class4", "x_kod = 'BUZM03' AND szerPodsta > 15")
      if int(arcpy.GetCount_management(scratchDB + "BUZM03_class4").getOutput(0))>0:
        if arcpy.Exists(outPath + "BUZM155"):
          arcpy.Delete_management(outPath + "BUZM155")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUZM03_class4","","1","")
        rasTemp = Con(eucDistTemp < 10, 155, 1)
        rasTemp.save(outPath + "BUZM155")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUZM155", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_water.append(outPath + "BUZM155")

  ## THEME ROADS (2)

    rasterList_road = []

    # 21 - area under roads and railways (PTKM_A)(eucDistTemp < 2.25) & (eucDistTemp >= 1.75)
    if PTKM_A == 1:
      print "Processing area under roads and railways  ..."

      print "Under roads ..."
      arcpy.Select_analysis("PTKM_A", scratchDB + "PTKM01", "x_kod = 'PTKM01'")
      if int(arcpy.GetCount_management(scratchDB + "PTKM01").getOutput(0))>0:                          
        if arcpy.Exists(outPath + "PTKM211"):
          arcpy.Delete_management(outPath + "PTKM211")
          print "... deleting existing raster"    
        arcpy.PolygonToRaster_conversion(scratchDB + "PTKM01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 211)
        rasTemp.save(outPath + "PTKM211")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTKM211", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "PTKM211")

      print "Under railway ..."
      arcpy.Select_analysis("PTKM_A", scratchDB + "PTKM02", "x_kod = 'PTKM02'")
      if int(arcpy.GetCount_management(scratchDB + "PTKM02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTKM212"):
          arcpy.Delete_management(outPath + "PTKM212")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTKM02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 212)
        rasTemp.save(outPath + "PTKM212")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTKM212", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "PTKM212")

      print "Under roads and railway ..."
      arcpy.Select_analysis("PTKM_A", scratchDB + "PTKM03", "x_kod = 'PTKM03'")
      if int(arcpy.GetCount_management(scratchDB + "PTKM03").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTKM213"):
          arcpy.Delete_management(outPath + "PTKM213")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTKM03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 213)
        rasTemp.save(outPath + "PTKM213")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTKM213", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "PTKM213")

      # defining exclusion area for verges
      tmpRaster = CellStatistics([outPath + "PTKM211", outPath + "PTKM213"], "MAXIMUM", "DATA")
      rasTemp = Con(tmpRaster==1, 0, 1)
      rasTemp.save(scratchDB + "exc_verges")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      # defining exclusion area for verges
      rasTemp = Con(Raster(outPath + "land")==1, 0, 1)
      rasTemp.save(scratchDB + "exc_verges")

    # 220 - railway tracks (SKTR_L)
    if SKTR_L == 1:
      print "Processing railway tracks ..."
      arcpy.Select_analysis("SKTR_L", scratchDB + "SKTR01", "x_kod = 'SKTR01'")
      if int(arcpy.GetCount_management(scratchDB + "SKTR01").getOutput(0))>0:
        if arcpy.Exists(outPath + "SKTR220"):
          arcpy.Delete_management(outPath + "SKTR220")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "SKTR01", "", "1", "")
        rasTemp = Con(eucDistTemp < 4.5, 220, 1)
        rasTemp.save(outPath + "SKTR220")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKTR220", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKTR220")
      
    # 24 - roads (SKJZ_L)
    # 23 - road verges (SKJZ_L)
    if SKJZ_L == 1:
      print "Processing roads and road verges ..."

      print "Dirt roads ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ08", "x_kod = 'SKJZ08'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ08").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ08","","1","")
        if arcpy.Exists(outPath + "SKJZ241"):
          arcpy.Delete_management(outPath + "SKJZ241")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 1.75, 241, 1)
        rasTemp.save(outPath + "SKJZ241")
        
        if arcpy.Exists(outPath + "SKJZ_verge231"):
          arcpy.Delete_management(outPath + "SKJZ_verge231")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 2.25) & (eucDistTemp >= 1.75), 231, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge231")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge231", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ241", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ241")
      rasterList_road.append(outPath + "SKJZ_verge231")

      print "Driveways ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ07", "x_kod = 'SKJZ07'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ07").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ07","","1","")
        if arcpy.Exists(outPath + "SKJZ242"):
          arcpy.Delete_management(outPath + "SKJZ242")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 1.75, 242, 1)
        rasTemp.save(outPath + "SKJZ242")
        
        if arcpy.Exists(outPath + "SKJZ_verge232"):
          arcpy.Delete_management(outPath + "SKJZ_verge232")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 2.25) & (eucDistTemp >= 1.75), 232, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge232")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge232", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ242", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ242")
      rasterList_road.append(outPath + "SKJZ_verge232")

      print "Local roads ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ06", "x_kod = 'SKJZ06'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ06").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ06","","1","")
        if arcpy.Exists(outPath + "SKJZ243"):
          arcpy.Delete_management(outPath + "SKJZ243")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 2.0, 243, 1)
        rasTemp.save(outPath + "SKJZ243")
        
        if arcpy.Exists(outPath + "SKJZ_verge233"):
          arcpy.Delete_management(outPath + "SKJZ_verge233")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 3.0) & (eucDistTemp >= 2.0), 233, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge233")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge233", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ243", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ243")
      rasterList_road.append(outPath + "SKJZ_verge233")

      print "Collective roads ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ05", "x_kod = 'SKJZ05'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ05").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ05","","1","")
        if arcpy.Exists(outPath + "SKJZ244"):
          arcpy.Delete_management(outPath + "SKJZ244")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 2.75, 244, 1)
        rasTemp.save(outPath + "SKJZ244")
        
        if arcpy.Exists(outPath + "SKJZ_verge234"):
          arcpy.Delete_management(outPath + "SKJZ_verge234")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 3.75) & (eucDistTemp >= 2.75), 234, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge234")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge234", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ244", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ244")
      rasterList_road.append(outPath + "SKJZ_verge234")

      print "Main roads ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ04", "x_kod = 'SKJZ04'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ04").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ04","","1","")
        if arcpy.Exists(outPath + "SKJZ245"):
          arcpy.Delete_management(outPath + "SKJZ245")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 3.0, 245, 1)
        rasTemp.save(outPath + "SKJZ245")
        
        if arcpy.Exists(outPath + "SKJZ_verge235"):
          arcpy.Delete_management(outPath + "SKJZ_verge235")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 4) & (eucDistTemp >= 3.0), 235, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge235")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge235", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ245", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ245")
      rasterList_road.append(outPath + "SKJZ_verge235")

      print "Express roads ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ02_03", "x_kod = 'SKJZ02' OR x_kod = 'SKJZ03'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ02_03").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ02_03","","1","")
        if arcpy.Exists(outPath + "SKJZ246"):
          arcpy.Delete_management(outPath + "SKJZ246")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 3.5, 246, 1)
        rasTemp.save(outPath + "SKJZ246")
        
        if arcpy.Exists(outPath + "SKJZ_verge236"):
          arcpy.Delete_management(outPath + "SKJZ_verge236")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 4.75) & (eucDistTemp >= 3.5), 236, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge236")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge236", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ246", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ246")
      rasterList_road.append(outPath + "SKJZ_verge236")
        
      print "Highways ..."
      arcpy.Select_analysis("SKJZ_L", scratchDB + "SKJZ01", "x_kod = 'SKJZ01'")
      if int(arcpy.GetCount_management(scratchDB + "SKJZ01").getOutput(0))>0:
        eucDistTemp = EucDistance(scratchDB + "SKJZ01","","1","")
        if arcpy.Exists(outPath + "SKJZ247"):
          arcpy.Delete_management(outPath + "SKJZ247")
          print "... deleting existing raster"
        rasTemp = Con(eucDistTemp < 3.75, 247, 1)
        rasTemp.save(outPath + "SKJZ247")
        
        if arcpy.Exists(outPath + "SKJZ_verge237"):
          arcpy.Delete_management(outPath + "SKJZ_verge237")
          print "... deleting existing raster"
        tmpRaster = Con((eucDistTemp < 6.25) & (eucDistTemp >= 3.75), 237, 1)
        Clip = Con(Raster(scratchDB + "exc_verges") == 1, 1, tmpRaster)
        Clip.save(outPath + "SKJZ_verge237")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ_verge237", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
        arcpy.CopyRaster_management(outPath + "land", outPath + "SKJZ247", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "SKJZ247")
      rasterList_road.append(outPath + "SKJZ_verge237")

    # 25 - high technical buildings (BUWT_P)
    if BUWT_P == 1:
      print "Processing high technical buildings ..."

      print "Pylons ..."
      arcpy.Select_analysis("BUWT_P", scratchDB + "BUWT06", "x_kod = 'BUWT06'")
      if int(arcpy.GetCount_management(scratchDB + "BUWT06").getOutput(0))>0:                           
        if arcpy.Exists(outPath + "BUWT251"):
          arcpy.Delete_management(outPath + "BUWT251")
          print "... deleting existing raster"    
        eucDistTemp = EucDistance(scratchDB + "BUWT06", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.5, 251, 1)
        rasTemp.save(outPath + "BUWT251")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUWT251", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "BUWT251")

      print "Transmission towers ..."
      arcpy.Select_analysis("BUWT_P", scratchDB + "BUWT04", "x_kod = 'BUWT04'")
      if int(arcpy.GetCount_management(scratchDB + "BUWT04").getOutput(0))>0:  
        if arcpy.Exists(outPath + "BUWT252"):
          arcpy.Delete_management(outPath + "BUWT252")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUWT04", "", "1", "")
        rasTemp = Con(eucDistTemp < 2.5, 252, 1)
        rasTemp.save(outPath + "BUWT252")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUWT252", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "BUWT252")

      print "Wind turbines ..."
      arcpy.Select_analysis("BUWT_P", scratchDB + "BUWT05", "x_kod = 'BUWT05'")
      if int(arcpy.GetCount_management(scratchDB + "BUWT05").getOutput(0))>0:  
        if arcpy.Exists(outPath + "BUWT253"):
          arcpy.Delete_management(outPath + "BUWT253")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUWT05", "", "1", "")
        rasTemp = Con(eucDistTemp < 1.5, 253, 1)
        rasTemp.save(outPath + "BUWT253")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUWT253", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_road.append(outPath + "BUWT253")

  ## THEME CULTURAL (3)

    rasterList_cultural = []

    # 310 - gardens (PTUT01)
    if PTUT_A == 1:
      print "Processing gardens ..."

      arcpy.Select_analysis("PTUT_A", scratchDB + "PTUT01", "x_kod = 'PTUT01'")
      if int(arcpy.GetCount_management(scratchDB + "PTUT01").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTUT310"):
          arcpy.Delete_management(outPath + "PTUT310")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTUT01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 310)
        rasTemp.save(outPath + "PTUT310")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTUT310", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "PTUT310")

    # 320 - parks (KUSK04)
    if KUSK_A == 1:
      print "Processing parks ..."

      arcpy.Select_analysis("KUSK_A", scratchDB + "KUSK04", "x_kod = 'KUSK04'")
      if int(arcpy.GetCount_management(scratchDB + "KUSK04").getOutput(0))>0: 
        if arcpy.Exists(outPath + "KUSK320"):
          arcpy.Delete_management(outPath + "KUSK320")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "KUSK04", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 320)
        rasTemp.save(outPath + "KUSK320")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "KUSK320", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "KUSK320")


    # 34 - natural sites (OIPR_L and OIPR_P)
    if OIPR_L == 1:
      print "Processing natural sites (lines)..."
      
      print "Lines of shrubs or hedgerows ..."
      arcpy.Select_analysis("OIPR_L", scratchDB + "OIPR08", "x_kod = 'OIPR08'")
      if int(arcpy.GetCount_management(scratchDB + "OIPR08").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIPR341"):
          arcpy.Delete_management(outPath + "OIPR341")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "OIPR08","","1","")
        rasTemp = Con(eucDistTemp < 2, 341, 1)
        rasTemp.save(outPath + "OIPR341")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIPR341", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "OIPR341")

      print "Lines of trees ..."
      arcpy.Select_analysis("OIPR_L", scratchDB + "OIPR10", "x_kod = 'OIPR10'")
      if int(arcpy.GetCount_management(scratchDB + "OIPR10").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIPR342"):
          arcpy.Delete_management(outPath + "OIPR342")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "OIPR10","","1","")
        rasTemp = Con(eucDistTemp < 2, 342, 1)
        rasTemp.save(outPath + "OIPR342")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIPR342", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "OIPR342")

    if OIPR_P == 1:
      print "Processing natural sites (points)..."
      
      print "Individual tress or groups of trees ..."
      arcpy.Select_analysis("OIPR_P", scratchDB + "OIPR01", "x_kod = 'OIPR01'")
      if int(arcpy.GetCount_management(scratchDB + "OIPR01").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIPR343"):
          arcpy.Delete_management(outPath + "OIPR343")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "OIPR01","","1","")
        rasTemp = Con(eucDistTemp < 5, 343, 1)
        rasTemp.save(outPath + "OIPR343")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIPR343", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "OIPR343")

      print "Clumbs of shrubs ..."
      arcpy.Select_analysis("OIPR_P", scratchDB + "OIPR03", "x_kod = 'OIPR03'")
      if int(arcpy.GetCount_management(scratchDB + "OIPR03").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIPR344"):
          arcpy.Delete_management(outPath + "OIPR344")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "OIPR03","","1","")
        rasTemp = Con(eucDistTemp < 5, 344, 1)
        rasTemp.save(outPath + "OIPR344")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIPR344", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "OIPR344")

      print "Small forest ..."
      arcpy.Select_analysis("OIPR_P", scratchDB + "OIPR06", "x_kod = 'OIPR06'")
      if int(arcpy.GetCount_management(scratchDB + "OIPR06").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIPR345"):
          arcpy.Delete_management(outPath + "OIPR345")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "OIPR06","","1","")
        rasTemp = Con(eucDistTemp < 8, 345, 1)
        rasTemp.save(outPath + "OIPR345")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIPR345", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultural.append(outPath + "OIPR345")

  ## THEME BUILDING (4)

    rasterList_builtup = []

    # 41 - built-up areas (PTZB)
    if PTZB_A == 1:
      print "Processing built-up areas ..."

      print "Multi-familty housing ..."
      arcpy.Select_analysis("PTZB_A", scratchDB + "PTZB01", "x_kod = 'PTZB01'")
      if int(arcpy.GetCount_management(scratchDB + "PTZB01").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTZB411"):
          arcpy.Delete_management(outPath + "PTZB411")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTZB01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 411)
        rasTemp.save(outPath + "PTZB411")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTZB411", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTZB411")

      print "Single-familty housing ..."
      arcpy.Select_analysis("PTZB_A", scratchDB + "PTZB02", "x_kod = 'PTZB02'")
      if int(arcpy.GetCount_management(scratchDB + "PTZB02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTZB412"):
          arcpy.Delete_management(outPath + "PTZB412")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTZB02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 412)
        rasTemp.save(outPath + "PTZB412")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTZB412", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTZB412")

      print "Industrial built-up areas ..."
      arcpy.Select_analysis("PTZB_A", scratchDB + "PTZB03", "x_kod = 'PTZB03'")
      if int(arcpy.GetCount_management(scratchDB + "PTZB03").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTZB413"):
          arcpy.Delete_management(outPath + "PTZB413")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTZB03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 413)
        rasTemp.save(outPath + "PTZB413")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTZB413", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTZB413")

      print "Commercial built-up areas ..."
      arcpy.Select_analysis("PTZB_A", scratchDB + "PTZB04", "x_kod = 'PTZB04'")
      if int(arcpy.GetCount_management(scratchDB + "PTZB04").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTZB414"):
          arcpy.Delete_management(outPath + "PTZB414")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTZB04", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 414)
        rasTemp.save(outPath + "PTZB414")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTZB414", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTZB414")

      print "Other built-up areas ..."
      arcpy.Select_analysis("PTZB_A", scratchDB + "PTZB05", "x_kod = 'PTZB05'")
      if int(arcpy.GetCount_management(scratchDB + "PTZB05").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTZB415"):
          arcpy.Delete_management(outPath + "PTZB415")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTZB05", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 415)
        rasTemp.save(outPath + "PTZB415")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTZB415", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTZB415")

    # 420 - place (PTPL)
    if PTPL_A == 1:
      print "Processing places ..."

      if int(arcpy.GetCount_management("PTPL_A").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTPL420"):
          arcpy.Delete_management(outPath + "PTPL420")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion("PTPL_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 420)
        rasTemp.save(outPath + "PTPL420")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTPL420", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTPL420")

    # 430 - landfill site (PTSO)
    if PTSO_A == 1:
      print "Processing landfill sites ..."

      if int(arcpy.GetCount_management("PTSO_A").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTSO430"):
          arcpy.Delete_management(outPath + "PTSO430")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion("PTSO_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 430)
        rasTemp.save(outPath + "PTSO430")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTSO430", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "PTSO430")

    # 440 - yards not included in PTZB (KUMN02)
    if KUMN_A == 1:
      print "Processing yards ..."

      arcpy.Select_analysis("KUMN_A", scratchDB + "KUMN02", "x_kod = 'KUMN02'")
      arcpy.Erase_analysis(scratchDB + "KUMN02", "PTZB_A", scratchDB + "KUMN02_erase")
      if int(arcpy.GetCount_management(scratchDB + "KUMN02_erase").getOutput(0))>0: 
        if arcpy.Exists(outPath + "KUMN440"):
          arcpy.Delete_management(outPath + "KUMN440")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "KUMN02_erase", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 440)
        rasTemp.save(outPath + "KUMN440")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "KUMN440", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "KUMN440")

    # 450 - cementries (KUSC01)
    if KUSC_A == 1:
      print "Processing cementries ..."

      arcpy.Select_analysis("KUSC_A", scratchDB + "KUSC01", "x_kod = 'KUSC01'")
      if int(arcpy.GetCount_management(scratchDB + "KUSC01").getOutput(0))>0: 
        if arcpy.Exists(outPath + "KUSC450"):
          arcpy.Delete_management(outPath + "KUSC450")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "KUSC01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 450)
        rasTemp.save(outPath + "KUSC450")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "KUSC450", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "KUSC450")

    # 460 - sport and recreational areas (KUSK03)
    if KUSK_A == 1:
      print "Processing sport and recreational areas ..."

      arcpy.Select_analysis("KUSK_A", scratchDB + "KUSK03", "x_kod = 'KUSK03'")
      if int(arcpy.GetCount_management(scratchDB + "KUSK03").getOutput(0))>0: 
        if arcpy.Exists(outPath + "KUSK460"):
          arcpy.Delete_management(outPath + "KUSK460")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "KUSK03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 460)
        rasTemp.save(outPath + "KUSK460")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "KUSK460", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "KUSK460")

    # 47 - sport facilities (BUSP)
    if BUSP_A == 1:
      print "Processing sport facilities ..."

      print "Playgrounds ..."
      arcpy.Select_analysis("BUSP_A", scratchDB + "BUSP06", "x_kod = 'BUSP06'")
      if int(arcpy.GetCount_management(scratchDB + "BUSP06").getOutput(0))>0: 
        if arcpy.Exists(outPath + "BUSP471"):
          arcpy.Delete_management(outPath + "BUSP471")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "BUSP06", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 471)
        rasTemp.save(outPath + "BUSP471")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUSP471", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "BUSP471")

      print "Playing fields ..."
      arcpy.Select_analysis("BUSP_A", scratchDB + "BUSP07", "x_kod = 'BUSP07'")
      if int(arcpy.GetCount_management(scratchDB + "BUSP07").getOutput(0))>0: 
        if arcpy.Exists(outPath + "BUSP472"):
          arcpy.Delete_management(outPath + "BUSP472")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "BUSP07", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 472)
        rasTemp.save(outPath + "BUSP472")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUSP472", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "BUSP472")

    # 480 - industrial chimneys (BUWT02)
    if BUWT_P == 1:
      print "Processing industrial chimneys ..."

      arcpy.Select_analysis("BUWT_P", scratchDB + "BUWT02", "x_kod = 'BUWT02'")
      if int(arcpy.GetCount_management(scratchDB + "BUWT02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "BUWT480"):
          arcpy.Delete_management(outPath + "BUWT480")
          print "... deleting existing raster"
        eucDistTemp = EucDistance(scratchDB + "BUWT02","","1","")
        rasTemp = Con(eucDistTemp < 5, 480, 1)
        rasTemp.save(outPath + "BUWT480")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUWT480", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_builtup.append(outPath + "BUWT480")

    # 490 - buildings (BUBD + OIOR10 + OIOR11)
    if BUBD_A == 1:
      print "Processing buildings ..."

      if int(arcpy.GetCount_management("BUBD_A").getOutput(0))>0:
        arcpy.CopyFeatures_management("BUBD_A", scratchDB + "BUBD_A")
        arcpy.DeleteField_management(scratchDB + "BUBD_A", [x.name for x in arcpy.ListFields(scratchDB + "BUBD_A") if x.name not in ["OBJECTID", "Shape", "x_kod", "Shape_Area", "Shape_Length"]])
        if OIOR_A == 1:
          arcpy.Select_analysis("OIOR_A", scratchDB + "OIOR10_11", "x_kod = 'OIOR10' OR x_kod = 'OIOR11'")
          if int(arcpy.GetCount_management(scratchDB + "OIOR10_11").getOutput(0))>0:
            # removing field from layer, except "x_kod"
            arcpy.DeleteField_management(scratchDB + "OIOR10_11", [x.name for x in arcpy.ListFields(scratchDB + "OIOR10_11") if x.name not in ["OBJECTID", "Shape", "x_kod", "Shape_Area", "Shape_Length"]])
            arcpy.Append_management(scratchDB + "OIOR10_11", scratchDB + "BUBD_A")

        if arcpy.Exists(outPath + "BUBD490"):
          arcpy.Delete_management(outPath + "BUBD490")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "BUBD_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")                        
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 490)
        rasTemp.save(outPath + "BUBD490")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "BUBD490", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")

  ## THEME NATURE (5)

    rasterList_nature = []

    # 51 - grasslands
    if PTTR_A == 1:
      print "Processing natural grasslands ..."
      arcpy.Select_analysis("PTTR_A", scratchDB + "PTTR01", "x_kod = 'PTTR01'")
      if int(arcpy.GetCount_management(scratchDB + "PTTR01").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTTR511"):
          arcpy.Delete_management(outPath + "PTTR511")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTTR01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 511)
        rasTemp.save(outPath + "PTTR511")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTTR511", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTTR511")

    if PZ == 1:
      print "Processing permanent grasslands ..."
      arcpy.Select_analysis("PZ", scratchDB + "PZ_TC", "kod = 'T' OR kod = 'C'")
      if int(arcpy.GetCount_management(scratchDB + "PZ_TC").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTTR512"):
          arcpy.Delete_management(outPath + "PTTR512")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PZ_TC", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 512)
        rasTemp.save(outPath + "PTTR512")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTTR512", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTTR512")

      
    # 52 - forested areas (PTLZ)
    if PTLZ_A == 1:
      print "Processing forested areas ..."

      print "Forest ..."
      arcpy.Select_analysis("PTLZ_A", scratchDB + "PTLZ01", "x_kod = 'PTLZ01'")
      if int(arcpy.GetCount_management(scratchDB + "PTLZ01").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTLZ521"):
          arcpy.Delete_management(outPath + "PTLZ521")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTLZ01", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 521)
        rasTemp.save(outPath + "PTLZ521")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTLZ521", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTLZ521")

      print "Copses ..."
      arcpy.Select_analysis("PTLZ_A", scratchDB + "PTLZ02", "x_kod = 'PTLZ02'")
      if int(arcpy.GetCount_management(scratchDB + "PTLZ02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTLZ522"):
          arcpy.Delete_management(outPath + "PTLZ522")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTLZ02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 522)
        rasTemp.save(outPath + "PTLZ522")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTLZ522", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTLZ522")

      print "Foliages ..."
      arcpy.Select_analysis("PTLZ_A", scratchDB + "PTLZ03", "x_kod = 'PTLZ03'")
      if int(arcpy.GetCount_management(scratchDB + "PTLZ03").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTLZ523"):
          arcpy.Delete_management(outPath + "PTLZ523")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTLZ03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 523)
        rasTemp.save(outPath + "PTLZ523")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTLZ523", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTLZ523")

    # 530 - shrubs (PTRK02)
    if PTRK_A == 1:
      print "Processing shrubs ..."

      arcpy.Select_analysis("PTRK_A", scratchDB + "PTRK02", "x_kod = 'PTRK02'")
      if int(arcpy.GetCount_management(scratchDB + "PTRK02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTRK530"):
          arcpy.Delete_management(outPath + "PTRK530")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTRK02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 530)
        rasTemp.save(outPath + "PTRK530")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTRK530", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTRK530")

    # 540 - non-use areas (sandy, gravelly...) (PTGN)
    if PTGN_A == 1:
      print "Processing non-use areas ..."

      if int(arcpy.GetCount_management("PTGN_A").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTGN540"):
          arcpy.Delete_management(outPath + "PTGN540")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion("PTGN_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 540)
        rasTemp.save(outPath + "PTGN540")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTGN540", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTGN540")

    # 550 - other non-built up areas (barrens) (PTNZ)
    if PTNZ_A == 1:
      print "Processing other non-built up areas ..."

      if int(arcpy.GetCount_management("PTNZ_A").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTNZ550"):
          arcpy.Delete_management(outPath + "PTNZ550")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion("PTNZ_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 550)
        rasTemp.save(outPath + "PTNZ550")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTNZ550", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "PTNZ550")

    # 560 - wetlands (OIMK_A)
    if OIMK_A == 1:
      print "Processing wetlands ..."
      if int(arcpy.GetCount_management("OIMK_A").getOutput(0))>0:
        if arcpy.Exists(outPath + "OIMK560"):
          arcpy.Delete_management(outPath + "OIMK560")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion("OIMK_A", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 560)
        rasTemp.save(outPath + "OIMK560")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "OIMK560", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_nature.append(outPath + "OIMK560")

  ## THEME CULTIVABLE (6)

    rasterList_cultivable = []

    # 61 - cultivable areas (PTUT)
    if PTUT_A == 1:
      print "Processing cultivable areas ..."

      print "Plantations ..."
      arcpy.Select_analysis("PTUT_A", scratchDB + "PTUT02", "x_kod = 'PTUT02'")
      if int(arcpy.GetCount_management(scratchDB + "PTUT02").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTUT611"):
          arcpy.Delete_management(outPath + "PTUT611")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTUT02", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 611)
        rasTemp.save(outPath + "PTUT611")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTUT611", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultivable.append(outPath + "PTUT611")

      print "Orchards ..."
      arcpy.Select_analysis("PTUT_A", scratchDB + "PTUT03", "x_kod = 'PTUT03'")
      if int(arcpy.GetCount_management(scratchDB + "PTUT03").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTUT612"):
          arcpy.Delete_management(outPath + "PTUT612")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTUT03", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 612)
        rasTemp.save(outPath + "PTUT612")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTUT612", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultivable.append(outPath + "PTUT612")

      print "Tree plantations ..."
      arcpy.Select_analysis("PTUT_A", scratchDB + "PTUT04", "x_kod = 'PTUT04'")
      if int(arcpy.GetCount_management(scratchDB + "PTUT04").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTUT613"):
          arcpy.Delete_management(outPath + "PTUT613")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTUT04", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 613)
        rasTemp.save(outPath + "PTUT613")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTUT613", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultivable.append(outPath + "PTUT613")

      print "Plantation of ornamental plants ..."
      arcpy.Select_analysis("PTUT_A", scratchDB + "PTUT05", "x_kod = 'PTUT05'")
      if int(arcpy.GetCount_management(scratchDB + "PTUT05").getOutput(0))>0: 
        if arcpy.Exists(outPath + "PTUT614"):
          arcpy.Delete_management(outPath + "PTUT614")
          print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "PTUT05", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
        rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 614)
        rasTemp.save(outPath + "PTUT614")
        arcpy.Delete_management(outPath + "tmpRaster")
      else:
        print "No such features in the area"
        arcpy.CopyRaster_management(outPath + "land", outPath + "PTUT614", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
      rasterList_cultivable.append(outPath + "PTUT614")

    rasTemp = ''
    rasTemp1 = ''
    print " "
  #===== End Chunk: Conversion =====#

  #===== Chunk: Themes =====#
  # Combine rasters to thematic maps 

    if water == 1:   #Assembles a water theme
      print "Processing water layers ..."
      if arcpy.Exists(outPath + "T1_water"):
        arcpy.Delete_management(outPath + "T1_water")
        print "... deleting existing raster"
      print rasterList_water
      rasTemp = CellStatistics(rasterList_water, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T1_water")

    if road == 1:   #Assembles a road theme
      print "Processing road layers ..."
      if arcpy.Exists(outPath + "T2_road"):
        arcpy.Delete_management(outPath + "T2_road")
        print "... deleting existing raster"
      print rasterList_road
      rasTemp = CellStatistics(rasterList_road, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T2_road")

    if cultural == 1:   #Assembles a cultural theme
      print "Processing cultural layers ..."
      if arcpy.Exists(outPath + "T3_cultural"):
        arcpy.Delete_management(outPath + "T3_cultural")
        print "... deleting existing raster"
      print rasterList_cultural
      rasTemp = CellStatistics(rasterList_cultural, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T3_cultural")

    if builtup == 1:   #Assembles a builtup theme
      print "Processing builtup layers ..."
      if arcpy.Exists(outPath + "T4_builtup"):
        arcpy.Delete_management(outPath + "T4_builtup")
        print "... deleting existing raster"
      print rasterList_builtup
      rasTemp = CellStatistics(rasterList_builtup, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T4_builtup")

    if nature == 1:   #Assembles a nature theme
      print "Processing nature layers ..."
      if arcpy.Exists(outPath + "T5_nature"):
        arcpy.Delete_management(outPath + "T5_nature")
        print "... deleting existing raster"
      print rasterList_nature
      rasTemp = CellStatistics(rasterList_nature, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T5_nature")

    if cultivable == 1:   #Assembles a cultivable theme
      print "Processing cultivable layers ..."
      if arcpy.Exists(outPath + "T6_cultivable"):
        arcpy.Delete_management(outPath + "T6_cultivable")
        print "... deleting existing raster"
      print rasterList_cultivable
      rasTemp = CellStatistics(rasterList_cultivable, "MAXIMUM", "DATA")
      rasTemp.save (outPath + "T6_cultivable")

  #===== End Chunk: Themes =====#

    #===== Chunk: Stack =====#
    # Assemble the final map
    # First delete any existing layers
    if finalmap == 1:   
      print "Processing mosaic for all themes ..."
      if arcpy.Exists(outPath + "MapReclassified"):
        arcpy.Delete_management(outPath + "MapReclassified")
        print "... deleting existing raster"
      if arcpy.Exists(outPath + "MapRaw"):
        arcpy.Delete_management(outPath + "MapRaw")
        print "... deleting existing raster"
      if arcpy.Exists(outPath + "MapFinal"):
        arcpy.Delete_management(outPath + "MapFinal")
        print "... deleting existing raster"

      print " "

    # Stack the thematic maps 
    # Here the hierarchy of individual themes is determined
    T1wa = Raster(outPath + "T1_water")
    T2ro = Raster(outPath + "T2_road")
    T3cu = Raster(outPath + "T3_cultural")
    T4bu = Raster(outPath + "T4_builtup")
    T5na = Raster(outPath + "T5_nature")
    T6cul = Raster(outPath + "T6_cultivable")

    fields = Raster(outPath + "fields_final")   # fields
    buildings_490 = Raster(outPath + "BUBD490")

    step1 = Con(T5na > 1, T5na, 1)                    # nature first
    print "natural areas added to map ..."
    step2 = Con(T6cul == 1, step1, T6cul)               # cultivable areas on top (nature)
    print "cultivable areas added to map ..."    
    step3 = Con(T4bu == 1, step2, T4bu)                  # built up areas on top (nature, cultivable)
    print "built up areas added to map ..."
    step4 = Con(T3cu == 1, step3, T3cu)                  # cultural features on top (nature, cultivable, built-up)
    print "cultural features added to map ..."
    step5 = Con(T1wa == 1, step4, T1wa)                   # freshwater on top (nature, cultivable, built-up, cultural)
    print "water added to map ..."
    step6 = Con(T2ro == 1, step5, T2ro)                   # roads on top (nature, cultivable, built-up, cultural, water)
    print "roads added to map ..."
    step7 = Con(buildings_490 == 1, step6, buildings_490)      # buildings on top (nature, cultivable, built-up, cultural, water, roads)
    print "buildings added to map ..."
    step8 = Con(step7 == 1, fields, step7)      # fields and field margins (i.e., long features/cadastral parcels classified as fields according to BDOT10k database) added in empty space
    print "fields added to map ..."
    step8.save(outPath + "MapRaw")
    #arcpy.RasterToPolygon_conversion(step8, outPath + "MapRaw_vector", "NO_SIMPLIFY", "VALUE")
              
    nowTime = time.strftime('%X %x')
    print "Raw landscape map created ..." + nowTime
    print "  "
    #===== End Chunk: Stack =====#

  except:
    if arcpy.Exists(outPath + "tmpRaster"):
        arcpy.Delete_management(outPath + "tmpRaster")
    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print msgs
    print pymsg

    arcpy.AddMessage(arcpy.GetMessages(1))
    print arcpy.GetMessages(1)

    

