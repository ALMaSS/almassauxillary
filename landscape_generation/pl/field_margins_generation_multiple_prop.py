#=======================================================================================================================
# Name: Landscape generator for Poland - field margins genegeration
# Authors: Elzbieta Ziolkowska - October 2018
# Last large update: Nov 2019

#===== Chunk: Setup =====#
# Import system modules
import arcpy, traceback, sys, time, gc, os, random, csv
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")

dst = "D:/ALMaSS_landscapes/Polish_landscapes/test_areas_new/"  # Output destination
test_areas_list = ["Wielun", "Skulsk", "Lipno", "Warnice", "Siematycze"]

# Set field margins properties
width = 4  # provide margin width in meters
#prop_list = [0.10, 0.20, 0.30, 0.40, 0.50, 0.60]  # provide list of proportion of fields with margins as value from 0.0 to 1.0 [assumed]
prop_list = [0.10, 0.20, 0.30, 0.40]
marginType = 160 #FieldBoundary 160, Mown grass 58, Permanent Setaside 33, Hedge 130

# Set field conditions
minFieldSize = 10000 # area condition in m2
minFieldWidth = 20  # width condition in m [based on minimum bounding geometry]


for test_area in test_areas_list:

    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    outPath = dst + test_area + "/outputs/" + test_area + ".gdb/"                    # saves maps here
    localSettings = dst + test_area + "/outputs/project.gdb/" + test_area + "_mask_buffered1km"   # project folder with mask
    gisDB = dst + test_area + "/" + test_area + ".gdb/"                                             # input features
    scratchDB = dst + test_area + "/outputs/scratch.gdb/"                      # scratch folder for tempfiles
    fields_map = outPath + "FIELDS_" + test_area
    input_reclass_map = outPath + "map_final_reclass"

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"

    try:
        # Creating map of possible field margins
        # This need to be done only once for each width regardless of the proportion of fields
##        print "Creating map of possible field margins ..."
##        print "Creating a basic 1-m field boundaries ..."
##        reclass_land_FM = ReclassByASCIIFile(input_reclass_map, dst + "reclasstable_FM.txt", "DATA")
##        outFocalStatMin = FocalStatistics(reclass_land_FM, NbrRectangle(3, 3, "CELL"), "MINIMUM", "DATA")
##        outFocalStatMax = FocalStatistics(reclass_land_FM, NbrRectangle(3, 3, "CELL"), "MAXIMUM", "DATA")
##        outCellStatsMin = CellStatistics([reclass_land_FM, outFocalStatMin], "VARIETY", "DATA")
##        outCellStatsMax = CellStatistics([reclass_land_FM, outFocalStatMax], "VARIETY", "DATA")
##        outConMin1 = Con(outCellStatsMin == 2, 1, 0)
##        outConMin2 = Con(outFocalStatMin != 1, 1, 0)
##        outConMin3 = Con(Times(outConMin1, outConMin2) == 1, 0, 1)
##        outConMax1 = Con(outCellStatsMax == 2, 1, 0)
##        outConMax2 = Con(reclass_land_FM > 1000, 1, 0)
##        outConMax3 = Con(Times(outConMax1, outConMax2) == 1, 0, 1)
##        toFieldMargins = Con(Plus(outConMin3, outConMax3) < 2, marginType, input_reclass_map)
##
##        # if FM > 1m then need to expand
##        if width > 1:
##            print "Defined width > 1m ... expanding 1-m field boundaries to " + str(width) + " m ..."
##            outExpand = Expand(toFieldMargins, width-1, [marginType])
##            toFieldMarginsFinal = Con(toFieldMargins < 1000, toFieldMargins, outExpand)
##        else:
##            toFieldMarginsFinal = toFieldMargins
##        toFieldMarginsFinal.save(outPath + "toFieldMarginsFinal" + test_area + "w" + str(width) + "m")
##        print "Map of possible field margins generated ..."
                         
        # convert final map to vector layer
        arcpy.RasterToPolygon_conversion(input_reclass_map, outPath + "map_final_reclass_vector", "NO_SIMPLIFY", "VALUE")
        # select polygons with fields & calculate field properties (MBG width, length and orientation)
        # this needs to be done only once for each landscape
        arcpy.MinimumBoundingGeometry_management(fields_map, scratchDB + "fields_map_bounding", "RECTANGLE_BY_WIDTH", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(fields_map, "Id", scratchDB + "fields_map_bounding", "Id", "")

        fields_gridcode = [row.getValue("gridcode") for row in arcpy.SearchCursor(fields_map, fields="gridcode")]
        fields_area = [row.getValue("Shape_Area") for row in arcpy.SearchCursor(fields_map, fields="Shape_Area")]
        fields_width = [row.getValue("MBG_Width_12") for row in arcpy.SearchCursor(fields_map, fields="MBG_Width_12")]
        fields_length = [row.getValue("MBG_Length_12") for row in arcpy.SearchCursor(fields_map, fields="MBG_Length_12")]

        fields = []
        for n in range (0, len(fields_gridcode)):
            fields.append([fields_gridcode[n], fields_area[n], fields_width[n], fields_length[n]])
        fields_with_conditions = []
        for field in fields:
            if (field[1] > minFieldSize and field[2] > minFieldWidth):
                    fields_with_conditions.append(field)

        print "No of fields in the landscape: " + str(len(fields))
        print "No of fields after aplying conditions: " + str(len(fields_with_conditions))

        fields_left = fields_with_conditions
        fields_selected = []
        gridcode_selected = []

        # creating field margins
        for prop in prop_list:
            print "Creating field margins of " + str(width) + " meters for " + str(int(prop*100)) + "% of fields in the landscape " + test_area + " ..."
            asciiexp = dst + test_area + "/outputs/ALMaSS_files/ASCII_" + test_area + "_FM_" + str(int(prop*100)) + "w" + str(width) + "m.txt"    # export in ascii (for ALMaSS)
            attrtable = "ATTR_" + test_area + "_FM_" + str(int(prop*100)) + "w" + str(width) + "m.csv"  # Name of attribute table
            attrexp = os.path.join(dst, test_area, "outputs/ALMaSS_files", attrtable)  # full path
            permtable = "PERM_CROPS_" + test_area + "_FM_" + str(int(prop*100)) + "w" + str(width) + "m.csv"  # Name of permanent crops table
            permexp = os.path.join(dst, test_area, "outputs/ALMaSS_files")  # folder

            if prop_list.index(prop)==0:
                prop_calc=prop_list[0]
            else:
                prop_calc=prop - prop_list[prop_list.index(prop)-1]
            print "Current prop to selected is " + str(prop_calc)

            maxNoFields = int(len(fields) * prop_calc)
            print "Targeted no of fields to select is: " + str(maxNoFields)
            k=0
            while (k<=maxNoFields and len(fields_left)>0):
                sample_field = random.sample(fields_with_conditions, 1)[0]
                fields_selected.append(sample_field)
                k=k+1
                fields_left.remove(sample_field)
            if k > maxNoFields:
                print "Declared proportion of fields has been selected ..."
            else:
                print str(k) + " fields from " + str(maxNoFields) + " declared has been selected ..."
                print "This gives " + str(k*100/len(fields)) + "% instead of " + str(int(prop_calc*100)) + "% declared for this stage ..."
                print "Other fields do not fulfill conditions for field margins generation ..."
                    
            for n in range (0, len(fields_selected)):
                gridcode_selected.append(fields_selected[n][0])
            print "Current gridcode list selected is:"
            print gridcode_selected

            whereClause = "VALUE IN (" + str(gridcode_selected)[1:-1] + ")"
            toFM = Con(input_reclass_map, outPath + "toFieldMarginsFinal" + test_area + "w" + str(width) + "m", input_reclass_map, whereClause)                
            toFM.save(outPath + "map_final_reclass_FM" + str(int(prop*100)) + "_k" + str(k*100/len(fields)) + "w" + str(width) + "m")
            print "Field margins generated for " + str(int(prop*100)) + "_k" + str(k*100/len(fields)) + " fields ..."

            # clipping back to the original extent
            outExtractByMask = ExtractByMask(toFM, dst + test_area + "/outputs/project.gdb/" + test_area + "_mask")
            outExtractByMask.save(outPath + "map_final_reclass_FM" + str(int(prop*100)) + "_k" + str(k*100/len(fields)) + "w" + str(width) + "m_mask")

            arcpy.env.extent = dst + test_area + "/outputs/project.gdb/" + test_area + "_mask"
            arcpy.env.mask = dst + test_area + "/outputs/project.gdb/" + test_area + "_mask"
            arcpy.env.cellSize = dst + test_area + "/outputs/project.gdb/" + test_area + "_mask"

            if k>0:
                # regionalise map
                print "Creating ALMaSS ASCII input file ..."
                regionALM = RegionGroup(outExtractByMask,"EIGHT","WITHIN","ADD_LINK","")
                regionALM.save(outPath + "Map_ALMaSS_FM" + str(int(prop*100)) + "_k" + str(k*100/len(fields)) + "w" + str(width) + "m")
                nowTime = time.strftime('%X %x')
                print "Regionalisation done ..." + nowTime

                # export attribute table 
                table = outPath + "Map_ALMaSS_FM" + str(int(prop*100)) + "_k" + str(k*100/len(fields)) + "w" + str(width) + "m"
                # Write an attribute tabel - based on this answer:
                # https://geonet.esri.com/thread/83294
                # List the fields
                fields_table = arcpy.ListFields(table)  
                field_names = [field.name for field in fields_table]  
                  
                with open(attrexp,'wb') as f:  
                    w = csv.writer(f)  
                    # Write the headers
                    w.writerow(field_names)  
                    # The search cursor iterates through the 
                    for row in arcpy.SearchCursor(table):  
                        field_vals = [row.getValue(field.name) for field in fields_table]  
                        w.writerow(field_vals)  
                        del row
                print "Attribute table exported..." + nowTime

                # convert regionalised map to ascii
                arcpy.RasterToASCII_conversion(regionALM, asciiexp)
                print "Conversion to ASCII done ..." + nowTime

                # fixing permanent crop staff
                permCropRegions = Con(regionALM, regionALM, 0, "LINK IN (35, 55, 56, 214)")
                perCropsNull = SetNull(permCropRegions, permCropRegions, "VALUE = 0")

                arcpy.PolygonToRaster_conversion(gisDB + "fields_final", "ZAKODOWANY_NR_PRODUCENTA", scratchDB + "fields_owners", "", "", 1)
                outZSaT = ZonalStatisticsAsTable(perCropsNull, "VALUE", scratchDB + "fields_owners", scratchDB + "permanent_crops_zonal", "DATA", "MAJORITY")
                arcpy.TableToTable_conversion(scratchDB + "permanent_crops_zonal", permexp, permtable)
                endTime = time.strftime('%X %x')
                print "ALMaSS inputs generated ..."  + endTime
            else:
                print "No more field margin is possible to be generated ..."

        
    #===== End Chunk: Finalize =====#

    except:
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

        arcpy.AddMessage(arcpy.GetMessages(1))
        print arcpy.GetMessages(1)

